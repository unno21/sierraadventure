package com.example.sierradventure;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sierradventure.Utilities.Global;
import com.google.android.gms.common.internal.GmsLogger;

import java.util.Arrays;
import java.util.Locale;

public class InformationActivity extends AppCompatActivity {

    ImageView imgInfoImage;
    EditText txtInfoDescription;
    TextToSpeech tts;
    Button btnNext, btnBack;

    EditText txtEnglishName, txtLocalName, txtScientificName, txtConservationStatus, txtPhotoCredit;
    TextView lblLocalName, lblPhotoCredit;
    CardView cardViewInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        imgInfoImage = findViewById(R.id.imgInfoImage);
        txtInfoDescription = findViewById(R.id.txtInfoDescription);
        btnNext = findViewById(R.id.btnNext);
        btnBack = findViewById(R.id.btnBack);

        cardViewInfo = findViewById(R.id.cardViewInfo);
        lblLocalName = findViewById(R.id.lblLocalName);
        txtEnglishName = findViewById(R.id.txtEnglishName);
        txtLocalName = findViewById(R.id.txtLocalName);
        txtScientificName = findViewById(R.id.txtScientificName);
        txtConservationStatus = findViewById(R.id.txtConservationStatus);

        lblPhotoCredit = findViewById(R.id.lblPhotoCredit);
        txtPhotoCredit = findViewById(R.id.txtPhotoCredit);

        ((Button)findViewById(R.id.btnInfoHome)).setOnClickListener(v -> {
            startActivity(new Intent(InformationActivity.this, HomeActivity.class));
            finish();
        });

        ((Button)findViewById(R.id.btnInfoMap)).setOnClickListener(v -> {
            startActivity(new Intent(InformationActivity.this, MapsActivity.class));
            finish();
        });


        //show info
        txtPhotoCredit.setText(Global.SelectedAnimal.getPhotoCredit());
        txtEnglishName.setText(Global.SelectedAnimal.getEnglishName());
        txtLocalName.setText(Global.SelectedAnimal.getLocalName());
        txtScientificName.setText(Global.SelectedAnimal.getScientificName());
        txtConservationStatus.setText(Global.SelectedAnimal.getConservationStatus());

        //make readonly
        txtPhotoCredit.setFocusable(false);
        txtPhotoCredit.setClickable(false);
        txtInfoDescription.setClickable(false);
        txtInfoDescription.setFocusable(false);
        txtEnglishName.setFocusable(false);
        txtEnglishName.setClickable(false);
        txtLocalName.setFocusable(false);
        txtLocalName.setClickable(false);
        txtScientificName.setFocusable(false);
        txtScientificName.setClickable(false);
        txtConservationStatus.setFocusable(false);
        txtConservationStatus.setClickable(false);

        //hide info not needed
        if (Global.SelectedTab.equals("tree")) {
            cardViewInfo.setVisibility(View.GONE);
            lblLocalName.setVisibility(View.GONE);
            txtLocalName.setVisibility(View.GONE);
        }
//        else if (Global.SelectedTab.equals("animal")) {
//            txtPhotoCredit.setVisibility(View.GONE);
//            lblPhotoCredit.setVisibility(View.GONE);
//        }

        btnNext.setOnClickListener(v -> {


            if (Global.SelectedTab.equals("animal")) {
                int index = Arrays.asList(Global.SelectedPlace.getAnimals()).indexOf(Global.SelectedAnimal);
                index++;
                if (Global.SelectedPlace.getAnimals().length == index) {
                    startActivity(new Intent(InformationActivity.this, GameModeActivity.class));
                    finish();
                } else {
                    Global.SelectedAnimal = Global.SelectedPlace.getAnimals()[index];
                    finish();
                    startActivity(new Intent(InformationActivity.this, InformationActivity.class));
                }
            } else {
                int index = Arrays.asList(Global.SelectedPlace.getTrees()).indexOf(Global.SelectedAnimal);
                index++;
                if (Global.SelectedPlace.getTrees().length == index) {
                    startActivity(new Intent(InformationActivity.this, GameModeActivity.class));
                    finish();
                } else {
                    Global.SelectedAnimal = Global.SelectedPlace.getTrees()[index];
                    finish();
                    startActivity(new Intent(InformationActivity.this, InformationActivity.class));
                }
            }


        });

        imgInfoImage.setBackgroundResource(Global.SelectedAnimal.getImage());
        txtInfoDescription.setText(Global.SelectedAnimal.getDescription());


        tts = Global.Tts;
        if (Global.SelectedTab.equals("animal"))
            tts.speak(Global.SelectedAnimal.getDescription(), TextToSpeech.QUEUE_FLUSH, null);


//        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//                if (status == TextToSpeech.SUCCESS) {
//                    int ttsLang = tts.setLanguage(Locale.UK);
//
//                    if (Global.SelectedTab.equals("animal"))
//                        tts.speak(Global.SelectedAnimal.getDescription(), TextToSpeech.QUEUE_FLUSH, null);
//
//                    if (ttsLang == TextToSpeech.LANG_MISSING_DATA
//                            || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
//                        Log.e("TTS", "The Language is not supported!");
//                    } else {
//                        Log.i("TTS", "Language Supported.");
//                    }
//                    Log.i("TTS", "Initialization success.");
//                } else {
//                    Toast.makeText(getApplicationContext(), "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        btnBack.setOnClickListener( v -> {
            finish();
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }
}
