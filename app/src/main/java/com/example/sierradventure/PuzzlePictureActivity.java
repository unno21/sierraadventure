package com.example.sierradventure;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sierradventure.Utilities.ClickSoundService;
import com.example.sierradventure.Utilities.Database;
import com.example.sierradventure.Utilities.Difficulty;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Settings;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import static android.app.PendingIntent.getActivity;

public class PuzzlePictureActivity extends AppCompatActivity {

    private static GestureDetectGridView mGridView;

    private static int COLUMNS = Settings.getDifficulty() == Difficulty.EASY ? 3 : Settings.getDifficulty() == Difficulty.AVERAGE ? 4 : 5;

    private static int DIMENSIONS = COLUMNS * COLUMNS;

    private static int mColumnWidth, mColumnHeight;

    public static final String up = "up";
    public static final String down = "down";
    public static final String left = "left";
    public static final String right = "right";

    private static String[] tileList;

    static Dialog dialogPuzzleComplete;

    private static ImageView actualImage, imgHome, imgMap, imgSetting, imgBack;

    private static CountDownTimer countDownTimer;

    private static TextView lblTime;

    private static Context context;

    TextToSpeech tts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle_picture);

        init();

        scramble();

        setDimensions();

        dialogPuzzleComplete = new Dialog(this);

        actualImage = findViewById(R.id.imgActualImage);

        lblTime = findViewById(R.id.lblTime);

        time = 10000;

        context = this;

        tts = Global.Tts;
        String instuctions = "The goal is to re-assemble the picture. Slide the each piece of picture to move.";
        tts.speak(instuctions, TextToSpeech.QUEUE_FLUSH, null);

//        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//                if (status == TextToSpeech.SUCCESS) {
//                    int ttsLang = tts.setLanguage(Locale.UK);
//
//                    String instuctions = "The goal is to re-assemble the picture. Slide the each piece of picture to move.";
//                    tts.speak(instuctions, TextToSpeech.QUEUE_FLUSH, null);
//                    if (ttsLang == TextToSpeech.LANG_MISSING_DATA
//                            || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
//                        Log.e("TTS", "The Language is not supported!");
//                    } else {
//                        Log.i("TTS", "Language Supported.");
//                    }
//                    Log.i("TTS", "Initialization success.");
//                } else {
//                    Toast.makeText(getApplicationContext(), "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        imgHome = findViewById(R.id.imgPPEasyHome);
        imgMap = findViewById(R.id.imgPPEasyMap);
        imgSetting = findViewById(R.id.imgPPEasySetting);
        imgBack = findViewById(R.id.imgPPEasyBack);

        imgHome.setOnClickListener( v -> {
            playButtonClickSound();
            Intent i = new Intent(PuzzlePictureActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        });

        imgMap.setOnClickListener( v -> {
            playButtonClickSound();
            Intent i = new Intent(PuzzlePictureActivity.this, MapsActivity.class);
            startActivity(i);
            finish();
        });

        imgSetting.setOnClickListener( v -> {
            playButtonClickSound();
            Intent i = new Intent(PuzzlePictureActivity.this, SettingsActivity.class);
            startActivity(i);
        });

        imgBack.setOnClickListener( v -> {
            playButtonClickSound();
            finish();
            startActivity(new Intent(PuzzlePictureActivity.this, LevelSelectActivity.class));
        });

    }

    private void init() {
        mGridView = (GestureDetectGridView) findViewById(R.id.grid);
        mGridView.setNumColumns(COLUMNS);

        tileList = new String[DIMENSIONS];
        for (int i = 0; i < DIMENSIONS; i++) {
            tileList[i] = String.valueOf(i);
        }
    }

    private void scramble() {
        int index;
        String temp;
        Random random = new Random();

        for (int i = tileList.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            temp = tileList[index];
            tileList[index] = tileList[i];
            tileList[i] = temp;
        }
    }

    private void setDimensions() {
        ViewTreeObserver vto = mGridView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mGridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int displayWidth = mGridView.getMeasuredWidth();
                int displayHeight = mGridView.getMeasuredHeight();

                int statusbarHeight = getStatusBarHeight(getApplicationContext());
                int requiredHeight = displayHeight - statusbarHeight;

                mColumnWidth = displayWidth / COLUMNS;
                mColumnHeight = requiredHeight / COLUMNS;

                display(getApplicationContext());
            }
        });
    }

    private int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen",
                "android");

        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }

        //Toast.makeText(context, String.valueOf(result), Toast.LENGTH_SHORT).show();
        //return 48;
        return result;
    }

    private static void display(Context context) {

        Database db = new Database(context);

        String difficulty = Settings.getDifficulty() == Difficulty.EASY ? "EASY" :  Settings.getDifficulty() == Difficulty.AVERAGE ? "AVERAGE" : "HARD";

//        int level = db.getLevel("Jigsaw Puzzle", difficulty);

        int level = Global.SelectedLevel;

        int currentJigsawLevel = level;

//        int[] images = Global.SelectedPlace.getJigsawPuzzle()[currentJigsawLevel - 1].getPartImages();
//        int image = Global.SelectedPlace.getJigsawPuzzle()[currentJigsawLevel - 1].getActualImage();

        int[] images = Settings.getDifficulty() == Difficulty.EASY ? Global.SelectedPlace.getJigsawPuzzle()[currentJigsawLevel - 1].getPartImages() : Settings.getDifficulty() == Difficulty.AVERAGE ? Global.SelectedPlace.getInterMediateJigsawPuzzle()[currentJigsawLevel - 1].getPartImages() : Global.SelectedPlace.getHardJigsawPuzzle()[currentJigsawLevel - 1].getPartImages();
        int image = Settings.getDifficulty() == Difficulty.EASY ? Global.SelectedPlace.getJigsawPuzzle()[currentJigsawLevel - 1].getActualImage() : Settings.getDifficulty() == Difficulty.AVERAGE ? Global.SelectedPlace.getInterMediateJigsawPuzzle()[currentJigsawLevel - 1].getActualImage() : Global.SelectedPlace.getHardJigsawPuzzle()[currentJigsawLevel - 1].getActualImage();

//        Toast.makeText(context, difficulty, Toast.LENGTH_SHORT).show();

        actualImage.setBackgroundResource(image);

        ArrayList<Button> buttons = new ArrayList<>();
        Button button;

//        for (int i = 0; i < tileList.length; i++) {
//            button = new Button(context);
//            button.setVisibility(View.GONE);
//        }

        for (int i = 0; i < tileList.length; i++) {
            button = new Button(context);

            try {

                int index = Integer.parseInt(tileList[i]);
                button.setBackgroundResource(images[index]);
                button.setVisibility(View.VISIBLE);

            } catch (Exception ex) { }

            buttons.add(button);
        }

        mGridView.setAdapter(new CustomAdapter(buttons, mColumnWidth, mColumnHeight));
        startTimer();
    }

    private static void swap(Context context, int currentPosition, int swap) {
        String newPosition = tileList[currentPosition + swap];
        tileList[currentPosition + swap] = tileList[currentPosition];
        tileList[currentPosition] = newPosition;
        display(context);

        if (isSolved()) {
            dialogPuzzleComplete.setContentView(R.layout.dialog_puzzle_completed);
            dialogPuzzleComplete.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialogPuzzleComplete.show();

            stopTimer();

            TextView lblMessageTime = dialogPuzzleComplete.findViewById(R.id.lblJigsawTime);
            ImageButton btnRetry =  dialogPuzzleComplete.findViewById(R.id.btnJigsawRetry);
            btnRetry.setOnClickListener(v -> {
                Intent map = new Intent(context, PuzzlePictureActivity.class);
                context.startActivity(map);
                ((Activity)context).finish();
            });

            ImageButton btnHome =  dialogPuzzleComplete.findViewById(R.id.btnJigsawHome);
            btnHome.setOnClickListener(v -> {

                Database db = new Database(context);

                String difficulty = Settings.getDifficulty() == Difficulty.EASY ? "EASY" :  Settings.getDifficulty() == Difficulty.AVERAGE ? "AVERAGE" : "HARD";

                int level = db.getLevel("Jigsaw Puzzle", difficulty);

                int maxLevel = Settings.getDifficulty() == Difficulty.EASY ? Global.SelectedPlace.getJigsawPuzzle().length :  Settings.getDifficulty() == Difficulty.AVERAGE ? Global.SelectedPlace.getInterMediateJigsawPuzzle().length : Global.SelectedPlace.getHardJigsawPuzzle().length;
                maxLevel = maxLevel > 20 ? 20 : maxLevel;
                if (Global.SelectedLevel == level && level < maxLevel) {

                    db.update("Jigsaw Puzzle", difficulty, ++level);
                }

                Intent home = new Intent(context, HomeActivity.class);
                context.startActivity(home);
            });

            ImageButton btnNext =  dialogPuzzleComplete.findViewById(R.id.btnJigsawNext);
            btnNext.setOnClickListener(v -> {
                Global.SelectedLevel++;
                Intent retry = new Intent(context, PuzzlePictureActivity.class);
                context.startActivity(retry);
                ((Activity)context).finish();
            });

            lblMessageTime.setText("You've completed the puzzle in " + formatTimer(10000 - time));

            context.stopService(new Intent(context, AlertSoundService.class));
            Global.AlertSound = Settings.getSuccessSound();
            context.startService(new Intent(context, AlertSoundService.class));

            Database db = new Database(context);

            String difficulty = Settings.getDifficulty() == Difficulty.EASY ? "EASY" :  Settings.getDifficulty() == Difficulty.AVERAGE ? "AVERAGE" : "HARD";

            int level = db.getLevel("Jigsaw Puzzle", difficulty);

            int currentJigsawLevel = level;

            int maxLevel = Settings.getDifficulty() == Difficulty.EASY ? Global.SelectedPlace.getGuessTheWord().length :  Settings.getDifficulty() == Difficulty.AVERAGE ? Global.SelectedPlace.getGuessTheWord().length : Global.SelectedPlace.getGuessTheWord().length;

            if (level < maxLevel) {
                db.update("Jigsaw Puzzle", difficulty, ++level);
            }


        }//Toast.makeText(context, "YOU WIN!", Toast.LENGTH_SHORT).show();
    }

    public static void moveTiles(Context context, String direction, int position) {

        // Upper-left-corner tile
        if (position == 0) {

            if (direction.equals(right)) swap(context, position, 1);
            else if (direction.equals(down)) swap(context, position, COLUMNS);
            else Toast.makeText(context, "Invalid move", Toast.LENGTH_SHORT).show();

            // Upper-center tiles
        } else if (position > 0 && position < COLUMNS - 1) {
            if (direction.equals(left)) swap(context, position, -1);
            else if (direction.equals(down)) swap(context, position, COLUMNS);
            else if (direction.equals(right)) swap(context, position, 1);
            else Toast.makeText(context, "Invalid move", Toast.LENGTH_SHORT).show();

            // Upper-right-corner tile
        } else if (position == COLUMNS - 1) {
            if (direction.equals(left)) swap(context, position, -1);
            else if (direction.equals(down)) swap(context, position, COLUMNS);
            else Toast.makeText(context, "Invalid move", Toast.LENGTH_SHORT).show();

            // Left-side tiles
        } else if (position > COLUMNS - 1 && position < DIMENSIONS - COLUMNS &&
                position % COLUMNS == 0) {
            if (direction.equals(up)) swap(context, position, -COLUMNS);
            else if (direction.equals(right)) swap(context, position, 1);
            else if (direction.equals(down)) swap(context, position, COLUMNS);
            else Toast.makeText(context, "Invalid move", Toast.LENGTH_SHORT).show();

            // Right-side AND bottom-right-corner tiles
        } else if (position == COLUMNS * 2 - 1 || position == COLUMNS * 3 - 1) {
            if (direction.equals(up)) swap(context, position, -COLUMNS);
            else if (direction.equals(left)) swap(context, position, -1);
            else if (direction.equals(down)) {

                // Tolerates only the right-side tiles to swap downwards as opposed to the bottom-
                // right-corner tile.
                if (position <= DIMENSIONS - COLUMNS - 1) swap(context, position,
                        COLUMNS);
                else Toast.makeText(context, "Invalid move", Toast.LENGTH_SHORT).show();
            } else Toast.makeText(context, "Invalid move", Toast.LENGTH_SHORT).show();

            // Bottom-left corner tile
        } else if (position == DIMENSIONS - COLUMNS) {
            if (direction.equals(up)) swap(context, position, -COLUMNS);
            else if (direction.equals(right)) swap(context, position, 1);
            else Toast.makeText(context, "Invalid move", Toast.LENGTH_SHORT).show();

            // Bottom-center tiles
        } else if (position < DIMENSIONS - 1 && position > DIMENSIONS - COLUMNS) {
            if (direction.equals(up)) swap(context, position, -COLUMNS);
            else if (direction.equals(left)) swap(context, position, -1);
            else if (direction.equals(right)) swap(context, position, 1);
            else Toast.makeText(context, "Invalid move", Toast.LENGTH_SHORT).show();

            // Center tiles
        } else {
            if (direction.equals(up)) swap(context, position, -COLUMNS);
            else if (direction.equals(left)) swap(context, position, -1);
            else if (direction.equals(right)) swap(context, position, 1);
            else swap(context, position, COLUMNS);
        }
    }

    private static boolean isSolved() {
        boolean solved = false;

        for (int i = 0; i < tileList.length; i++) {
            if (tileList[i].equals(String.valueOf(i))) {
                solved = true;
            } else {
                solved = false;
                break;
            }
        }

        return solved;
    }

    // ******************************* TIMER *******************************//
    private static int time = 10000;
    private static  void startTimer() {
        if (countDownTimer != null) stopTimer();

        countDownTimer = new CountDownTimer(time * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                time = (int)millisUntilFinished / 1000;
                lblTime.setText(formatTimer(10000 - time));
            }

            @Override
            public void onFinish() {
//                mView.showMessage("Time is up!");
//                IdentificationPresenter.this.answer("");
//                stopTimer();
            }

        }.start();

    }

    public static void stopTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    private static String formatTimer(int time) {
        int minute = time / 60;
        int seconds = time % 60;
        String formattedTime = (minute < 10 ? "0" + String.valueOf(minute) : String.valueOf(minute)) + ":"
                + (seconds < 10 ? "0" + String.valueOf(seconds) : String.valueOf(seconds));
        return formattedTime;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }

    protected void onDestroy() {
        //stop service and stop music
        stopService(new Intent(PuzzlePictureActivity.this, SoundService.class));
        super.onDestroy();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }

    private static void playButtonClickSound() {
        if (Settings.isIsSoundOn()) {
            context.stopService(new Intent(context, ClickSoundService.class));
            context.startService(new Intent(context, ClickSoundService.class));
        }
    }
}
