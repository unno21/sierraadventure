package com.example.sierradventure;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sierradventure.Presenters.LevelSelectPresenter;
import com.example.sierradventure.Utilities.ClickSoundService;
import com.example.sierradventure.Utilities.Difficulty;
import com.example.sierradventure.Utilities.Settings;

import java.util.ArrayList;

public class LevelSelectActivity extends AppCompatActivity implements LevelSelectPresenter.View {

    private ArrayList<Button> btnLevels = new ArrayList<>();

    private LevelSelectPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_select);

        btnLevels.add(findViewById(R.id.btnLevel1));
        btnLevels.add(findViewById(R.id.btnLevel2));
        btnLevels.add(findViewById(R.id.btnLevel3));
        btnLevels.add(findViewById(R.id.btnLevel4));
        btnLevels.add(findViewById(R.id.btnLevel5));
        btnLevels.add(findViewById(R.id.btnLevel6));
        btnLevels.add(findViewById(R.id.btnLevel7));
        btnLevels.add(findViewById(R.id.btnLevel8));
        btnLevels.add(findViewById(R.id.btnLevel9));
        btnLevels.add(findViewById(R.id.btnLevel10));
        btnLevels.add(findViewById(R.id.btnLevel11));
        btnLevels.add(findViewById(R.id.btnLevel12));
        btnLevels.add(findViewById(R.id.btnLevel13));
        btnLevels.add(findViewById(R.id.btnLevel14));
        btnLevels.add(findViewById(R.id.btnLevel15));
        btnLevels.add(findViewById(R.id.btnLevel16));
        btnLevels.add(findViewById(R.id.btnLevel17));
        btnLevels.add(findViewById(R.id.btnLevel18));
        btnLevels.add(findViewById(R.id.btnLevel19));
        btnLevels.add(findViewById(R.id.btnLevel20));

        mPresenter = new LevelSelectPresenter(this);
        mPresenter.initiateDisplay();

        for (Button b : btnLevels) {
            b.setOnClickListener(v -> {
                playButtonClickSound();
                Button eventSender = (Button)v;
                int index = btnLevels.indexOf(eventSender);
                mPresenter.clickQuesttion(index);
            });
        }

        findViewById(R.id.btnLevelSelectHome).setOnClickListener(v -> {
            playButtonClickSound();
            Intent i = new Intent(LevelSelectActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        });
        findViewById(R.id.btnLevelSelectSettings).setOnClickListener(v -> {
            playButtonClickSound();
            Intent i = new Intent(LevelSelectActivity.this, SettingsActivity.class);
            startActivity(i);
        });
        findViewById(R.id.btnLevelSelectMap).setOnClickListener(v -> {
            playButtonClickSound();
            Intent i = new Intent(LevelSelectActivity.this, MapsActivity.class);
            startActivity(i);
            finish();
        });

        findViewById(R.id.btnLevelSelectBack).setOnClickListener(v -> {
            playButtonClickSound();
            finish();
        });
    }

    @Override
    public void showDifficultyTitle(String title) {
        TextView lblDifficulty = findViewById(R.id.lblDifficulty);
        lblDifficulty.setText(title);
    }

    @Override
    public void changeBackGroundLock(int index) {
        btnLevels.get(index).setBackgroundResource(R.drawable.ic_lock);
    }

    @Override
    public void changeBackGoundNumber(int index, int number) {
        btnLevels.get(index).setBackgroundResource(R.drawable.button_bg_4);
        btnLevels.get(index).setText(String.valueOf(number));
    }

    @Override
    public void hideQuestionNumber(int index) {
        btnLevels.get(index).setVisibility(View.GONE);
    }

    @Override
    public void showQuestionActivity() {
        Intent i = new Intent(LevelSelectActivity.this, QuizChallengeActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void showPicturePuzzleActivity() {

        if (Settings.getDifficulty() == Difficulty.EASY) {
            Intent i = new Intent(LevelSelectActivity.this, PuzzlePictureActivity.class);
            startActivity(i);
        } else if (Settings.getDifficulty() == Difficulty.AVERAGE) {
            Intent i = new Intent(LevelSelectActivity.this, PuzzlePictureAverage.class);
            startActivity(i);
        } else if (Settings.getDifficulty() == Difficulty.HARD) {
            Intent i = new Intent(LevelSelectActivity.this, PuzzlePictureHard.class);
            startActivity(i);
        }

        finish();
    }

    @Override
    public void showGuessTheWordActivity() {
        Intent i = new Intent(LevelSelectActivity.this, GuessPictureActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void setTitleImage(int image) {
        ImageView img = findViewById(R.id.imgTitle);
        img.setBackgroundResource(image);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(LevelSelectActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void playButtonClickSound() {
        if (Settings.isIsSoundOn()) {
            stopService(new Intent(LevelSelectActivity.this, ClickSoundService.class));
            startService(new Intent(LevelSelectActivity.this, ClickSoundService.class));
        }
    }
}
