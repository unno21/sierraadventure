package com.example.sierradventure;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sierradventure.Utilities.ClickSoundService;
import com.example.sierradventure.Utilities.Settings;

public class AboutActivity extends AppCompatActivity {

    Button btnCredits, btnDisclaimers, btnBack;
    Dialog creditsDialog, disclaimersDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        btnBack = findViewById(R.id.btnBack);
        btnCredits = findViewById(R.id.btnCredits);
        btnDisclaimers = findViewById(R.id.btnDisclaimers);

        creditsDialog = new Dialog(this);
        disclaimersDialog = new Dialog(this);


        btnCredits.setOnClickListener((v) -> {
            playButtonClickSound();
            creditsDialog.setContentView(R.layout.dialog_credits);
            creditsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            creditsDialog.show();

            TextView lblCredits = creditsDialog.findViewById(R.id.lblCredits);
            String text = "NSMNP information:\nMabuwaya Foundation Inc.\n\nJoni T Acay\nMerlijn Van Weerd\n\nSuggestions:\nDr. Amy Lyn M. Maddalora\nJose Dominic Lucas\nNoami A. Ubiña\n\nSierra Adventure Character Artist:\nAbigail Maddalora";
            lblCredits.setText(text);
            lblCredits.setMovementMethod(new ScrollingMovementMethod());

            Button btnOk = creditsDialog.findViewById(R.id.btnCreditsOk);
            btnOk.setOnClickListener( b -> {
                creditsDialog.dismiss();
            });
        });

        btnDisclaimers.setOnClickListener((v) -> {
            playButtonClickSound();
            disclaimersDialog.setContentView(R.layout.dialog_disclaimers);
            disclaimersDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            disclaimersDialog.show();

            TextView lblDisclaimers = disclaimersDialog.findViewById(R.id.lblDisclaimers);
            String text = "The species included in this mobile application are only limited to the information given by Mabuwaya Foundation Inc. The exact location of the animal species does not recognize by this application. The plants species, specification and picture of trees and other information about each town in NSMNP are not included. ";
            lblDisclaimers.setText(text);
            lblDisclaimers.setMovementMethod(new ScrollingMovementMethod());

            Button btnOk = disclaimersDialog.findViewById(R.id.btnDisclaimerOk);
            btnOk.setOnClickListener( b -> {
                disclaimersDialog.dismiss();
            });
        });

        btnBack.setOnClickListener((v) -> {
            playButtonClickSound();
            finish();
        });

    }

    private void playButtonClickSound() {
        if (Settings.isIsSoundOn()) {
            stopService(new Intent(AboutActivity.this, ClickSoundService.class));
            startService(new Intent(AboutActivity.this, ClickSoundService.class));
        }
    }
}
