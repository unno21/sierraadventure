package com.example.sierradventure;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sierradventure.Models.Animal;


public class AnimalTreesAdapter extends ArrayAdapter<Animal> {

    public AnimalTreesAdapter(PlaceInfoActivity context, Animal[] animTrees) {
        super(context, R.layout.custom_list_item, animTrees);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater cartInflater = LayoutInflater.from(getContext());
        View customView = cartInflater.inflate(R.layout.custom_list_item, parent, false);

        Animal animal = getItem(position);
        TextView txtEnglishName = (TextView) customView.findViewById(R.id.txtEnglishName);
        ImageView imgAnimalAdapter = customView.findViewById(R.id.picAnimTree);

        txtEnglishName.setText(animal.getEnglishName());
        imgAnimalAdapter.setBackgroundResource(animal.getImage());

        return customView;
    }

}
