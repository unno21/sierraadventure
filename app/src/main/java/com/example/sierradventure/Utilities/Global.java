package com.example.sierradventure.Utilities;

import android.speech.tts.TextToSpeech;

import com.example.sierradventure.Models.Animal;
import com.example.sierradventure.Models.Place;
import com.example.sierradventure.Models.Question;

import java.util.ArrayList;

public abstract class Global {

    public static ArrayList<Place> Places = new ArrayList();

    public static Place SelectedPlace;

    public static Animal SelectedAnimal;

    public static Question SelectedQuestion;

    public static int AlertSound;

    public static Mode Mode = com.example.sierradventure.Utilities.Mode.QUESTION;

    public static String SelectedTab = "animal";

    public static String SearchString = "";

    public static boolean IsFirstLoad = true;

    public static int SelectedLevel;

    public static TextToSpeech Tts;

}
