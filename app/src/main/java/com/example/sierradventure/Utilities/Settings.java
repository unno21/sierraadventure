package com.example.sierradventure.Utilities;

import com.example.sierradventure.R;

public class Settings {

    private static boolean isMusicOn = true;

    private static boolean isSoundOn = true;

    private static Difficulty difficulty = Difficulty.HARD;

    private static int backgroundMusic = R.raw.background_music;

    private static int successSound = R.raw.success;

    private static int failSound = R.raw.fail;

    private static int buttonClickSound = R.raw.click;

    private static int buttonClickSoundNew = R.raw.bclick;

    public static boolean isIsMusicOn() {
        return isMusicOn;
    }

    public static boolean isIsSoundOn() {
        return isSoundOn;
    }

    public static int getBackgroundMusic() {
        return backgroundMusic;
    }

    public static int getSuccessSound() {
        return successSound;
    }

    public static int getFailSound() {
        return failSound;
    }

    public static int getButtonClickSound() {
        return buttonClickSound;
    }

    public static int getButtonClickNewSound() {
        return buttonClickSoundNew;
    }

    public static Difficulty getDifficulty() { return difficulty; }

    public static void Music(boolean isOn) {
        isMusicOn = isOn;
    }

    public static void Sound(boolean isOn) {
        isSoundOn = isOn;
    }

    public static void Difficulty(Difficulty diff) { difficulty = diff; }

}

