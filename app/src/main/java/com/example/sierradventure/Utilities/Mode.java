package com.example.sierradventure.Utilities;

public enum Mode {
    QUESTION,
    PICTURE_PUZZLE,
    GUESS_THE_WORD
}
