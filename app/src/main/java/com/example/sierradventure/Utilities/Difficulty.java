package com.example.sierradventure.Utilities;

public enum Difficulty {

    EASY,
    AVERAGE,
    HARD

}
