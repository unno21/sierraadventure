package com.example.sierradventure.Utilities;

import android.content.Context;
import android.graphics.Point;
import android.view.WindowManager;

public class Screen {

    private static final int defaultWidth = 720; //720

    private static final int defaultHeight = 1440; //1440

    private int width;

    private int heigth;

    public Screen(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        wm.getDefaultDisplay().getRealSize(size);
        this.width = size.x;
        this.heigth = size.y;
    }

    public float getWidthInPercentage() {
        float difference = (this.heigth - defaultWidth);
        float differencePercent =  difference / defaultWidth;
        float newWidthPercentage = 1 + differencePercent;

        return newWidthPercentage;
    }

    public float getHeigthPercentage() {
        float difference = (this.heigth - defaultHeight);
        float differencePercent =  difference / defaultHeight;
        float newHeightPercentage = 1 + differencePercent;

        return newHeightPercentage;
    }

    public static int getDefaultWidth() {
        return defaultWidth;
    }

    public static int getDefaultHeight() {
        return defaultHeight;
    }

    public int getWidth() {
        return width;
    }

    public int getHeigth() {
        return heigth;
    }
}
