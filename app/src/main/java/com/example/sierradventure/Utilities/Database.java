package com.example.sierradventure.Utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class Database extends SQLiteOpenHelper {

    public static final String DB_NAME = "sierra.db";
    public static final String TABLE_NAME = "data";
    public static final String COL_ID = "id";
    public static final String COL_NAME = "name";
    public static final String COL_DIFFICULTY = "difficulty";
    public static final String COL_LEVEL = "level";

    public Database(Context context)
    {
        super(context, DB_NAME, null,1);
    }
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("create table " + TABLE_NAME +
                " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, " +
                "difficulty TEXT, " +
                "level INTEGER)");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        onCreate(db);
    }

    public boolean insert(String name, String difficulty, int level) {

        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues contentvalue = new ContentValues();
        contentvalue.put(COL_NAME, name);
        contentvalue.put(COL_DIFFICULTY, difficulty);
        contentvalue.put(COL_LEVEL, level);
        long result  = db.insert(TABLE_NAME,null, contentvalue);

        return result != -1;

    }

    public boolean update(String name, String difficulty, int level)
    {
        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues contentvalue = new ContentValues();
        contentvalue.put(COL_LEVEL, level);
        long result  = db.update(TABLE_NAME, contentvalue, "name='" + name +"' AND difficulty ='" + difficulty + "'", null);

        return result != -1;
    }

    public int getLevel(String name, String difficulty) {
        SQLiteDatabase db  = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select id, name, level, difficulty from "+ TABLE_NAME + " WHERE name = '" + name + "' AND " + COL_DIFFICULTY + " = '" + difficulty + "' ORDER BY " + COL_ID + " DESC",null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int level = cursor.getInt(2);

                return level;
            }
        }
        return 0;
    }

}
