package com.example.sierradventure.Utilities;

import java.util.ArrayList;
import java.util.Random;

public class CollectionHelper {

    public static ArrayList<String> RandomizeLetters(String includedLetters, int size) {

        ArrayList<String> randomizeLetters = new ArrayList<>();
        String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        includedLetters = includedLetters.toUpperCase();

        Random randomizer = new Random();

        /* Append random letter to includedLetters */
        for (int i = includedLetters.length(); i < size; i++) {
            int randomNumber = randomizer.nextInt(26);
            includedLetters += letters.charAt(randomNumber);
        }

        /* Randomize the letters */
        ArrayList<String> indexes = new ArrayList<>();
        while (randomizeLetters.size() < size) {

            int randomIndex = randomizer.nextInt(size);

            /* Check if index is already used */
            boolean isUsed = indexes.indexOf(String.valueOf(randomIndex)) > -1;

            if (!isUsed) {
                indexes.add(String.valueOf(randomIndex));
                String letter = String.valueOf(includedLetters.charAt(randomIndex));
                randomizeLetters.add(letter);
            }
        }

        return randomizeLetters;
    }

}
