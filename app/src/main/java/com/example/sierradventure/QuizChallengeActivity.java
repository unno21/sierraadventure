package com.example.sierradventure;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.sierradventure.Models.Question;
import com.example.sierradventure.Presenters.QuizChallengePresenter;
import com.example.sierradventure.R;
import com.example.sierradventure.Utilities.ClickSoundService;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Settings;

import java.util.ArrayList;

public class QuizChallengeActivity extends AppCompatActivity implements QuizChallengePresenter.View {

    Button btnAnswer1, btnAnswer2, btnAnswer3, btnAnswer4, btnBack;
    ArrayList<Button> btnAnswers = new ArrayList<>();
    Dialog dialogCorrect, dialogWrong;
    TextView txtQuestion;
    QuizChallengePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_challenge);

        txtQuestion = findViewById(R.id.txtQuestion);
        btnAnswers.add(findViewById(R.id.btnAnswer1));
        btnAnswers.add(findViewById(R.id.btnAnswer2));
        btnAnswers.add(findViewById(R.id.btnAnswer3));
        btnAnswers.add(findViewById(R.id.btnAnswer4));
        dialogCorrect = new Dialog(this);
        dialogWrong = new Dialog(this);

        mPresenter = new QuizChallengePresenter(this);
        mPresenter.initializeDisplay();

        for (Button b : btnAnswers) {
            b.setOnClickListener(v -> {
                Button btn = (Button)v;
                String answer = btn.getText().toString();
                mPresenter.answer(answer);
            });
        }

        btnBack = findViewById(R.id.btnQuizBack);
        btnBack.setOnClickListener( v -> {
            playButtonClickSound();
            finish();
        });

    }

    @Override
    public void showQuestion(Question question) {
        txtQuestion.setText(question.getText().toUpperCase());

        for (int i = 0; i < question.getChoices().length; i++) {
            btnAnswers.get(i).setText(question.getChoices()[i].toUpperCase());
        }

        for (int i = question.getChoices().length; i < 4; i++) {
            btnAnswers.get(i).setVisibility(View.GONE);
        }
    }

    @Override
    public void showCorrectDialog() {

        stopService(new Intent(QuizChallengeActivity.this, AlertSoundService.class));
        Global.AlertSound = Settings.getSuccessSound();
        startService(new Intent(QuizChallengeActivity.this, AlertSoundService.class));

        dialogCorrect.setContentView(R.layout.dialog_correct);
        dialogCorrect.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCorrect.show();

        ImageButton btnMap = dialogCorrect.findViewById(R.id.btnCorrectMap);
        btnMap.setOnClickListener(v -> {
//            startActivity(new Intent(QuizChallengeActivity.this, QuizChallengeActivity.class));
//            finish();
            mPresenter.initializeDisplay();
            dialogCorrect.hide();
        });

        ImageButton btnHome = dialogCorrect.findViewById(R.id.btnCorrectHome);
        btnHome.setOnClickListener(v -> {
            startActivity(new Intent(QuizChallengeActivity.this, HomeActivity.class));
            finish();
        });

        ImageButton btnSettings = dialogCorrect.findViewById(R.id.btnCorrectSettings);
        btnSettings.setOnClickListener(v -> {
            startActivity(new Intent(QuizChallengeActivity.this, LevelSelectActivity.class));
            finish();
        });

    }

    @Override
    public void showWrongDialog() {

        stopService(new Intent(QuizChallengeActivity.this, AlertSoundService.class));
        Global.AlertSound = Settings.getFailSound();
        startService(new Intent(QuizChallengeActivity.this, AlertSoundService.class));

        dialogWrong.setContentView(R.layout.dialog_wrong);
        dialogWrong.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogWrong.show();

        ImageButton btnMap = dialogWrong.findViewById(R.id.btnWrongMap);
        btnMap.setOnClickListener(v -> {
            startActivity(new Intent(QuizChallengeActivity.this, MapsActivity.class));
            finish();
        });

        ImageButton btnHome = dialogWrong.findViewById(R.id.btnWrongHome);
        btnHome.setOnClickListener(v -> {
            startActivity(new Intent(QuizChallengeActivity.this, HomeActivity.class));
            finish();
        });

        ImageButton btnRetry = dialogWrong.findViewById(R.id.btnWrongRetry);
        btnRetry.setOnClickListener(v -> {
            mPresenter.initializeDisplay();
            dialogWrong.hide();
        });

    }

    private void playButtonClickSound() {
        if (Settings.isIsSoundOn()) {
            stopService(new Intent(QuizChallengeActivity.this, ClickSoundService.class));
            startService(new Intent(QuizChallengeActivity.this, ClickSoundService.class));
        }
    }
}
