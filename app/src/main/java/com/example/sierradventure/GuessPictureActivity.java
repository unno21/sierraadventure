package com.example.sierradventure;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sierradventure.Presenters.GuessPicturePresenter;
import com.example.sierradventure.Utilities.ClickSoundService;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Screen;
import com.example.sierradventure.Utilities.Settings;

import java.util.ArrayList;

public class GuessPictureActivity extends AppCompatActivity implements GuessPicturePresenter.View {

    private ArrayList<Button> btnClues = new ArrayList<>();
    private ArrayList<Button> btnAnswers = new ArrayList<>();
    ImageView imgClue;
    private GuessPicturePresenter mPresenter;
    Dialog dialogCorrect;
    ImageButton btnBack;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess_picture);

        initComponents();
        mPresenter = new GuessPicturePresenter(this);
        mPresenter.initializeComponents();
        addEventListeners();
    }

    private void initComponents() {

        btnClues.add(findViewById(R.id.btn1));
        btnClues.add(findViewById(R.id.btn2));
        btnClues.add(findViewById(R.id.btn3));
        btnClues.add(findViewById(R.id.btn4));
        btnClues.add(findViewById(R.id.btn5));
        btnClues.add(findViewById(R.id.btn6));
        btnClues.add(findViewById(R.id.btn7));
        btnClues.add(findViewById(R.id.btn8));
        btnClues.add(findViewById(R.id.btn9));
        btnClues.add(findViewById(R.id.btn10));
        btnClues.add(findViewById(R.id.btn11));
        btnClues.add(findViewById(R.id.btn12));

        btnAnswers.add(findViewById(R.id.btnAns1));
        btnAnswers.add(findViewById(R.id.btnAns2));
        btnAnswers.add(findViewById(R.id.btnAns3));
        btnAnswers.add(findViewById(R.id.btnAns4));
        btnAnswers.add(findViewById(R.id.btnAns5));
        btnAnswers.add(findViewById(R.id.btnAns6));
        btnAnswers.add(findViewById(R.id.btnAns7));
        btnAnswers.add(findViewById(R.id.btnAns8));
        btnAnswers.add(findViewById(R.id.btnAns9));
        btnAnswers.add(findViewById(R.id.btnAns10));

        imgClue= findViewById(R.id.imgClue);

        dialogCorrect = new Dialog(this);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener( v -> {
            playButtonClickSound();
            finish();
            startActivity(new Intent(GuessPictureActivity.this, LevelSelectActivity.class));
        });

        btnSend = findViewById(R.id.btnSend);
        btnSend.setOnClickListener( v -> {
            mPresenter.checkAnswer();
            playButtonClickSound();
        });

        /*
            Resize the font base on screen resolution
         */
        Screen screen = new Screen(this);
        float screenPercentageHeightDifference = screen.getHeigthPercentage();

        if (screenPercentageHeightDifference < 1) {
            for (Button btn : btnAnswers) {
                float newTextSize = btn.getTextSize() * screenPercentageHeightDifference;
                float newTextSizeInSp = newTextSize / 2;
                btn.setTextSize(newTextSizeInSp - 6);

            }
            for (Button btn : btnClues) {
                float newTextSize = btn.getTextSize() * screenPercentageHeightDifference;
                float newTextSizeInSp = newTextSize / 2;
                btn.setTextSize(newTextSizeInSp);
            }

            float newTextSize = btnSend.getTextSize() * screenPercentageHeightDifference;
            float newTextSizeInSp = newTextSize / 2;
            btnSend.setTextSize(newTextSizeInSp - 3);
        }

    }

    private void addEventListeners() {
        for (Button b : btnClues) {
            b.setOnClickListener(v -> {
                Button eventSender = (Button)v;
                String letter = eventSender.getText().toString();
                int index = btnClues.indexOf(eventSender);
                mPresenter.answer(letter, index);
            });
        }

        for (Button b : btnAnswers) {
            b.setOnClickListener(v -> {
                Button eventSender = (Button)v;
                String letter = eventSender.getText().toString();
                int index = btnAnswers.indexOf(eventSender);
                mPresenter.returnAnswer(letter, index);
            });

        }
    }

    @Override
    public void hideAnswerButton(int left, int right) {
        //hide left side
        for (int i = 0; i < left; i++) {
            btnAnswers.get(i).setVisibility(View.GONE);
        }

        //hide right side
        for (int i = 1; i <= right; i++) {
            btnAnswers.get(btnAnswers.size() - i).setVisibility(View.GONE);
        }
    }

    @Override
    public void showAllAnswerButton() {
        for (Button b : btnAnswers) {
            b.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addAnswer(String text, int position) {
        btnAnswers.get(position).setText(text);
    }

    @Override
    public void removeAnswer(int position) {
        btnAnswers.get(position).setText("");
    }

    @Override
    public void enableLetter(int position) {
        btnClues.get(position).setEnabled(true);
    }

    @Override
    public void disableLetter(int position) {
        btnClues.get(position).setEnabled(false);
    }

    @Override
    public void loadImage(int image) {
        imgClue.setBackgroundResource(image);
    }

    @Override
    public void loadLetters(ArrayList<String> letters) {
        for (int i = 0; i < letters.size(); i++) {
            btnClues.get(i).setText(letters.get(i));
        }
    }

    @Override
    public void clearAnswer() {
        for (Button b : btnAnswers) {
            b.setText("");
        }
    }

    @Override
    public void showCorrectAlert(String message) {
        stopService(new Intent(GuessPictureActivity.this, AlertSoundService.class));
        Global.AlertSound = Settings.getSuccessSound();
        startService(new Intent(GuessPictureActivity.this, AlertSoundService.class));

        dialogCorrect.setContentView(R.layout.dialog_picgame_completed);
        dialogCorrect.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCorrect.show();

        TextView lblAnswer = dialogCorrect.findViewById(R.id.lblPicGameAnswer);
        lblAnswer.setText(message);

        ImageButton btnNext = dialogCorrect.findViewById(R.id.btnPicGameNext);
        ImageButton btnHome = dialogCorrect.findViewById(R.id.btnPicGameHome);
        ImageButton btnRetry = dialogCorrect.findViewById(R.id.btnPicGameRetry);

        btnNext.setOnClickListener(v -> {
//            startActivity(new Intent(GuessPictureActivity.this, MapsActivity.class));
//            finish();
            mPresenter.next();
            startActivity(new Intent(GuessPictureActivity.this, GuessPictureActivity.class));
            finish();
            dialogCorrect.dismiss();
        });
        btnHome.setOnClickListener(v -> {
            startActivity(new Intent(GuessPictureActivity.this, HomeActivity.class));
            finish();
        });
        btnRetry.setOnClickListener(v -> {
            startActivity(new Intent(GuessPictureActivity.this, GuessPictureActivity.class));
            finish();
            dialogCorrect.dismiss();
        });

    }

    @Override
    public void showMessage(String message) {

        Toast.makeText(GuessPictureActivity.this, message, Toast.LENGTH_SHORT).show();

    }

    private void playButtonClickSound() {
        if (Settings.isIsSoundOn()) {
            stopService(new Intent(GuessPictureActivity.this, ClickSoundService.class));
            startService(new Intent(GuessPictureActivity.this, ClickSoundService.class));
        }
    }
}
