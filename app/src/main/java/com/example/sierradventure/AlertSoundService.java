package com.example.sierradventure;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.session.MediaSession;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Settings;

public class AlertSoundService extends Service {

    MediaPlayer player;
    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    public void onCreate()
    {
        player = MediaPlayer.create(this, Global.AlertSound);
        player.setLooping(false); // Set looping
        player.setVolume(100,100);

    }
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return Service.START_NOT_STICKY;
    }
    public void onDestroy() {
        player.stop();
        player.release();
        stopSelf();
        super.onDestroy();
    }
    public void onStart(Intent intent,int startid){
        player.start();
    }
}
