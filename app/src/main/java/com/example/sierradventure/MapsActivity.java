package com.example.sierradventure;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.speech.tts.TextToSpeech;

import com.example.sierradventure.Models.Animal;
import com.example.sierradventure.Utilities.ClickSoundService;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Models.Place;
import com.example.sierradventure.Utilities.Settings;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    ImageButton btnSanMariano;
    TextToSpeech tts;
//    Dialog introDialog;

    ArrayList<ImageButton> btnPlaces = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);



        btnPlaces.add(findViewById(R.id.btnCabagan));
        btnPlaces.add(findViewById(R.id.btnDinapigue));
        btnPlaces.add(findViewById(R.id.btnDivilacan));
        btnPlaces.add(findViewById(R.id.btnIlagan));
        btnPlaces.add(findViewById(R.id.btnMaconacon));
        btnPlaces.add(findViewById(R.id.btnPalanan));
        btnPlaces.add(findViewById(R.id.btnSanMariano));
        btnPlaces.add(findViewById(R.id.btnSanPablo));
        btnPlaces.add(findViewById(R.id.btnTumauini));

        for (ImageButton b : btnPlaces) {
            b.setOnClickListener(v -> {
                playButtonClickSound();
                ImageButton imgButton = (ImageButton)v;
                int index = btnPlaces.indexOf(imgButton);
                Global.SelectedPlace = Global.Places.get(index);
//                startActivity(new Intent(MapsActivity.this, PlaceInfoActivity.class));
                startActivity(new Intent(MapsActivity.this, ZoomActivity.class));
            });
        }


//        if (isNetworkAvailable()) {
//            //hide custom marker buttons
//            for (ImageButton b : btnPlaces) {
//               b.setVisibility(View.GONE);
//            }
//        } else {
//
//            //hide google map fragment
//            mapFragment.getView().setVisibility(View.INVISIBLE);
//
//            for (ImageButton b : btnPlaces) {
//                b.setOnClickListener(v -> {
//                    ImageButton imgButton = (ImageButton)v;
//                    int index = btnPlaces.indexOf(imgButton);
//                    Global.SelectedPlace = Global.Places.get(index);
//                    startActivity(new Intent(MapsActivity.this, PlaceInfoActivity.class));
//                });
//            }
//        }

        tts = Global.Tts;
        tts.speak("Welcome Northern Sierra Madre Natural Park Map. Did you know that hunting, and agricultural activities that exacerbate the degraded state of much of the forest ecosystem. At the same time, logging and mining concessions have influenced the development of support industries in the lowlands to process timber and mineral products. When the forests are decreasing the population of the species is also decreasing. These illegal activities must be put to an end to preserve the natural resources, most especially the animal species and other living species in the park. Please, let us save our nature.", TextToSpeech.QUEUE_FLUSH, null);

//        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//                if (status == TextToSpeech.SUCCESS) {
//                    int ttsLang = tts.setLanguage(Locale.UK);
//
//                    tts.speak("Welcome Northern Sierra Madre Natural Park Map. Did you know that hunting, and agricultural activities that exacerbate the degraded state of much of the forest ecosystem. At the same time, logging and mining concessions have influenced the development of support industries in the lowlands to process timber and mineral products. When the forests are decreasing the population of the species is also decreasing. These illegal activities must be put to an end to preserve the natural resources, most especially the animal species and other living species in the park. Please, let us save our nature.", TextToSpeech.QUEUE_FLUSH, null);
//
//                    if (ttsLang == TextToSpeech.LANG_MISSING_DATA
//                            || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
//                        Log.e("TTS", "The Language is not supported!");
//                    } else {
//                        Log.i("TTS", "Language Supported.");
//                    }
//                    Log.i("TTS", "Initialization success.");
//                } else {
//                    Toast.makeText(getApplicationContext(), "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        ImageButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener( v -> {
            playButtonClickSound();
            startActivity(new Intent(MapsActivity.this, HomeActivity.class));
            finish();
        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (isNetworkAvailable()) {

            mMap = googleMap;

            for (Place p : Global.Places) {
                LatLng latLng = new LatLng(p.getLatitude(), p.getLongitude());
                String title = p.getName();
                String description = "Click to view information.";
                mMap.addMarker(new MarkerOptions().position(latLng).title(title).snippet(description).icon(BitmapDescriptorFactory.defaultMarker(p.getLegendColor())));
            }
//
//        //move the camera to the location of first place
            LatLng currentLatLng = new LatLng(Global.Places.get(3).getLatitude(), Global.Places.get(3).getLongitude());
            mMap.animateCamera(CameraUpdateFactory.zoomTo(5.0f));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(9), 2000, null);
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    //Toast.makeText(MapsActivity.this, marker.getTitle(), Toast.LENGTH_SHORT).show();
                    markerClick(marker.getTitle());
                    return true;
                }
            });

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    LinearLayout info = new LinearLayout(MapsActivity.this);
                    info.setOrientation(LinearLayout.VERTICAL);

                    TextView title = new TextView(MapsActivity.this);
                    title.setTextColor(Color.BLACK);
                    title.setGravity(Gravity.CENTER);
                    title.setTypeface(null, Typeface.BOLD);
                    title.setText(marker.getTitle());

                    TextView snippet = new TextView(MapsActivity.this);
                    snippet.setTextColor(Color.GRAY);
                    snippet.setText(marker.getSnippet());

                    info.addView(title);
                    info.addView(snippet);

                    return info;
                }
            });
        }
    }

    public void markerClick(String title) {

        for(Place p : Global.Places) {
            if (p.getName().equals(title)) {
                Global.SelectedPlace = p;
                startActivity(new Intent(MapsActivity.this, PlaceInfoActivity.class));
                break;
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }

    private void playButtonClickSound() {
        if (Settings.isIsSoundOn()) {
            stopService(new Intent(MapsActivity.this, ClickSoundService.class));
            startService(new Intent(MapsActivity.this, ClickSoundService.class));
        }
    }


}
