package com.example.sierradventure;

import android.content.Intent;
import android.graphics.Color;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sierradventure.Models.Animal;
import com.example.sierradventure.Utilities.Global;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.internal.GmsLogger;

import java.util.ArrayList;
import java.util.Locale;

public class PlaceInfoActivity extends AppCompatActivity {

    Button btnBack, btnTabAnimals, btnTabTrees, btnAnimalInformation;
    ImageButton btnSearchAnimal;
    EditText txtEnglishName, txtLocalName, txtScientificName, txtConservationStatus, txtSearchAnimal;
    ImageView imgAnimal;
    TextView txtListTitle, txtWelcomeGreeting;

    Animal[] animalArray;
    Animal[] treesArray;

    String selectedTab = "animal";

    TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_info);

        btnBack = findViewById(R.id.btnBack);
        btnTabAnimals = findViewById(R.id.btnTabAnimals);
        btnTabTrees = findViewById(R.id.btnTabTrees);
        btnSearchAnimal = findViewById(R.id.btnSearchAnimal);
        txtListTitle = findViewById(R.id.txtListTitle);
        txtWelcomeGreeting = findViewById(R.id.txtWelcomeGreeting);
        txtSearchAnimal = findViewById(R.id.txtSearchAnimal);

        animalArray = Global.SelectedPlace.getAnimals();
//        btnAnimalInformation = findViewById(R.id.btnAnimalInformation);
//
//        imgAnimal = findViewById(R.id.imgAnimal);
//        txtEnglishName = findViewById(R.id.txtEnglishName);
//        txtLocalName = findViewById(R.id.txtLocalName);
//        txtScientificName = findViewById(R.id.txtScientificName);
//        txtConservationStatus = findViewById(R.id.txtConservationStatus);

        txtWelcomeGreeting.setText(Global.SelectedPlace.getName());

        ListAdapter animalTreesAdapter = new AnimalTreesAdapter(this, Global.SelectedPlace.getAnimals());
        ListView animalListView = findViewById(R.id.lstAnimTree);
        animalListView.setAdapter(animalTreesAdapter);

//        btnAnimalInformation.setOnClickListener(v -> {
//            if (Global.SelectedAnimal != null)
//                startActivity(new Intent(PlaceInfoActivity.this, InformationActivity.class));
//        });

        animalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {
                Animal animal = animalArray[index];
                Global.SelectedAnimal = animal;
                startActivity(new Intent(PlaceInfoActivity.this, InformationActivity.class));
//                imgAnimal.setBackgroundResource(animal.getImage());
//                txtEnglishName.setText(animal.getEnglishName());
//                txtLocalName.setText(animal.getLocalName());
//                txtScientificName.setText(animal.getScientificName());
//                txtConservationStatus.setText(animal.getConservationStatus());
            }

        });

        btnBack.setOnClickListener((v) -> {
            finish();
        });

        btnTabAnimals.setOnClickListener((v) -> {

            /*
                Reset animal array, clear search box
             */
            selectedTab = "animal";
            Global.SelectedTab = selectedTab;
            animalArray = Global.SelectedPlace.getAnimals();
            txtSearchAnimal.setText("");

            btnTabAnimals.setBackgroundColor(Color.parseColor("#68B92D"));
            btnTabTrees.setBackgroundColor(Color.parseColor("#72CB31"));
            txtListTitle.setText("List of Animals");
            ListAdapter animalAdapter = new AnimalTreesAdapter(this, animalArray);
            animalListView.setAdapter(animalAdapter);

            animalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {

                    Animal animal = animalArray[index];
                    Global.SelectedAnimal = animal;
                    startActivity(new Intent(PlaceInfoActivity.this, InformationActivity.class));
//                    imgAnimal.setBackgroundResource(animal.getImage());
//                    txtEnglishName.setText(animal.getEnglishName());
//                    txtLocalName.setText(animal.getLocalName());
//                    txtScientificName.setText(animal.getScientificName());
//                    txtConservationStatus.setText(animal.getConservationStatus());
                }

            });
        });

        btnTabTrees.setOnClickListener((v) -> {


            /*
                Reset animal array, clear search box
             */
            selectedTab = "tree";
            Global.SelectedTab = selectedTab;
            treesArray = Global.SelectedPlace.getTrees();
            txtSearchAnimal.setText("");

            btnTabTrees.setBackgroundColor(Color.parseColor("#68B92D"));
            btnTabAnimals.setBackgroundColor(Color.parseColor("#72CB31"));
            txtListTitle.setText("List of Trees");
            ListAdapter treeListView = new AnimalTreesAdapter(this, treesArray);
            animalListView.setAdapter(treeListView);

            animalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {

                    Animal animal = treesArray[index];
                    Global.SelectedAnimal = animal;
                    startActivity(new Intent(PlaceInfoActivity.this, InformationActivity.class));
//                    imgAnimal.setBackgroundResource(animal.getImage());
//                    txtEnglishName.setText(animal.getEnglishName());
//                    txtLocalName.setText(animal.getLocalName());
//                    txtScientificName.setText(animal.getScientificName());
//                    txtConservationStatus.setText(animal.getConservationStatus());
                }

            });
        });

        txtSearchAnimal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String keyword = txtSearchAnimal.getText().toString().toUpperCase();
                ArrayList<Animal> lstAnimals = new ArrayList<>();

                //filter
                if (selectedTab.equals("animal")) {
                    for (Animal animal : Global.SelectedPlace.getAnimals()) {
                        if (animal.getEnglishName().toUpperCase().startsWith(keyword)) {
                            lstAnimals.add(animal);
                        }
                    }
                } else {
                    for (Animal animal : Global.SelectedPlace.getTrees()) {
                        if (animal.getEnglishName().toUpperCase().startsWith(keyword)) {
                            lstAnimals.add(animal);
                        }
                    }
                }


                animalArray = lstAnimals.toArray(new Animal[0]);

                ListAdapter animalAdapter = new AnimalTreesAdapter(PlaceInfoActivity.this, animalArray);
                animalListView.setAdapter(animalAdapter);

                animalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {


                        Animal animal;

                        if (selectedTab.equals("animal")) {
                            animal = animalArray[index];
                        } else {
                            animal = treesArray[index];
                        }

                        Global.SelectedAnimal = animal;
                        startActivity(new Intent(PlaceInfoActivity.this, InformationActivity.class));
//                    imgAnimal.setBackgroundResource(animal.getImage());
//                    txtEnglishName.setText(animal.getEnglishName());
//                    txtLocalName.setText(animal.getLocalName());
//                    txtScientificName.setText(animal.getScientificName());
//                    txtConservationStatus.setText(animal.getConservationStatus());
                    }

                });
            }
        });

        btnSearchAnimal.setOnClickListener( v -> {

//            startActivity(new Intent(PlaceInfoActivity.this, ZoomActivity.class));

//            String keyword = txtSearchAnimal.getText().toString().toUpperCase();
//            ArrayList<Animal> lstAnimals = new ArrayList<>();
//
//            //filter
//            if (selectedTab.equals("animal")) {
//                for (Animal animal : Global.SelectedPlace.getAnimals()) {
//                    if (animal.getEnglishName().toUpperCase().startsWith(keyword)) {
//                        lstAnimals.add(animal);
//                    }
//                }
//            } else {
//                for (Animal animal : Global.SelectedPlace.getTrees()) {
//                    if (animal.getEnglishName().toUpperCase().startsWith(keyword)) {
//                        lstAnimals.add(animal);
//                    }
//                }
//            }
//
//
//            animalArray = lstAnimals.toArray(new Animal[0]);
//
//            ListAdapter animalAdapter = new AnimalTreesAdapter(this, animalArray);
//            animalListView.setAdapter(animalAdapter);
//
//            animalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//                @Override
//                public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {
//
//
//                    Animal animal;
//
//                    if (selectedTab.equals("animal")) {
//                        animal = animalArray[index];
//                    } else {
//                        animal = treesArray[index];
//                    }
//
//                    Global.SelectedAnimal = animal;
//                    startActivity(new Intent(PlaceInfoActivity.this, InformationActivity.class));
////                    imgAnimal.setBackgroundResource(animal.getImage());
////                    txtEnglishName.setText(animal.getEnglishName());
////                    txtLocalName.setText(animal.getLocalName());
////                    txtScientificName.setText(animal.getScientificName());
////                    txtConservationStatus.setText(animal.getConservationStatus());
//                }
//
//            });

        });

//        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//                if (status == TextToSpeech.SUCCESS) {
//                    int ttsLang = tts.setLanguage(Locale.UK);
//
////                    String strCount = String.valueOf(Global.SelectedPlace.getAnimals().length);
////                    String placeName = Global.SelectedPlace.getName();
////                    String toSpeak = "There are " + strCount + " animal species with the town of " + placeName;
////                    tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
//
//                    if (ttsLang == TextToSpeech.LANG_MISSING_DATA
//                            || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
//                        Log.e("TTS", "The Language is not supported!");
//                    } else {
//                        Log.i("TTS", "Language Supported.");
//                    }
//                    Log.i("TTS", "Initialization success.");
//                } else {
//                    Toast.makeText(getApplicationContext(), "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });



        /*
            ONLOAD, show filtered animals
         */

        String keyword = Global.SearchString.toUpperCase();
        ArrayList<Animal> lstAnimals = new ArrayList<>();

        //filter
        if (selectedTab.equals("animal")) {
            for (Animal animal : Global.SelectedPlace.getAnimals()) {
                if (animal.getEnglishName().toUpperCase().startsWith(keyword)) {
                    lstAnimals.add(animal);
                }
            }
        } else {
            for (Animal animal : Global.SelectedPlace.getTrees()) {
                if (animal.getEnglishName().toUpperCase().startsWith(keyword)) {
                    lstAnimals.add(animal);
                }
            }
        }


        animalArray = lstAnimals.toArray(new Animal[0]);

        ListAdapter animalAdapter = new AnimalTreesAdapter(PlaceInfoActivity.this, animalArray);
        animalListView.setAdapter(animalAdapter);

        animalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {


                Animal animal;

                if (selectedTab.equals("animal")) {
                    animal = animalArray[index];
                } else {
                    animal = treesArray[index];
                }

                Global.SelectedAnimal = animal;
                startActivity(new Intent(PlaceInfoActivity.this, InformationActivity.class));
//                    imgAnimal.setBackgroundResource(animal.getImage());
//                    txtEnglishName.setText(animal.getEnglishName());
//                    txtLocalName.setText(animal.getLocalName());
//                    txtScientificName.setText(animal.getScientificName());
//                    txtConservationStatus.setText(animal.getConservationStatus());
            }

        });



    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }

}
