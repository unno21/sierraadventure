package com.example.sierradventure;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.sierradventure.Utilities.Settings;

public class SettingsActivity extends AppCompatActivity {

    Button btnBack;

    ImageButton btnSound, btnMusic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        btnBack = findViewById(R.id.btnBack);
        btnSound = findViewById(R.id.btnSound);
        btnMusic = findViewById(R.id.btnMusic);


        if (Settings.isIsMusicOn()) {
            btnMusic.setBackgroundResource(R.drawable.ic_music_on);
        } else {
            btnMusic.setBackgroundResource(R.drawable.ic_music_off);
        }

        if (Settings.isIsSoundOn()) {
            btnSound.setBackgroundResource(R.drawable.ic_sound_on);
        } else {
            btnSound.setBackgroundResource(R.drawable.ic_sound_off);
        }



        btnSound.setOnClickListener(v -> {
            if (Settings.isIsSoundOn()) {
                Settings.Sound(false);
                btnSound.setBackgroundResource(R.drawable.ic_sound_off);
            } else {
                Settings.Sound(true);
                btnSound.setBackgroundResource(R.drawable.ic_sound_on);
            }
        });

        btnMusic.setOnClickListener(v -> {
            if (Settings.isIsMusicOn()) {
                Settings.Music(false);
                btnMusic.setBackgroundResource(R.drawable.ic_music_off);
                stopService(new Intent(SettingsActivity.this, SoundService.class));
            } else {
                Settings.Music(true);
                btnMusic.setBackgroundResource(R.drawable.ic_music_on);
                startService(new Intent(SettingsActivity.this, SoundService.class));
            }
        });



        btnBack.setOnClickListener((v) -> {
            finish();
        });
    }

    protected void onDestroy() {
        //stop service and stop music
        stopService(new Intent(SettingsActivity.this, SoundService.class));
        super.onDestroy();
    }

}
