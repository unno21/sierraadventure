package com.example.sierradventure;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sierradventure.Utilities.ClickSoundService;
import com.example.sierradventure.Utilities.Database;
import com.example.sierradventure.Utilities.Difficulty;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Mode;
import com.example.sierradventure.Utilities.Settings;

public class GameModeActivity extends AppCompatActivity {

    ImageButton btnQuizChallenge, btnPicPuzzle, btnGuessPic, btnHelp, btnMap, btnHome, btnBack;

    Dialog difficultyDialog;

    Dialog insstructionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_mode);

        btnQuizChallenge = findViewById(R.id.btnQuizChallenge);
        btnPicPuzzle = findViewById(R.id.btnPicPuzzle);
        btnGuessPic = findViewById(R.id.btnGuessPic);
        btnHelp = findViewById(R.id.btnHelp);
        btnMap = findViewById(R.id.btnGMMap);
        btnHome = findViewById(R.id.btnGMHome);
        btnBack = findViewById(R.id.btnGameModeBack);


        btnQuizChallenge.setOnClickListener((v) -> {
            Global.Mode = Mode.QUESTION;
            showInstrucitonDialog("Quiz");
//            showDifficultyDialog();
//            Intent i = new Intent(GameModeActivity.this, LevelSelectActivity.class);
//            startActivity(i);
        });

        btnPicPuzzle.setOnClickListener((v) -> {
            Global.Mode = Mode.PICTURE_PUZZLE;
            showInstrucitonDialog("Jigsaw");
//            showDifficultyDialog();
        });
//
        btnGuessPic.setOnClickListener((v) -> {
            Global.Mode = Mode.GUESS_THE_WORD;
            showInstrucitonDialog("Guess");
//            showDifficultyDialog();
        });

        btnHelp.setOnClickListener((v) -> {
            playButtonClickSound();
            Intent i = new Intent(GameModeActivity.this, HelpActivity.class);
            startActivity(i);
        });

        btnMap.setOnClickListener( v -> {
            playButtonClickSound();
            Intent i = new Intent(GameModeActivity.this, MapsActivity.class);
            startActivity(i);
        });

        btnHome.setOnClickListener( v -> {
            playButtonClickSound();
            Intent i = new Intent(GameModeActivity.this, HomeActivity.class);
            startActivity(i);
        });

        btnBack.setOnClickListener( v -> {
            playButtonClickSound();
            finish();
        });

    }

    private void showInstrucitonDialog(String type) {

        String instruction = "";
        if (type.equals("Quiz")) {
            instruction = "1. Choose the letter of your answer; base your answer from what you learned about the animal species inside the NSMNP. ";
        } else if (type.equals("Jigsaw")) {
            instruction = "1. You will be presented with a scrambled version of the picture.\n2. Reassemble the picture puzzle by swapping pieces to matching the animal picture that is shown above.";
        } else if (type.equals("Guess")) {
            instruction = "1. You need to recall the animals and guess what animal name is shown on the picture.\n2. Below the pictures, you will see blank spaces indicating how many letters are in the answer.\n3. Top on these letters to spell out the word you believe to be the answer and click send button.";
        }

        insstructionDialog = new Dialog(this);
        insstructionDialog.setContentView(R.layout.dialog_help);
        insstructionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        insstructionDialog.show();

        TextView lblHelp = insstructionDialog.findViewById(R.id.lblHelp);
        lblHelp.setText(instruction);
        lblHelp.setMovementMethod(new ScrollingMovementMethod());

        Button btnHelp = insstructionDialog.findViewById(R.id.btnHelpOk);
        btnHelp.setOnClickListener( b -> {
            insstructionDialog.dismiss();
            showDifficultyDialog();
        });
    }

    private void showDifficultyDialog() {
        difficultyDialog = new Dialog(this);

        difficultyDialog.setContentView(R.layout.difficulty_dialog);
        difficultyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        difficultyDialog.show();

        Button btnEasy = difficultyDialog.findViewById(R.id.btnEasy);
        Button btnAverage = difficultyDialog.findViewById(R.id.btnAverage);
        Button btnHard = difficultyDialog.findViewById(R.id.btnHard);
        ImageView btnClose = difficultyDialog.findViewById(R.id.imgClose);



        /*
            Disable difficulty buttons
         */

        Database db = new Database(GameModeActivity.this);
        if (Global.Mode == Mode.GUESS_THE_WORD) {
            int maxAttainedLevelEasy = db.getLevel("Guess the word", "EASY");
            int totalLevelEasy =  Global.SelectedPlace.getGuessTheWord().length;
            totalLevelEasy = totalLevelEasy > 20 ? 20 : totalLevelEasy;
            if (maxAttainedLevelEasy < totalLevelEasy) {
                //disable INTERMEDIATE
                btnAverage.setEnabled(false);
                btnAverage.setBackgroundResource(R.drawable.button_bg_disable);
            }

            int maxAttainedLevelIntermediate = db.getLevel("Guess the word", "AVERAGE");
            int totalLevelIntermediate =  Global.SelectedPlace.getGuessTheWord().length;
            totalLevelIntermediate = totalLevelIntermediate > 20 ? 20 : totalLevelIntermediate;
            if (maxAttainedLevelIntermediate < totalLevelIntermediate) {
                //disable HARD
                btnHard.setEnabled(false);
                btnHard.setBackgroundResource(R.drawable.button_bg_disable);
            }

        } else if (Global.Mode == Mode.PICTURE_PUZZLE) {
            int maxAttainedLevelEasy = db.getLevel("Jigsaw Puzzle", "EASY");
            int totalLevelEasy =  Global.SelectedPlace.getJigsawPuzzle().length;
            totalLevelEasy = totalLevelEasy > 20 ? 20 : totalLevelEasy;
            if (maxAttainedLevelEasy < totalLevelEasy) {
                //disable INTERMEDIATE
                btnAverage.setEnabled(false);
                btnAverage.setBackgroundResource(R.drawable.button_bg_disable);
            }

            int maxAttainedLevelIntermediate = db.getLevel("Jigsaw Puzzle", "AVERAGE");
            int totalLevelIntermediate =  Global.SelectedPlace.getInterMediateJigsawPuzzle().length;
            totalLevelIntermediate = totalLevelIntermediate > 20 ? 20 : totalLevelIntermediate;
            if (maxAttainedLevelIntermediate < totalLevelIntermediate) {
                //disable HARD
                btnHard.setEnabled(false);
                btnHard.setBackgroundResource(R.drawable.button_bg_disable);
            }
        } else if (Global.Mode == Mode.QUESTION) {
            int maxAttainedLevelEasy = db.getLevel("Quiz", "EASY");

            int totalLevelEasy =  Global.SelectedPlace.getQuestions().length;

            totalLevelEasy = totalLevelEasy > 20 ? 20 : totalLevelEasy;

            if (maxAttainedLevelEasy < totalLevelEasy) {
                //disable INTERMEDIATE
                btnAverage.setEnabled(false);
                btnAverage.setBackgroundResource(R.drawable.button_bg_disable);
            }

            int maxAttainedLevelIntermediate = db.getLevel("Quiz", "AVERAGE");
            int totalLevelIntermediate =  Global.SelectedPlace.getIntermediateQuestions().length;

            totalLevelIntermediate = totalLevelIntermediate > 20 ? 20 : totalLevelIntermediate;

            if (maxAttainedLevelIntermediate < totalLevelIntermediate) {
                //disable HARD
                btnHard.setEnabled(false);
                btnHard.setBackgroundResource(R.drawable.button_bg_disable);
            }
        }

        btnEasy.setOnClickListener(v -> {
            playButtonClickSound();
            Settings.Difficulty(Difficulty.EASY);
            showLevelSelectActivity();
            difficultyDialog.dismiss();
        });
        btnAverage.setOnClickListener(v -> {
            playButtonClickSound();
            Settings.Difficulty(Difficulty.AVERAGE);
            showLevelSelectActivity();
            difficultyDialog.dismiss();
        });
        btnHard.setOnClickListener(v -> {
            playButtonClickSound();
            Settings.Difficulty(Difficulty.HARD);
            showLevelSelectActivity();
            difficultyDialog.dismiss();
        });
        btnClose.setOnClickListener( v -> {
            playButtonClickSound();
            difficultyDialog.dismiss();
        });
    }

    private void showLevelSelectActivity() {
        Intent i = new Intent(GameModeActivity.this, LevelSelectActivity.class);
        startActivity(i);
    }

    private void playButtonClickSound() {
        if (Settings.isIsSoundOn()) {
            stopService(new Intent(GameModeActivity.this, ClickSoundService.class));
            startService(new Intent(GameModeActivity.this, ClickSoundService.class));
        }
    }
}
