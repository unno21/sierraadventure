package com.example.sierradventure;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sierradventure.Models.Animal;
import com.example.sierradventure.Models.GuessTheWord;
import com.example.sierradventure.Models.JigsawPuzzle;
import com.example.sierradventure.Models.Place;
import com.example.sierradventure.Models.Question;
import com.example.sierradventure.Models.Tree;
import com.example.sierradventure.Utilities.ClickSoundService;
import com.example.sierradventure.Utilities.Database;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Screen;
import com.example.sierradventure.Utilities.Settings;
import com.example.sierradventure.SoundService;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.Locale;

import pl.droidsonroids.gif.GifImageView;

public class HomeActivity extends AppCompatActivity {

    ImageButton btnStart, btnGameMode, btnSettings, btnAbout, btnSearchAnimal;
    TextToSpeech tts;
    Dialog introDialog, searchDialog;
    GifImageView gifFairy;
    Place allAnimals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnStart = findViewById(R.id.btnStart);
        btnGameMode = findViewById(R.id.btnGameMode);
        btnSettings = findViewById(R.id.btnSettings);
        btnAbout = findViewById(R.id.btnAbout);
//        btnSearchAnimal = findViewById(R.id.btnSearchAnimal);

        gifFairy = findViewById(R.id.gifFairy);

//        btnSearchAnimal.setOnClickListener(v -> {
//            searchDialog = new Dialog(this);
//            searchDialog.setContentView(R.layout.dialog_search);
//            searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            searchDialog.show();
//
//            Global.SelectedPlace = allAnimals;
//
//            Button btnSearch = searchDialog.findViewById(R.id.btnDialogSearchAnimal);
//            EditText txtSearchAnimal = searchDialog.findViewById(R.id.txtDialogSearchAnimal);
//
//            btnSearch.setOnClickListener( b -> {
//                Global.SearchString = txtSearchAnimal.getText().toString();
//                searchDialog.dismiss();
//
//                Intent i = new Intent(HomeActivity.this, PlaceInfoActivity.class);
//                startActivity(i);
//
//            });
//
//        });

        btnStart.setOnClickListener(v -> {
            playButtonClickSound();
            Intent i = new Intent(HomeActivity.this, MapsActivity.class);
            startActivity(i);
        });

        btnGameMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playButtonClickSound();
                Global.SelectedPlace = Global.Places.get(0);
                Intent i = new Intent(HomeActivity.this, GameModeActivity.class);
                startActivity(i);
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playButtonClickSound();
                Intent i = new Intent(HomeActivity.this, SettingsActivity.class);
                startActivity(i);
            }
        });

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playButtonClickSound();
                Intent i = new Intent(HomeActivity.this, AboutActivity.class);
                startActivity(i);
            }
        });

        if (Global.Places.size() == 0) {
            initializeData();
        }

        if (Settings.isIsMusicOn()) {
            //start service and play music
            startService(new Intent(HomeActivity.this, SoundService.class));
        }

        Database db = new Database(this);
        int jigsawLevel = db.getLevel("Jigsaw Puzzle", "EASY");
        if (jigsawLevel == 0) {
            //insert data to database
            String[] difficulties = new String[] { "EASY", "AVERAGE", "HARD"};

            for (String difficulty : difficulties) {
                db.insert("Jigsaw Puzzle", difficulty, 1);
                db.insert("Guess the word", difficulty, 1);
                db.insert("Quiz", difficulty, 1);
            }

        }

        //        --------------------------------INTRO DIALOG---------------------------------

        if (Global.IsFirstLoad) {
            introDialog = new Dialog(this);

            introDialog.setContentView(R.layout.dialog_intro);
            introDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


            TextView lblIntro = introDialog.findViewById(R.id.lblIntro);
            Button btnSkip = introDialog.findViewById(R.id.btnSkip);
            lblIntro.setMovementMethod(new ScrollingMovementMethod());
            lblIntro.setText("The Northern Sierra Madre Natural Park (NSMNP) is one of the largest protected areas in the Philippines. It protects the mid-section of the Northern Sierra Madre range in the eastern part of Isabela Province in Northeast Luzon forming a natural shield from strong typhoons. It is also a globally important area for biological and cultural diversity. The NSMP is one of the top priority protected areas in the country as it supports a large number of ecosystems and endemic species. It provides important ecosystem services to people. The park has a total area of 359,486 hectares including 287,861 hectares of terrestrial ecosystems and 71,625 of coastal waters. It covers 9 municipalities of Isabela: Divilacan, Maconacon, Palanan entirely and parts of San Pablo, Cabagan, Tumauini, Ilagan, San Mariano, and Dinapigue. ");
            btnSkip.setOnClickListener(v -> {
                playButtonClickSound();
                introDialog.dismiss();
                if (tts != null) {
                    tts.stop();
//                    tts.shutdown();
                }
            });

            introDialog.show();
            Global.IsFirstLoad = false;
        }


//        --------------------------------------------------------------------------------

        if (Global.Tts == null) {
            tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        int ttsLang = tts.setLanguage(Locale.UK);

                        tts.speak("The Northern Sierra Madre Natural Park (NSMNP) is one of the largest protected areas in the Philippines. It protects the mid-section of the Northern Sierra Madre range in the eastern part of Isabela Province in Northeast Luzon forming a natural shield from strong typhoons. It is also a globally important area for biological and cultural diversity. The NSMNP is one of the top priority protected areas in the country as it supports a large number of ecosystems and endemic species. It provides important ecosystem services to people. The park has a total area of 359,486 hectares including 287,861 hectares of terrestrial ecosystems and 71,625 of coastal waters. It covers 9 municipalities of Isabela: Divilacan, Maconacon, Palanan entirely and parts of San Pablo, Cabagan, Tumauini, Ilagan, San Mariano, and Dinapigue. ", TextToSpeech.QUEUE_FLUSH, null);

                        if (ttsLang == TextToSpeech.LANG_MISSING_DATA
                                || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
                            Log.e("TTS", "The Language is not supported!");
                        } else {
                            Log.i("TTS", "Language Supported.");
                        }
                        Log.i("TTS", "Initialization success.");
                    } else {
                        Toast.makeText(getApplicationContext(), "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            Global.Tts = tts;
        } else {
            tts = Global.Tts;
            tts.speak("The Northern Sierra Madre Natural Park (NSMNP) is one of the largest protected areas in the Philippines. It protects the mid-section of the Northern Sierra Madre range in the eastern part of Isabela Province in Northeast Luzon forming a natural shield from strong typhoons. It is also a globally important area for biological and cultural diversity. The NSMNP is one of the top priority protected areas in the country as it supports a large number of ecosystems and endemic species. It provides important ecosystem services to people. The park has a total area of 359,486 hectares including 287,861 hectares of terrestrial ecosystems and 71,625 of coastal waters. It covers 9 municipalities of Isabela: Divilacan, Maconacon, Palanan entirely and parts of San Pablo, Cabagan, Tumauini, Ilagan, San Mariano, and Dinapigue. ", TextToSpeech.QUEUE_FLUSH, null);
        }




//        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
////        Point size = new Point();
////        wm.getDefaultDisplay().getRealSize(size);
////        String resolution = size.x + "x" + size.y;
////        //720 x 1440
////
////        Toast.makeText(this, resolution, Toast.LENGTH_LONG).show();
//        Screen s = new Screen(this);
//        Toast.makeText(this, "Height : " + s.getHeigthPercentage() + " Width : " + s.getWidthInPercentage(), Toast.LENGTH_LONG).show();

    }



    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }

    protected void onDestroy() {
        //stop service and stop music
        stopService(new Intent(HomeActivity.this, SoundService.class));
        super.onDestroy();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
    }

    private void playButtonClickSound() {
        if (Settings.isIsSoundOn()) {
            stopService(new Intent(HomeActivity.this, ClickSoundService.class));
            startService(new Intent(HomeActivity.this, ClickSoundService.class));
        }
    }

    private void initializeData() {
        /*********************************************************************************/


        allAnimals = new Place("Siera Madre",
                new Animal[]{
                        new Animal(R.drawable.cabagan_animal_12,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),
                        new Animal(R.drawable.divilacan_animal_17,
                                "Bottlenose dolphins have the second largest encephalization levels of any mammal on Earth (humans have the largest), sharing close ratios with those of humans and other great apes, which more than likely contributes to their high intelligence and emotional intelligence.",
                                "Bottle-Nosed Dolphin",
                                " ",
                                "Tursiops Truncatus",
                                "Endangered",
                                "ROBERT PERRY"
                        ),
                        new Animal(R.drawable.divilacan_animal_20,
                                "The celestial monarch is a species of bird in the family Monarchidae, and one of the most attractive of all the monarch flycatchers, with its spectacular blue crest and large yellow eye-ring (neither of which are illustrated in the facing painting). It is endemic to the Philippines.",
                                "Celestial Monarch",
                                " ",
                                "Hypothymis Coelestis",
                                "Vulnerable",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.maconacon_animal_17,
                                "The Hawksbill Sea Turtle can be found only in tropical seas. It has overlapping scutes on its shell. It has a narrow head and a hooked beak shaped like the bill of a hawk. They live in coral reefs and eat sponges.",
                                "Hawksbill Sea Turtle",
                                "Pawikan, Karahan",
                                "Eretmochelys Imbricata",
                                "Critically Endangered",
                                "NICK CALOYIANIS"
                        ),
                        new Animal(R.drawable.dinapigue_animal_13,
                                "The Large flying fox also known as the greater flying fox, Malaysian flying fox, large fruit bat, kalang or kalong, is a species of megabat in the Pteropodidae family. Like the other members of the genus Pteropus, or the Old World fruit bats, it feeds exclusively on fruits, nectar and flowers. It is noted for being one of the largest bats. It, as with all other Old World fruit bats, lacks the ability to echolocate.",
                                "Large Flying Fox",
                                " ",
                                "Pteropus Vampyrus",
                                "Near Threatened",
                                "FLETCHER & BAYLIS"
                        ),
                        new Animal(R.drawable.divilacan_animal_4,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),
                        new Animal(R.drawable.cabagan_animal_6,
                                "With its rusty-cinnamon head and bluish-grey bill, the Philippine duck is a rather distinctive bird. The cinnamon coloured head is boldly decorated with a black crown and a black stripe through the eye, while the rest of the plumage is brownish-grey. When in flight, a well-defined patch of glossy green on the wing can be clearly seen, which is bordered with black and has a narrow white edge, and the underside of the wing is also white. Immature Philippine ducks have slightly duller plumage than that of adults, while ducklings are olive-brown with a bright yellow face and neck. It calls with a typical duck-like quack.",
                                "Philippine Duck",
                                " ",
                                "Anas Luzonica",
                                "Vulnerable",
                                "E.G PEIKER"
                        ),
                        new Animal(R.drawable.cabagan_animal_11,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),
                        new Animal(R.drawable.san_mariano_animal_14,
                                "The Bitatawa is a giant fruit-eating monitor lizard. It lives in the lowland forest of northern Luzon. It has golden-yellow spots on its body and large strong claws to climb trees. It can grow up to 6 feet and can weigh up to 10 kg.",
                                "Bitatawa",
                                "Bitatawa, Batikaw",
                                "Varanus Bitatawa",
                                "Data Deficient",
                                "BRIAN SANTOS"
                        ),
                        new Animal(R.drawable.san_pablo_animal_3,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),
                        new Animal(R.drawable.dinapigue_animal_14,
                                "The Green Sea Turtle can be found in tropical seas. It feeds on sea grasses, which gives it a greenish-colored fat under its shell. It has a small, rounded head and smooth carapace.",
                                "Green Sea Turtle",
                                "Pawikan, Bildog",
                                "Chelonia Mydas",
                                "Endangered",
                                "LUCIANO CANDISANI"
                        ),
                        new Animal(R.drawable.divilacan_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not Threatened",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.divilacan_animal_11,
                                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                                "Northern Luzon Giant Cloud Rat",
                                " ",
                                "Phloeomys Pallidus",
                                "Least Concern",
                                "DANIEL HEUCLIN"
                        ),
                        new Animal(R.drawable.san_mariano_animal_15,
                                "The Philippine Eagle is one of the largest eagles 3 feet tall and a wingspan of 7 feet. It feeds mainly on small mammals that live in trees. It is the national bird of the Philippines and survives only in pristine forests. It has a very large dark bill, distinct crest on the head and large powerful claws.",
                                "Philippine Eagle",
                                "Agila, Haring ibon",
                                "Pithecophaga Jefferyi",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        ),
                        new Animal(R.drawable.san_pablo_animal_9,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),
                        new Animal(R.drawable.divilacan_animal_15,
                                "The Saltwater Crocodile or Estuarine Crocodile is one of the largest crocodiles in the world and can reach a length of 6 to 7 meters. It lives mainly in mangrove swamps, estuaries and large coastal lakes.",
                                "Saltwater Crocodile",
                                "Buwaya",
                                "Crocodylus Porosus",
                                "Not Globally Threatened",
                                "MERJLIN VAN WEERD"
                        ),
                        new Animal(R.drawable.ilagan_animal_6,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.ilagan_animal_9,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.maconacon_animal_3,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),
                        new Animal(R.drawable.dinapigue_animal_12,
                                "The Golden-crowned Flying Fox is the heaviest bat in the world. It can weigh up to 1,400 grams and has a wingspan of 3 feet. It is found only in the Philippines. It has a light yellow to golden fur on top of its head extending the back. It depends on undisturbed lowland forest where it feeds on fruits and flowers.",
                                "Golden-Crowned Flying Fox",
                                "Paniki",
                                "Acerodon Jubatus",
                                "Endangered",
                                "GODFREY JAKOSALEM"
                        ),
                        new Animal(R.drawable.san_mariano_animal_16,
                                "The Isabela Oriole is one of the rarest birds in the world and can be found only in Luzon island in the Philippines. It has a yellow-green body, broad gray beak, yellow lores, a yellow eye-ring and gray feet. It lives in lowland forest and eats insects and fruits.",
                                "Isabela Oriole",
                                "Kiyaw",
                                "Oriolus Isabellae",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        ),
                        new Animal(R.drawable.san_pablo_animal_2,
                                "The Montane Racquet-tail belongs to the family of parrots. It is endemic to Luzon. Also known as Luzon Racquet-tail, it can only found high up in the mountains, above 1,000 meters from sea level. Their central tail feathers have extensions that look like rackets. The male bird has a red spot on its head.",
                                "Montane Racquet-tail",
                                " ",
                                "Prioniturus Montanus",
                                "Near Threatened",
                                "MERJLIN VAN WEERD"
                        ),
                        new Animal(R.drawable.tumauini_animal_12,
                                "The Philippine Crocodile is one of the rarest crocodile species in the world. It can only be found in the Philippines. It is a relatively small freshwater crocodile. It lives in rivers, streams, small lakes and ponds. It feeds on snails, shrimps, fish, frogs, snakes and small mammals.",
                                "Philippine Crocodile",
                                "Bukarot",
                                "Crocodylus Mindorensis",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        )
                },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),
                });

        Global.Places.add(new Place("Cabagan",
                new Animal[]{
                        new Animal(R.drawable.cabagan_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not Threatened",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.cabagan_animal_2,
                                "The Montane Racquet-tail belongs to the family of parrots. It is endemic to Luzon. Also known as Luzon Racquet-tail, it can only found high up in the mountains, above 1,000 meters from sea level. Their central tail feathers have extensions that look like rackets. The male bird has a red spot on its head.",
                                "Montane Racquet-tail",
                                " ",
                                "Prioniturus Montanus",
                                "Near Threatened",
                                "MERJLIN VAN WEERD"
                        ),


                        new Animal(R.drawable.cabagan_animal_3,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),

                        new Animal(R.drawable.cabagan_animal_4,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),

                        new Animal(R.drawable.cabagan_animal_5,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),


                        new Animal(R.drawable.cabagan_animal_6,
                                "With its rusty-cinnamon head and bluish-grey bill, the Philippine duck is a rather distinctive bird. The cinnamon coloured head is boldly decorated with a black crown and a black stripe through the eye, while the rest of the plumage is brownish-grey. When in flight, a well-defined patch of glossy green on the wing can be clearly seen, which is bordered with black and has a narrow white edge, and the underside of the wing is also white. Immature Philippine ducks have slightly duller plumage than that of adults, while ducklings are olive-brown with a bright yellow face and neck. It calls with a typical duck-like quack.",
                                "Philippine Duck",
                                " ",
                                "Anas Luzonica",
                                "Vulnerable",
                                "E.G PEIKER"
                        ),

                        new Animal(R.drawable.cabagan_animal_7,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.cabagan_animal_8,
                                "The Northern Rufous Hornbill is endemic to Luzon and satellite islands. It is a good indicator of forest quality and is only found in large undisturbed forest areas.",
                                "Northern Rufous Hornbill",
                                "Kalaw",
                                "Buceros Hydrocorax",
                                "Vulnerable",
                                "TOMASZ  DORON"
                        ),

                        new Animal(R.drawable.cabagan_animal_9,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),

                        new Animal(R.drawable.cabagan_animal_10,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.cabagan_animal_11,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),

                        new Animal(R.drawable.cabagan_animal_12,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),


                        new Animal(R.drawable.cabagan_animal_13,
                                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                                "Northern Luzon Giant Cloud Rat",
                                " ",
                                "Phloeomys Pallidus",
                                "Least Concern",
                                "DANIEL HEUCLIN"
                        ),


                },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),

                }));

        Global.Places.add(new Place("Dinapigue",
                new Animal[]{
                        new Animal(R.drawable.dinapigue_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not Threatened",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.dinapigue_animal_2,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),

                        new Animal(R.drawable.dinapigue_animal_3,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),

                        new Animal(R.drawable.dinapigue_animal_4,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),


                        new Animal(R.drawable.dinapigue_animal_5,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.dinapigue_animal_6,
                                "The Northern Rufous Hornbill is endemic to Luzon and satellite islands. It is a good indicator of forest quality and is only found in large undisturbed forest areas.",
                                "Northern Rufous Hornbill",
                                "Kalaw",
                                "Buceros Hydrocorax",
                                "Vulnerable",
                                "TOMASZ  DORON"
                        ),

                        new Animal(R.drawable.dinapigue_animal_7,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),

                        new Animal(R.drawable.dinapigue_animal_8,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.dinapigue_animal_9,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),

                        new Animal(R.drawable.dinapigue_animal_10,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),

                        new Animal(R.drawable.dinapigue_animal_11,
                                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                                "Northern Luzon Giant Cloud Rat",
                                " ",
                                "Phloeomys Pallidus",
                                "Least Concern",
                                "DANIEL HEUCLIN"
                        ),

                        new Animal(R.drawable.dinapigue_animal_12,
                                "The Golden-crowned Flying Fox is the heaviest bat in the world. It can weigh up to 1,400 grams and has a wingspan of 3 feet. It is found only in the Philippines. It has a light yellow to golden fur on top of its head extending the back. It depends on undisturbed lowland forest where it feeds on fruits and flowers.",
                                "Golden-Crowned Flying Fox",
                                "Paniki",
                                "Acerodon Jubatus",
                                "Endangered",
                                "GODFREY JAKOSALEM"
                        ),

                        new Animal(R.drawable.dinapigue_animal_13,
                                "The Large flying fox also known as the greater flying fox, Malaysian flying fox, large fruit bat, kalang or kalong, is a species of megabat in the Pteropodidae family. Like the other members of the genus Pteropus, or the Old World fruit bats, it feeds exclusively on fruits, nectar and flowers. It is noted for being one of the largest bats. It, as with all other Old World fruit bats, lacks the ability to echolocate.",
                                "Large Flying Fox",
                                " ",
                                "Pteropus Vampyrus",
                                "Near Threatened",
                                "FLETCHER & BAYLIS"
                        ),

                        new Animal(R.drawable.dinapigue_animal_14,
                                "The Green Sea Turtle can be found in tropical seas. It feeds on sea grasses, which gives it a greenish-colored fat under its shell. It has a small, rounded head and smooth carapace.",
                                "Green Sea Turtle",
                                "Pawikan, Bildog",
                                "Chelonia Mydas",
                                "Endangered",
                                "LUCIANO CANDISANI"
                        ),

                        new Animal(R.drawable.dinapigue_animal_15,
                                "The Hawksbill Sea Turtle can be found only in tropical seas. It has overlapping scutes on its shell. It has a narrow head and a hooked beak shaped like the bill of a hawk. They live in coral reefs and eat sponges.",
                                "Hawksbill Sea Turtle",
                                "Pawikan, Karahan",
                                "Eretmochelys Imbricata",
                                "Critically Endangered",
                                "NICK CALOYIANIS"
                        ),

                        new Animal(R.drawable.dinapigue_animal_16,
                                "The Philippine Eagle is one of the largest eagles 3 feet tall and a wingspan of 7 feet. It feeds mainly on small mammals that live in trees. It is the national bird of the Philippines and survives only in pristine forests. It has a very large dark bill, distinct crest on the head and large powerful claws.",
                                "Philippine Eagle",
                                "Agila, Haring ibon",
                                "Pithecophaga Jefferyi",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        ),


                },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),
                }));


        Global.Places.add(new Place("Divilacan",
                new Animal[]{
                        new Animal(R.drawable.divilacan_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not Threatened",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.divilacan_animal_2,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),

                        new Animal(R.drawable.divilacan_animal_3,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),

                        new Animal(R.drawable.divilacan_animal_4,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),


                        new Animal(R.drawable.divilacan_animal_5,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.divilacan_animal_6,
                                "The Northern Rufous Hornbill is endemic to Luzon and satellite islands. It is a good indicator of forest quality and is only found in large undisturbed forest areas.",
                                "Northern Rufous Hornbill",
                                "Kalaw",
                                "Buceros Hydrocorax",
                                "Vulnerable",
                                "TOMASZ  DORON"
                        ),

                        new Animal(R.drawable.divilacan_animal_7,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),

                        new Animal(R.drawable.divilacan_animal_9,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.divilacan_animal_10,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),

                        new Animal(R.drawable.divilacan_animal_11,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),

                        new Animal(R.drawable.divilacan_animal_12,
                                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                                "Northern Luzon Giant Cloud Rat",
                                " ",
                                "Phloeomys Pallidus",
                                "Least Concern",
                                "DANIEL HEUCLIN"
                        ),

                        new Animal(R.drawable.divilacan_animal_13,
                                "The Golden-crowned Flying Fox is the heaviest bat in the world. It can weigh up to 1,400 grams and has a wingspan of 3 feet. It is found only in the Philippines. It has a light yellow to golden fur on top of its head extending the back. It depends on undisturbed lowland forest where it feeds on fruits and flowers.",
                                "Golden-crowned Flying Fox",
                                "Paniki",
                                "Acerodon Jubatus",
                                "Endangered",
                                "GODFREY JAKOSALEM"
                        ),


                        new Animal(R.drawable.divilacan_animal_14,
                                "The Large flying fox also known as the greater flying fox, Malaysian flying fox, large fruit bat, kalang or kalong, is a species of megabat in the Pteropodidae family. Like the other members of the genus Pteropus, or the Old World fruit bats, it feeds exclusively on fruits, nectar and flowers. It is noted for being one of the largest bats. It, as with all other Old World fruit bats, lacks the ability to echolocate.",
                                "Large Flying Fox",
                                " ",
                                "Pteropus Vampyrus",
                                "Near Threatened",
                                "FLETCHER & BAYLIS"
                        ),


                        new Animal(R.drawable.divilacan_animal_15,
                                "The Philippine Crocodile is one of the rarest crocodile species in the world. It can only be found in the Philippines. It is a relatively small freshwater crocodile. It lives in rivers, streams, small lakes and ponds. It feeds on snails, shrimps, fish, frogs, snakes and small mammals.",
                                "Philippine Crocodile",
                                "Bukarot",
                                "Crocodylus Mindorensis",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        ),

                        new Animal(R.drawable.divilacan_animal_16,
                                "The Saltwater Crocodile or Estuarine Crocodile is one of the largest crocodiles in the world and can reach a length of 6 to 7 meters. It lives mainly in mangrove swamps, estuaries and large coastal lakes.",
                                "Saltwater Crocodile",
                                "Buwaya",
                                "Crocodylus Porosus",
                                "Not Globally Threatened",
                                "MERJLIN VAN WEERD"
                        ),


                        new Animal(R.drawable.divilacan_animal_17,
                                "The Bitatawa is a giant fruit-eating monitor lizard. It lives in the lowland forest of northern Luzon. It has golden-yellow spots on its body and large strong claws to climb trees. It can grow up to 6 feet and can weigh up to 10 kg.",
                                "Bitatawa",
                                "Bitatawa, Batikaw",
                                "Varanus Bitatawa",
                                "Data Deficient",
                                "BRIAN SANTOS"
                        ),

                        new Animal(R.drawable.divilacan_animal_18,
                                "Bottlenose dolphins have the second largest encephalization levels of any mammal on Earth (humans have the largest), sharing close ratios with those of humans and other great apes, which more than likely contributes to their high intelligence and emotional intelligence.",
                                "Bottle-Nosed Dolphin",
                                " ",
                                "Tursiops Truncatus",
                                "Endangered",
                                "ROBERT PERRY"
                        ),

                        new Animal(R.drawable.divilacan_animal_19,
                                "The Green Sea Turtle can be found in tropical seas. It feeds on sea grasses, which gives it a greenish-colored fat under its shell. It has a small, rounded head and smooth carapace.",
                                "Green Sea Turtle",
                                "Pawikan, Bildog",
                                "Chelonia Mydas",
                                "Endangered",
                                "LUCIANO CANDISANI"
                        ),

                        new Animal(R.drawable.divilacan_animal_20,
                                "The Hawksbill Sea Turtle can be found only in tropical seas. It has overlapping scutes on its shell. It has a narrow head and a hooked beak shaped like the bill of a hawk. They live in coral reefs and eat sponges.",
                                "Hawksbill Sea Turtle",
                                "Pawikan, Karahan",
                                "Eretmochelys Imbricata",
                                "Critically Endangered",
                                "NICK CALOYIANIS"
                        ),

                        new Animal(R.drawable.divilacan_animal_21,
                                "The celestial monarch is a species of bird in the family Monarchidae, and one of the most attractive of all the monarch flycatchers, with its spectacular blue crest and large yellow eye-ring (neither of which are illustrated in the facing painting). It is endemic to the Philippines.",
                                "Celestial Monarch",
                                " ",
                                "Hypothymis Coelestis",
                                "Vulnerable",
                                "JONI T. ACAY"
                        ),

                    },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),
                    }));


        Global.Places.add(new Place("Ilagan City",
                new Animal[]{
                        new Animal(R.drawable.ilagan_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not Threatened",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.ilagan_animal_2,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),

                        new Animal(R.drawable.ilagan_animal_3,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),

                        new Animal(R.drawable.ilagan_animal_4,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),


                        new Animal(R.drawable.ilagan_animal_5,
                                "With its rusty-cinnamon head and bluish-grey bill, the Philippine duck is a rather distinctive bird. The cinnamon coloured head is boldly decorated with a black crown and a black stripe through the eye, while the rest of the plumage is brownish-grey. When in flight, a well-defined patch of glossy green on the wing can be clearly seen, which is bordered with black and has a narrow white edge, and the underside of the wing is also white. Immature Philippine ducks have slightly duller plumage than that of adults, while ducklings are olive-brown with a bright yellow face and neck. It calls with a typical duck-like quack.",
                                "Philippine Duck",
                                " ",
                                "Anas Luzonica",
                                "Vulnerable",
                                "E.G PEIKER"
                        ),

                        new Animal(R.drawable.ilagan_animal_6,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.ilagan_animal_7,
                                "The Northern Rufous Hornbill is endemic to Luzon and satellite islands. It is a good indicator of forest quality and is only found in large undisturbed forest areas.",
                                "Northern Rufous Hornbill",
                                "Kalaw",
                                "Buceros Hydrocorax",
                                "Vulnerable",
                                "TOMASZ  DORON"
                        ),

                        new Animal(R.drawable.ilagan_animal_8,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),

                        new Animal(R.drawable.ilagan_animal_9,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.ilagan_animal_10,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),

                        new Animal(R.drawable.ilagan_animal_11,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),

                        new Animal(R.drawable.ilagan_animal_12,
                                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                                "Northern Luzon Giant Cloud Rat",
                                " ",
                                "Phloeomys Pallidus",
                                "Least Concern",
                                "DANIEL HEUCLIN"
                        ),
                },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),
                }));


        Global.Places.add(new Place("Maconacon",
                new Animal[]{
                        new Animal(R.drawable.maconacon_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not Threatened",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.maconacon_animal_2,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),

                        new Animal(R.drawable.maconacon_animal_3,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),

                        new Animal(R.drawable.maconacon_animal_4,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),


                        new Animal(R.drawable.maconacon_animal_5,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.maconacon_animal_6,
                                "The Northern Rufous Hornbill is endemic to Luzon and satellite islands. It is a good indicator of forest quality and is only found in large undisturbed forest areas.",
                                "Northern Rufous Hornbill",
                                "Kalaw",
                                "Buceros Hydrocorax",
                                "Vulnerable",
                                "TOMASZ  DORON"
                        ),

                        new Animal(R.drawable.maconacon_animal_7,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),

                        new Animal(R.drawable.maconacon_animal_8,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.maconacon_animal_9,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),

                        new Animal(R.drawable.maconacon_animal_10,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),

                        new Animal(R.drawable.maconacon_animal_11,
                                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                                "Northern Luzon Giant Cloud Rat",
                                " ",
                                "Phloeomys Pallidus",
                                "Least Concern",
                                "DANIEL HEUCLIN"
                        ),

                        new Animal(R.drawable.maconacon_animal_12,
                                "The Philippine Crocodile is one of the rarest crocodile species in the world. It can only be found in the Philippines. It is a relatively small freshwater crocodile. It lives in rivers, streams, small lakes and ponds. It feeds on snails, shrimps, fish, frogs, snakes and small mammals.",
                                "Philippine Crocodile",
                                "Bukarot",
                                "Crocodylus Mindorensis",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        ),

                        new Animal(R.drawable.maconacon_animal_13,
                                "The Saltwater Crocodile or Estuarine Crocodile is one of the largest crocodiles in the world and can reach a length of 6 to 7 meters. It lives mainly in mangrove swamps, estuaries and large coastal lakes.",
                                "Saltwater Crocodile",
                                "Buwaya",
                                "Crocodylus Porosus",
                                "Not Globally Threatened",
                                "MERJLIN VAN WEERD"
                        ),

                        new Animal(R.drawable.maconacon_animal_14,
                                "The Bitatawa is a giant fruit-eating monitor lizard. It lives in the lowland forest of northern Luzon. It has golden-yellow spots on its body and large strong claws to climb trees. It can grow up to 6 feet and can weigh up to 10 kg.",
                                "Bitatawa",
                                "Bitatawa, Batikaw",
                                "Varanus Bitatawa",
                                "Data Deficient",
                                "BRIAN SANTOS"
                        ),

                        new Animal(R.drawable.maconacon_animal_15,
                                "Bottlenose dolphins have the second largest encephalization levels of any mammal on Earth (humans have the largest), sharing close ratios with those of humans and other great apes, which more than likely contributes to their high intelligence and emotional intelligence.",
                                "Bottle-Nosed Dolphin",
                                " ",
                                "Tursiops Truncatus",
                                "Endangered",
                                "ROBERT PERRY"
                        ),

                        new Animal(R.drawable.maconacon_animal_16,
                                "The Green Sea Turtle can be found in tropical seas. It feeds on sea grasses, which gives it a greenish-colored fat under its shell. It has a small, rounded head and smooth carapace.",
                                "Green Sea Turtle",
                                "Pawikan, Bildog",
                                "Chelonia Mydas",
                                "Endangered",
                                "LUCIANO CANDISANI"
                        ),

                        new Animal(R.drawable.maconacon_animal_17,
                                "The Hawksbill Sea Turtle can be found only in tropical seas. It has overlapping scutes on its shell. It has a narrow head and a hooked beak shaped like the bill of a hawk. They live in coral reefs and eat sponges.",
                                "Hawksbill Sea Turtle",
                                "Pawikan, Karahan",
                                "Eretmochelys Imbricata",
                                "Critically Endangered",
                                "NICK CALOYIANIS"
                        ),




                },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),
                }));

        Global.Places.add(new Place("Palanan",
                new Animal[]{
                        new Animal(R.drawable.palanan_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not Threatened",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.palanan_animal_2,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),

                        new Animal(R.drawable.palanan_animal_3,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),

                        new Animal(R.drawable.palanan_animal_4,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),


                        new Animal(R.drawable.palanan_animal_5,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.palanan_animal_6,
                                "The Northern Rufous Hornbill is endemic to Luzon and satellite islands. It is a good indicator of forest quality and is only found in large undisturbed forest areas.",
                                "Northern Rufous Hornbill",
                                "Kalaw",
                                "Buceros Hydrocorax",
                                "Vulnerable",
                                "TOMASZ  DORON"
                        ),

                        new Animal(R.drawable.palanan_animal_7,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),

                        new Animal(R.drawable.palanan_animal_8,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.palanan_animal_9,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),

                        new Animal(R.drawable.palanan_animal_10,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),

                        new Animal(R.drawable.palanan_animal_11,
                                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                                "Northern Luzon Giant Cloud Rat",
                                " ",
                                "Phloeomys Pallidus",
                                "Least Concern",
                                "DANIEL HEUCLIN"
                        ),




                },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),
                }));


        Global.Places.add(new Place("San Mariano",
                new Animal[]{
                        new Animal(R.drawable.san_mariano_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not threatened",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.san_mariano_animal_2,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),

                        new Animal(R.drawable.san_mariano_animal_3,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),

                        new Animal(R.drawable.san_mariano_animal_4,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),

                        new Animal(R.drawable.san_mariano_animal_5,
                                "With its rusty-cinnamon head and bluish-grey bill, the Philippine duck is a rather distinctive bird. The cinnamon coloured head is boldly decorated with a black crown and a black stripe through the eye, while the rest of the plumage is brownish-grey. When in flight, a well-defined patch of glossy green on the wing can be clearly seen, which is bordered with black and has a narrow white edge, and the underside of the wing is also white. Immature Philippine ducks have slightly duller plumage than that of adults, while ducklings are olive-brown with a bright yellow face and neck. It calls with a typical duck-like quack.",
                                "Philippine Duck",
                                " ",
                                "Anas Luzonica",
                                "Vulnerable",
                                "E.G PEIKER"
                        ),

                        new Animal(R.drawable.san_mariano_animal_6,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.san_mariano_animal_7,
                                "The Northern Rufous Hornbill is endemic to Luzon and satellite islands. It is a good indicator of forest quality and is only found in large undisturbed forest areas.",
                                "Northern Rufous Hornbill",
                                "Kalaw",
                                "Buceros Hydrocorax",
                                "Vulnerable",
                                "TOMASZ  DORON"
                        ),

                        new Animal(R.drawable.san_mariano_animal_8,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),

                        new Animal(R.drawable.san_mariano_animal_9,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.san_mariano_animal_10,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),

                        new Animal(R.drawable.san_mariano_animal_11,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),

                        new Animal(R.drawable.san_mariano_animal_12,
                                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                                "Northern Luzon Giant Cloud Rat",
                                " ",
                                "Phloeomys Pallidus",
                                "Least Concern",
                                "DANIEL HEUCLIN"
                        ),

                        new Animal(R.drawable.san_mariano_animal_13,
                                "The Philippine Crocodile is one of the rarest crocodile species in the world. It can only be found in the Philippines. It is a relatively small freshwater crocodile. It lives in rivers, streams, small lakes and ponds. It feeds on snails, shrimps, fish, frogs, snakes and small mammals.",
                                "Philippine Crocodile",
                                "Bukarot",
                                "Crocodylus Mindorensis",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        ),

                        new Animal(R.drawable.san_mariano_animal_14,
                                "The Bitatawa is a giant fruit-eating monitor lizard. It lives in the lowland forest of northern Luzon. It has golden-yellow spots on its body and large strong claws to climb trees. It can grow up to 6 feet and can weigh up to 10 kg.",
                                "Bitatawa",
                                "Bitatawa, Batikaw",
                                "Varanus Bitatawa",
                                "Data Deficient",
                                "BRIAN SANTOS"
                        ),

                        new Animal(R.drawable.san_mariano_animal_15,
                                "The Philippine Eagle is one of the largest eagles 3 feet tall and a wingspan of 7 feet. It feeds mainly on small mammals that live in trees. It is the national bird of the Philippines and survives only in pristine forests. It has a very large dark bill, distinct crest on the head and large powerful claws.",
                                "Philippine Eagle",
                                "Agila, Haring ibon",
                                "Pithecophaga Jefferyi",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        ),

                        new Animal(R.drawable.san_mariano_animal_16,
                                "The Isabela Oriole is one of the rarest birds in the world and can be found only in Luzon island in the Philippines. It has a yellow-green body, broad gray beak, yellow lores, a yellow eye-ring and gray feet. It lives in lowland forest and eats insects and fruits.",
                                "Isabela Oriole",
                                "Kiyaw",
                                "Oriolus Isabellae",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        ),




                },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),
                }));


        Global.Places.add(new Place("San Pablo",
                new Animal[]{
                        new Animal(R.drawable.san_pablo_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not Threatened",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.san_pablo_animal_2,
                                "The Montane Racquet-tail belongs to the family of parrots. It is endemic to Luzon. Also known as Luzon Racquet-tail, it can only found high up in the mountains, above 1,000 meters from sea level. Their central tail feathers have extensions that look like rackets. The male bird has a red spot on its head.",
                                "Montane Racquet-tail",
                                " ",
                                "Prioniturus Montanus",
                                "Near Threatened",
                                "MERJLIN VAN WEERD"
                        ),

                        new Animal(R.drawable.san_pablo_animal_3,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),

                        new Animal(R.drawable.san_pablo_animal_4,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),


                        new Animal(R.drawable.san_pablo_animal_5,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),

                        new Animal(R.drawable.san_pablo_animal_6,
                                "With its rusty-cinnamon head and bluish-grey bill, the Philippine duck is a rather distinctive bird. The cinnamon coloured head is boldly decorated with a black crown and a black stripe through the eye, while the rest of the plumage is brownish-grey. When in flight, a well-defined patch of glossy green on the wing can be clearly seen, which is bordered with black and has a narrow white edge, and the underside of the wing is also white. Immature Philippine ducks have slightly duller plumage than that of adults, while ducklings are olive-brown with a bright yellow face and neck. It calls with a typical duck-like quack.",
                                "Philippine Duck",
                                " ",
                                "Anas Luzonica",
                                "Vulnerable",
                                "E.G PEIKER"
                        ),

                        new Animal(R.drawable.san_pablo_animal_7,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.san_pablo_animal_8,
                                "The Northern Rufous Hornbill is endemic to Luzon and satellite islands. It is a good indicator of forest quality and is only found in large undisturbed forest areas.",
                                "Northern Rufous Hornbill",
                                "Kalaw",
                                "Buceros Hydrocorax",
                                "Vulnerable",
                                "TOMASZ  DORON"
                        ),

                        new Animal(R.drawable.san_pablo_animal_9,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),

                        new Animal(R.drawable.san_pablo_animal_10,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.san_pablo_animal_11,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),

                        new Animal(R.drawable.san_pablo_animal_12,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),

                        new Animal(R.drawable.san_pablo_animal_13,
                                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                                "Northern Luzon Giant Cloud Rat",
                                " ",
                                "Phloeomys Pallidus",
                                "Least Concern",
                                "DANIEL HEUCLIN"
                        ),



                },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),
                }));


        Global.Places.add(new Place("Tumauini",
                new Animal[]{
                        new Animal(R.drawable.tumauini_animal_1,
                                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                                "Luzon Hornbill",
                                "Tarictic",
                                "Penelopides Manillae",
                                "Not Threatened",
                                "JONI T. ACAY"
                        ),
                        new Animal(R.drawable.tumauini_animal_2,
                                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                                "Green Racquet-tail",
                                " ",
                                "Prioniturus Luconesis",
                                "Endangered",
                                "DEL HOYO"
                        ),

                        new Animal(R.drawable.tumauini_animal_3,
                                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                                "Warty Pig",
                                " ",
                                "Sus Philippensis",
                                "Vulnerable",
                                "BRENT HUFFMAN"
                        ),

                        new Animal(R.drawable.tumauini_animal_4,
                                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                                "Philippine Brown Deer",
                                " ",
                                "Rusa Marianna",
                                "Vulnerable",
                                "DANIEL HEUCLIN"
                        ),


                        new Animal(R.drawable.tumauini_animal_5,
                                "With its rusty-cinnamon head and bluish-grey bill, the Philippine duck is a rather distinctive bird. The cinnamon coloured head is boldly decorated with a black crown and a black stripe through the eye, while the rest of the plumage is brownish-grey. When in flight, a well-defined patch of glossy green on the wing can be clearly seen, which is bordered with black and has a narrow white edge, and the underside of the wing is also white. Immature Philippine ducks have slightly duller plumage than that of adults, while ducklings are olive-brown with a bright yellow face and neck. It calls with a typical duck-like quack.",
                                "Philippine Duck",
                                " ",
                                "Anas Luzonica",
                                "Vulnerable",
                                "E.G PEIKER"
                        ),

                        new Animal(R.drawable.tumauini_animal_6,
                                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                                "Scale-feathered Malkoha",
                                " ",
                                "Lepidogrammus Cumingi",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.tumauini_animal_7,
                                "The Northern Rufous Hornbill is endemic to Luzon and satellite islands. It is a good indicator of forest quality and is only found in large undisturbed forest areas.",
                                "Northern Rufous Hornbill",
                                "Kalaw",
                                "Buceros Hydrocorax",
                                "Vulnerable",
                                "TOMASZ  DORON"
                        ),

                        new Animal(R.drawable.tumauini_animal_8,
                                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                                "Philippine Hanging Parrot",
                                "Colasisi",
                                "Loriculus Philippensis",
                                "Least Concern",
                                "ALEX VARGAS"
                        ),

                        new Animal(R.drawable.tumauini_animal_9,
                                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                                "Sierra Madre Ground Warbler",
                                " ",
                                "Robsonius Thompsoni",
                                "Least Concern",
                                "JONI T. ACAY"
                        ),

                        new Animal(R.drawable.tumauini_animal_10,
                                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                                "Philippine Dwarf Kingfisher",
                                " ",
                                "Ceyx Melanurus",
                                "Vulnerable",
                                "MIGUEL DAVID DE LEON"
                        ),

                        new Animal(R.drawable.tumauini_animal_11,
                                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                                "Philippine Pit Viper",
                                " ",
                                "Trimeresurus Flavomaculatus",
                                "Least Concern",
                                "ZEBULON HOOVER"
                        ),

                        new Animal(R.drawable.tumauini_animal_12,
                                "The Philippine Crocodile is one of the rarest crocodile species in the world. It can only be found in the Philippines. It is a relatively small freshwater crocodile. It lives in rivers, streams, small lakes and ponds. It feeds on snails, shrimps, fish, frogs, snakes and small mammals.",
                                "Philippine Crocodile",
                                "Bukarot",
                                "Crocodylus Mindorensis",
                                "Critically Endangered",
                                "MERJLIN VAN WEERD"
                        ),

//                        new Animal(R.drawable.tumauini_animal_13,
//                                "The Saltwater Crocodile or Estuarine Crocodile is one of the largest crocodiles in the world and can reach a length of 6 to 7 meters. It lives mainly in mangrove swamps, estuaries and large coastal lakes.",
//                                "Saltwater Crocodile",
//                                "Buwaya",
//                                "Crocodylus Porosus",
//                                "Not Globally Threatened"
//                        ),
//
//                        new Animal(R.drawable.tumauini_animal_14,
//                                "The Bitatawa is a giant fruit-eating monitor lizard. It lives in the lowland forest of northern Luzon. It has golden-yellow spots on its body and large strong claws to climb trees. It can grow up to 6 feet and can weigh up to 10 kg.",
//                                "Bitatawa",
//                                "Bitatawa, Batikaw",
//                                "Varanus Bitatawa",
//                                "Data Deficient"
//                        ),
//
//                        new Animal(R.drawable.tumauini_animal_15,
//                                "Bottlenose dolphins have the second largest encephalization levels of any mammal on Earth (humans have the largest), sharing close ratios with those of humans and other great apes, which more than likely contributes to their high intelligence and emotional intelligence.",
//                                "Bottle-Nosed Dolphin",
//                                " ",
//                                "Tursiops Truncatus",
//                                "Endangered"
//                        ),

//                        new Animal(R.drawable.tumauini_animal_16,
//                                "The Green Sea Turtle can be found in tropical seas. It feeds on sea grasses, which gives it a greenish-colored fat under its shell. It has a small, rounded head and smooth carapace.",
//                                "Green Sea Turtle",
//                                "Pawikan, Bildog",
//                                "Chelonia Mydas",
//                                "Endangered"
//                        ),
//
//                        new Animal(R.drawable.tumauini_animal_17,
//                                "The Hawksbill Sea Turtle can be found only in tropical seas. It has overlapping scutes on its shell. It has a narrow head and a hooked beak shaped like the bill of a hawk. They live in coral reefs and eat sponges.",
//                                "Hawksbill Sea Turtle",
//                                "Pawikan, Karahan",
//                                "Eretmochelys Imbricata",
//                                "Critically Endangered"
//                        ),




                },
                new Animal[]{
                        new Animal(R.drawable.tree_1,
                                "",
                                "Pahutan",
                                "",
                                "Mangifera altissima",
                                "Vulnerable",
                                "Binhi"
                        ),
                        new Animal(R.drawable.tree_2,
                                "",
                                "Pili",
                                "",
                                "Canarium ovatum",
                                "Vulnerable",
                                "Organic Spa Magazine"
                        ),
                        new Animal(R.drawable.tree_3,
                                "",
                                "Katmon",
                                "",
                                "Dillenia sp.",
                                "Vulnerable",
                                "Pinterest"
                        ),
                        new Animal(R.drawable.tree_4,
                                "",
                                "White Lauan",
                                "",
                                "Shorea contorta",
                                "Vulnerable",
                                "Plants and their Uses"
                        ),
                        new Animal(R.drawable.tree_5,
                                "",
                                "Apitong",
                                "",
                                "Dipterocarpus grandiflorus",
                                "CR",
                                "Twitter.com"
                        ),
                        new Animal(R.drawable.tree_6,
                                "",
                                "Red Lauan",
                                "",
                                "Shorea negrosensis",
                                "CR",
                                "FEED,Inc."
                        ),
                        new Animal(R.drawable.tree_7,
                                "",
                                "Tangile",
                                "",
                                "Shorea polysperma",
                                "CR",
                                "Facebook"
                        ),
                        new Animal(R.drawable.tree_8,
                                "",
                                "Yakal (small leaf)",
                                "",
                                "Shorea malibato",
                                "CR",
                                "The Habitat Advocate"
                        ),
                        new Animal(R.drawable.tree_9,
                                "",
                                "Guijo",
                                "",
                                "Shorea guiso",
                                "CR",
                                "Tonji and Sylvia’s Bird Sanctuary"
                        ),
                        new Animal(R.drawable.tree_10,
                                "Steemit",
                                "Narra",
                                "",
                                "Pterocarpus indicus",
                                "Vulnerable",
                                ""
                        ),
                        new Animal(R.drawable.tree_11,
                                "",
                                "Molave",
                                "",
                                "Vitex parviflora",
                                "Vulnerable",
                                "Crop Farming"
                        ),
                }));


                //******************* SET LOCATIONS *********************//

                Global.Places.get(0).setLocation(17.3608, 121.8790); //Cabagan
                Global.Places.get(1).setLocation(16.6960, 122.2200); //Dinapigue
                Global.Places.get(2).setLocation(17.2705, 122.1519); //Divilacan
                Global.Places.get(3).setLocation(17.1263, 122.0101); //Ilagan
                Global.Places.get(4).setLocation(17.3621, 122.1065); //Maconacon
                Global.Places.get(5).setLocation(16.9969, 122.4014); //Palanan
                Global.Places.get(6).setLocation(16.8783, 122.1292); //SanMariano
                Global.Places.get(7).setLocation(17.4615, 121.7943); //SanPablo
                Global.Places.get(8).setLocation(17.2771, 121.8790); //Tumauini


                //******************* SET LEGEND COLORS *********************//
                Global.Places.get(0).setLegendColor(BitmapDescriptorFactory.HUE_AZURE);
                Global.Places.get(1).setLegendColor(BitmapDescriptorFactory.HUE_BLUE);
                Global.Places.get(2).setLegendColor(BitmapDescriptorFactory.HUE_GREEN);
                Global.Places.get(3).setLegendColor(BitmapDescriptorFactory.HUE_MAGENTA);
                Global.Places.get(4).setLegendColor(BitmapDescriptorFactory.HUE_ORANGE);
                Global.Places.get(5).setLegendColor(BitmapDescriptorFactory.HUE_ORANGE);
                Global.Places.get(6).setLegendColor(BitmapDescriptorFactory.HUE_VIOLET);
                Global.Places.get(7).setLegendColor(BitmapDescriptorFactory.HUE_YELLOW);
                Global.Places.get(8).setLegendColor(BitmapDescriptorFactory.HUE_RED);



        //******************* SET JIGSAW PUZZLES *********************//
                Global.Places.get(0).setJigsawPuzzle(
                        new JigsawPuzzle[]{
                                new JigsawPuzzle(
                                        R.drawable.easy_1,
                                        new int[] {
                                                R.drawable.easy_1_1,
                                                R.drawable.easy_1_2,
                                                R.drawable.easy_1_3,
                                                R.drawable.easy_1_4,
                                                R.drawable.easy_1_5,
                                                R.drawable.easy_1_6,
                                                R.drawable.easy_1_7,
                                                R.drawable.easy_1_8,
                                                R.drawable.easy_1_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_2,
                                        new int[] {
                                                R.drawable.easy_2_1,
                                                R.drawable.easy_2_2,
                                                R.drawable.easy_2_3,
                                                R.drawable.easy_2_4,
                                                R.drawable.easy_2_5,
                                                R.drawable.easy_2_6,
                                                R.drawable.easy_2_7,
                                                R.drawable.easy_2_8,
                                                R.drawable.easy_2_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_3,
                                        new int[] {
                                                R.drawable.easy_3_1,
                                                R.drawable.easy_3_2,
                                                R.drawable.easy_3_3,
                                                R.drawable.easy_3_4,
                                                R.drawable.easy_3_5,
                                                R.drawable.easy_3_6,
                                                R.drawable.easy_3_7,
                                                R.drawable.easy_3_8,
                                                R.drawable.easy_3_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_4,
                                        new int[] {
                                                R.drawable.easy_4_1,
                                                R.drawable.easy_4_2,
                                                R.drawable.easy_4_3,
                                                R.drawable.easy_4_4,
                                                R.drawable.easy_4_5,
                                                R.drawable.easy_4_6,
                                                R.drawable.easy_4_7,
                                                R.drawable.easy_4_8,
                                                R.drawable.easy_4_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_5,
                                        new int[] {
                                                R.drawable.easy_5_1,
                                                R.drawable.easy_5_2,
                                                R.drawable.easy_5_3,
                                                R.drawable.easy_5_4,
                                                R.drawable.easy_5_5,
                                                R.drawable.easy_5_6,
                                                R.drawable.easy_5_7,
                                                R.drawable.easy_5_8,
                                                R.drawable.easy_5_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_6,
                                        new int[] {
                                                R.drawable.easy_6_1,
                                                R.drawable.easy_6_2,
                                                R.drawable.easy_6_3,
                                                R.drawable.easy_6_4,
                                                R.drawable.easy_6_5,
                                                R.drawable.easy_6_6,
                                                R.drawable.easy_6_7,
                                                R.drawable.easy_6_8,
                                                R.drawable.easy_6_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_7,
                                        new int[] {
                                                R.drawable.easy_7_1,
                                                R.drawable.easy_7_2,
                                                R.drawable.easy_7_3,
                                                R.drawable.easy_7_4,
                                                R.drawable.easy_7_5,
                                                R.drawable.easy_7_6,
                                                R.drawable.easy_7_7,
                                                R.drawable.easy_7_8,
                                                R.drawable.easy_7_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_8,
                                        new int[] {
                                                R.drawable.easy_8_1,
                                                R.drawable.easy_8_2,
                                                R.drawable.easy_8_3,
                                                R.drawable.easy_8_4,
                                                R.drawable.easy_8_5,
                                                R.drawable.easy_8_6,
                                                R.drawable.easy_8_7,
                                                R.drawable.easy_8_8,
                                                R.drawable.easy_8_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_9,
                                        new int[] {
                                                R.drawable.easy_9_1,
                                                R.drawable.easy_9_2,
                                                R.drawable.easy_9_3,
                                                R.drawable.easy_9_4,
                                                R.drawable.easy_9_5,
                                                R.drawable.easy_9_6,
                                                R.drawable.easy_9_7,
                                                R.drawable.easy_9_8,
                                                R.drawable.easy_9_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_10,
                                        new int[] {
                                                R.drawable.easy_10_1,
                                                R.drawable.easy_10_2,
                                                R.drawable.easy_10_3,
                                                R.drawable.easy_10_4,
                                                R.drawable.easy_10_5,
                                                R.drawable.easy_10_6,
                                                R.drawable.easy_10_7,
                                                R.drawable.easy_10_8,
                                                R.drawable.easy_10_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_11,
                                        new int[] {
                                                R.drawable.easy_11_1,
                                                R.drawable.easy_11_2,
                                                R.drawable.easy_11_3,
                                                R.drawable.easy_11_4,
                                                R.drawable.easy_11_5,
                                                R.drawable.easy_11_6,
                                                R.drawable.easy_11_7,
                                                R.drawable.easy_11_8,
                                                R.drawable.easy_11_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_12,
                                        new int[] {
                                                R.drawable.easy_12_1,
                                                R.drawable.easy_12_2,
                                                R.drawable.easy_12_3,
                                                R.drawable.easy_12_4,
                                                R.drawable.easy_12_5,
                                                R.drawable.easy_12_6,
                                                R.drawable.easy_12_7,
                                                R.drawable.easy_12_8,
                                                R.drawable.easy_12_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_13,
                                        new int[] {
                                                R.drawable.easy_13_1,
                                                R.drawable.easy_13_2,
                                                R.drawable.easy_13_3,
                                                R.drawable.easy_13_4,
                                                R.drawable.easy_13_5,
                                                R.drawable.easy_13_6,
                                                R.drawable.easy_13_7,
                                                R.drawable.easy_13_8,
                                                R.drawable.easy_13_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_14,
                                        new int[] {
                                                R.drawable.easy_14_1,
                                                R.drawable.easy_14_2,
                                                R.drawable.easy_14_3,
                                                R.drawable.easy_14_4,
                                                R.drawable.easy_14_5,
                                                R.drawable.easy_14_6,
                                                R.drawable.easy_14_7,
                                                R.drawable.easy_14_8,
                                                R.drawable.easy_14_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_15,
                                        new int[] {
                                                R.drawable.easy_15_1,
                                                R.drawable.easy_15_2,
                                                R.drawable.easy_15_3,
                                                R.drawable.easy_15_4,
                                                R.drawable.easy_15_5,
                                                R.drawable.easy_15_6,
                                                R.drawable.easy_15_7,
                                                R.drawable.easy_15_8,
                                                R.drawable.easy_15_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_16,
                                        new int[] {
                                                R.drawable.easy_16_1,
                                                R.drawable.easy_16_2,
                                                R.drawable.easy_16_3,
                                                R.drawable.easy_16_4,
                                                R.drawable.easy_16_5,
                                                R.drawable.easy_16_6,
                                                R.drawable.easy_16_7,
                                                R.drawable.easy_16_8,
                                                R.drawable.easy_16_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_17,
                                        new int[] {
                                                R.drawable.easy_17_1,
                                                R.drawable.easy_17_2,
                                                R.drawable.easy_17_3,
                                                R.drawable.easy_17_4,
                                                R.drawable.easy_17_5,
                                                R.drawable.easy_17_6,
                                                R.drawable.easy_17_7,
                                                R.drawable.easy_17_8,
                                                R.drawable.easy_17_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_18,
                                        new int[] {
                                                R.drawable.easy_18_1,
                                                R.drawable.easy_18_2,
                                                R.drawable.easy_18_3,
                                                R.drawable.easy_18_4,
                                                R.drawable.easy_18_5,
                                                R.drawable.easy_18_6,
                                                R.drawable.easy_18_7,
                                                R.drawable.easy_18_8,
                                                R.drawable.easy_18_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_19,
                                        new int[] {
                                                R.drawable.easy_19_1,
                                                R.drawable.easy_19_2,
                                                R.drawable.easy_19_3,
                                                R.drawable.easy_19_4,
                                                R.drawable.easy_19_5,
                                                R.drawable.easy_19_6,
                                                R.drawable.easy_19_7,
                                                R.drawable.easy_19_8,
                                                R.drawable.easy_19_9,
                                        })  ,
                                new JigsawPuzzle(
                                        R.drawable.easy_20,
                                        new int[] {
                                                R.drawable.easy_20_1,
                                                R.drawable.easy_20_2,
                                                R.drawable.easy_20_3,
                                                R.drawable.easy_20_4,
                                                R.drawable.easy_20_5,
                                                R.drawable.easy_20_6,
                                                R.drawable.easy_20_7,
                                                R.drawable.easy_20_8,
                                                R.drawable.easy_20_9,
                                        })  ,
                        }


                 );

        Global.Places.get(0).setInterMediateJigsawPuzzle(
                new JigsawPuzzle[]{
                        new JigsawPuzzle(
                                R.drawable.average_1,
                                new int[] {
                                        R.drawable.average_1_1,
                                        R.drawable.average_1_2,
                                        R.drawable.average_1_3,
                                        R.drawable.average_1_4,
                                        R.drawable.average_1_5,
                                        R.drawable.average_1_6,
                                        R.drawable.average_1_7,
                                        R.drawable.average_1_8,
                                        R.drawable.average_1_9,
                                        R.drawable.average_1_10,
                                        R.drawable.average_1_11,
                                        R.drawable.average_1_12,
                                        R.drawable.average_1_13,
                                        R.drawable.average_1_14,
                                        R.drawable.average_1_15,
                                        R.drawable.average_1_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_2,
                                new int[] {
                                        R.drawable.average_2_1,
                                        R.drawable.average_2_2,
                                        R.drawable.average_2_3,
                                        R.drawable.average_2_4,
                                        R.drawable.average_2_5,
                                        R.drawable.average_2_6,
                                        R.drawable.average_2_7,
                                        R.drawable.average_2_8,
                                        R.drawable.average_2_9,
                                        R.drawable.average_2_10,
                                        R.drawable.average_2_11,
                                        R.drawable.average_2_12,
                                        R.drawable.average_2_13,
                                        R.drawable.average_2_14,
                                        R.drawable.average_2_15,
                                        R.drawable.average_2_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_3,
                                new int[] {
                                        R.drawable.average_3_1,
                                        R.drawable.average_3_2,
                                        R.drawable.average_3_3,
                                        R.drawable.average_3_4,
                                        R.drawable.average_3_5,
                                        R.drawable.average_3_6,
                                        R.drawable.average_3_7,
                                        R.drawable.average_3_8,
                                        R.drawable.average_3_9,
                                        R.drawable.average_3_10,
                                        R.drawable.average_3_11,
                                        R.drawable.average_3_12,
                                        R.drawable.average_3_13,
                                        R.drawable.average_3_14,
                                        R.drawable.average_3_15,
                                        R.drawable.average_3_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_4,
                                new int[] {
                                        R.drawable.average_4_1,
                                        R.drawable.average_4_2,
                                        R.drawable.average_4_3,
                                        R.drawable.average_4_4,
                                        R.drawable.average_4_5,
                                        R.drawable.average_4_6,
                                        R.drawable.average_4_7,
                                        R.drawable.average_4_8,
                                        R.drawable.average_4_9,
                                        R.drawable.average_4_10,
                                        R.drawable.average_4_11,
                                        R.drawable.average_4_12,
                                        R.drawable.average_4_13,
                                        R.drawable.average_4_14,
                                        R.drawable.average_4_15,
                                        R.drawable.average_4_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_5,
                                new int[] {
                                        R.drawable.average_5_1,
                                        R.drawable.average_5_2,
                                        R.drawable.average_5_3,
                                        R.drawable.average_5_4,
                                        R.drawable.average_5_5,
                                        R.drawable.average_5_6,
                                        R.drawable.average_5_7,
                                        R.drawable.average_5_8,
                                        R.drawable.average_5_9,
                                        R.drawable.average_5_10,
                                        R.drawable.average_5_11,
                                        R.drawable.average_5_12,
                                        R.drawable.average_5_13,
                                        R.drawable.average_5_14,
                                        R.drawable.average_5_15,
                                        R.drawable.average_5_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_6,
                                new int[] {
                                        R.drawable.average_6_1,
                                        R.drawable.average_6_2,
                                        R.drawable.average_6_3,
                                        R.drawable.average_6_4,
                                        R.drawable.average_6_5,
                                        R.drawable.average_6_6,
                                        R.drawable.average_6_7,
                                        R.drawable.average_6_8,
                                        R.drawable.average_6_9,
                                        R.drawable.average_6_10,
                                        R.drawable.average_6_11,
                                        R.drawable.average_6_12,
                                        R.drawable.average_6_13,
                                        R.drawable.average_6_14,
                                        R.drawable.average_6_15,
                                        R.drawable.average_6_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_7,
                                new int[] {
                                        R.drawable.average_7_1,
                                        R.drawable.average_7_2,
                                        R.drawable.average_7_3,
                                        R.drawable.average_7_4,
                                        R.drawable.average_7_5,
                                        R.drawable.average_7_6,
                                        R.drawable.average_7_7,
                                        R.drawable.average_7_8,
                                        R.drawable.average_7_9,
                                        R.drawable.average_7_10,
                                        R.drawable.average_7_11,
                                        R.drawable.average_7_12,
                                        R.drawable.average_7_13,
                                        R.drawable.average_7_14,
                                        R.drawable.average_7_15,
                                        R.drawable.average_7_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_8,
                                new int[] {
                                        R.drawable.average_8_1,
                                        R.drawable.average_8_2,
                                        R.drawable.average_8_3,
                                        R.drawable.average_8_4,
                                        R.drawable.average_8_5,
                                        R.drawable.average_8_6,
                                        R.drawable.average_8_7,
                                        R.drawable.average_8_8,
                                        R.drawable.average_8_9,
                                        R.drawable.average_8_10,
                                        R.drawable.average_8_11,
                                        R.drawable.average_8_12,
                                        R.drawable.average_8_13,
                                        R.drawable.average_8_14,
                                        R.drawable.average_8_15,
                                        R.drawable.average_8_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_9,
                                new int[] {
                                        R.drawable.average_9_1,
                                        R.drawable.average_9_2,
                                        R.drawable.average_9_3,
                                        R.drawable.average_9_4,
                                        R.drawable.average_9_5,
                                        R.drawable.average_9_6,
                                        R.drawable.average_9_7,
                                        R.drawable.average_9_8,
                                        R.drawable.average_9_9,
                                        R.drawable.average_9_10,
                                        R.drawable.average_9_11,
                                        R.drawable.average_9_12,
                                        R.drawable.average_9_13,
                                        R.drawable.average_9_14,
                                        R.drawable.average_9_15,
                                        R.drawable.average_9_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_10,
                                new int[] {
                                        R.drawable.average_10_1,
                                        R.drawable.average_10_2,
                                        R.drawable.average_10_3,
                                        R.drawable.average_10_4,
                                        R.drawable.average_10_5,
                                        R.drawable.average_10_6,
                                        R.drawable.average_10_7,
                                        R.drawable.average_10_8,
                                        R.drawable.average_10_9,
                                        R.drawable.average_10_10,
                                        R.drawable.average_10_11,
                                        R.drawable.average_10_12,
                                        R.drawable.average_10_13,
                                        R.drawable.average_10_14,
                                        R.drawable.average_10_15,
                                        R.drawable.average_10_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_11,
                                new int[] {
                                        R.drawable.average_11_1,
                                        R.drawable.average_11_2,
                                        R.drawable.average_11_3,
                                        R.drawable.average_11_4,
                                        R.drawable.average_11_5,
                                        R.drawable.average_11_6,
                                        R.drawable.average_11_7,
                                        R.drawable.average_11_8,
                                        R.drawable.average_11_9,
                                        R.drawable.average_11_10,
                                        R.drawable.average_11_11,
                                        R.drawable.average_11_12,
                                        R.drawable.average_11_13,
                                        R.drawable.average_11_14,
                                        R.drawable.average_11_15,
                                        R.drawable.average_11_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_12,
                                new int[] {
                                        R.drawable.average_12_1,
                                        R.drawable.average_12_2,
                                        R.drawable.average_12_3,
                                        R.drawable.average_12_4,
                                        R.drawable.average_12_5,
                                        R.drawable.average_12_6,
                                        R.drawable.average_12_7,
                                        R.drawable.average_12_8,
                                        R.drawable.average_12_9,
                                        R.drawable.average_12_10,
                                        R.drawable.average_12_11,
                                        R.drawable.average_12_12,
                                        R.drawable.average_12_13,
                                        R.drawable.average_12_14,
                                        R.drawable.average_12_15,
                                        R.drawable.average_12_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_13,
                                new int[] {
                                        R.drawable.average_13_1,
                                        R.drawable.average_13_2,
                                        R.drawable.average_13_3,
                                        R.drawable.average_13_4,
                                        R.drawable.average_13_5,
                                        R.drawable.average_13_6,
                                        R.drawable.average_13_7,
                                        R.drawable.average_13_8,
                                        R.drawable.average_13_9,
                                        R.drawable.average_13_10,
                                        R.drawable.average_13_11,
                                        R.drawable.average_13_12,
                                        R.drawable.average_13_13,
                                        R.drawable.average_13_14,
                                        R.drawable.average_13_15,
                                        R.drawable.average_13_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_14,
                                new int[] {
                                        R.drawable.average_14_1,
                                        R.drawable.average_14_2,
                                        R.drawable.average_14_3,
                                        R.drawable.average_14_4,
                                        R.drawable.average_14_5,
                                        R.drawable.average_14_6,
                                        R.drawable.average_14_7,
                                        R.drawable.average_14_8,
                                        R.drawable.average_14_9,
                                        R.drawable.average_14_10,
                                        R.drawable.average_14_11,
                                        R.drawable.average_14_12,
                                        R.drawable.average_14_13,
                                        R.drawable.average_14_14,
                                        R.drawable.average_14_15,
                                        R.drawable.average_14_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_15,
                                new int[] {
                                        R.drawable.average_15_1,
                                        R.drawable.average_15_2,
                                        R.drawable.average_15_3,
                                        R.drawable.average_15_4,
                                        R.drawable.average_15_5,
                                        R.drawable.average_15_6,
                                        R.drawable.average_15_7,
                                        R.drawable.average_15_8,
                                        R.drawable.average_15_9,
                                        R.drawable.average_15_10,
                                        R.drawable.average_15_11,
                                        R.drawable.average_15_12,
                                        R.drawable.average_15_13,
                                        R.drawable.average_15_14,
                                        R.drawable.average_15_15,
                                        R.drawable.average_15_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_16,
                                new int[] {
                                        R.drawable.average_16_1,
                                        R.drawable.average_16_2,
                                        R.drawable.average_16_3,
                                        R.drawable.average_16_4,
                                        R.drawable.average_16_5,
                                        R.drawable.average_16_6,
                                        R.drawable.average_16_7,
                                        R.drawable.average_16_8,
                                        R.drawable.average_16_9,
                                        R.drawable.average_16_10,
                                        R.drawable.average_16_11,
                                        R.drawable.average_16_12,
                                        R.drawable.average_16_13,
                                        R.drawable.average_16_14,
                                        R.drawable.average_16_15,
                                        R.drawable.average_16_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_17,
                                new int[] {
                                        R.drawable.average_17_1,
                                        R.drawable.average_17_2,
                                        R.drawable.average_17_3,
                                        R.drawable.average_17_4,
                                        R.drawable.average_17_5,
                                        R.drawable.average_17_6,
                                        R.drawable.average_17_7,
                                        R.drawable.average_17_8,
                                        R.drawable.average_17_9,
                                        R.drawable.average_17_10,
                                        R.drawable.average_17_11,
                                        R.drawable.average_17_12,
                                        R.drawable.average_17_13,
                                        R.drawable.average_17_14,
                                        R.drawable.average_17_15,
                                        R.drawable.average_17_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_18,
                                new int[] {
                                        R.drawable.average_18_1,
                                        R.drawable.average_18_2,
                                        R.drawable.average_18_3,
                                        R.drawable.average_18_4,
                                        R.drawable.average_18_5,
                                        R.drawable.average_18_6,
                                        R.drawable.average_18_7,
                                        R.drawable.average_18_8,
                                        R.drawable.average_18_9,
                                        R.drawable.average_18_10,
                                        R.drawable.average_18_11,
                                        R.drawable.average_18_12,
                                        R.drawable.average_18_13,
                                        R.drawable.average_18_14,
                                        R.drawable.average_18_15,
                                        R.drawable.average_18_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_19,
                                new int[] {
                                        R.drawable.average_19_1,
                                        R.drawable.average_19_2,
                                        R.drawable.average_19_3,
                                        R.drawable.average_19_4,
                                        R.drawable.average_19_5,
                                        R.drawable.average_19_6,
                                        R.drawable.average_19_7,
                                        R.drawable.average_19_8,
                                        R.drawable.average_19_9,
                                        R.drawable.average_19_10,
                                        R.drawable.average_19_11,
                                        R.drawable.average_19_12,
                                        R.drawable.average_19_13,
                                        R.drawable.average_19_14,
                                        R.drawable.average_19_15,
                                        R.drawable.average_19_16,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.average_20,
                                new int[] {
                                        R.drawable.average_20_1,
                                        R.drawable.average_20_2,
                                        R.drawable.average_20_3,
                                        R.drawable.average_20_4,
                                        R.drawable.average_20_5,
                                        R.drawable.average_20_6,
                                        R.drawable.average_20_7,
                                        R.drawable.average_20_8,
                                        R.drawable.average_20_9,
                                        R.drawable.average_20_10,
                                        R.drawable.average_20_11,
                                        R.drawable.average_20_12,
                                        R.drawable.average_20_13,
                                        R.drawable.average_20_14,
                                        R.drawable.average_20_15,
                                        R.drawable.average_20_16,
                                }) ,
                }
         );

        Global.Places.get(0).setHardJigsawPuzzle(
                new JigsawPuzzle[]{
                        new JigsawPuzzle(
                                R.drawable.hard_1,
                                new int[] {
                                        R.drawable.hard_1_1,
                                        R.drawable.hard_1_2,
                                        R.drawable.hard_1_3,
                                        R.drawable.hard_1_4,
                                        R.drawable.hard_1_5,
                                        R.drawable.hard_1_6,
                                        R.drawable.hard_1_7,
                                        R.drawable.hard_1_8,
                                        R.drawable.hard_1_9,
                                        R.drawable.hard_1_10,
                                        R.drawable.hard_1_11,
                                        R.drawable.hard_1_12,
                                        R.drawable.hard_1_13,
                                        R.drawable.hard_1_14,
                                        R.drawable.hard_1_15,
                                        R.drawable.hard_1_16,
                                        R.drawable.hard_1_17,
                                        R.drawable.hard_1_18,
                                        R.drawable.hard_1_19,
                                        R.drawable.hard_1_20,
                                        R.drawable.hard_1_21,
                                        R.drawable.hard_1_22,
                                        R.drawable.hard_1_23,
                                        R.drawable.hard_1_24,
                                        R.drawable.hard_1_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_2,
                                new int[] {
                                        R.drawable.hard_2_1,
                                        R.drawable.hard_2_2,
                                        R.drawable.hard_2_3,
                                        R.drawable.hard_2_4,
                                        R.drawable.hard_2_5,
                                        R.drawable.hard_2_6,
                                        R.drawable.hard_2_7,
                                        R.drawable.hard_2_8,
                                        R.drawable.hard_2_9,
                                        R.drawable.hard_2_10,
                                        R.drawable.hard_2_11,
                                        R.drawable.hard_2_12,
                                        R.drawable.hard_2_13,
                                        R.drawable.hard_2_14,
                                        R.drawable.hard_2_15,
                                        R.drawable.hard_2_16,
                                        R.drawable.hard_2_17,
                                        R.drawable.hard_2_18,
                                        R.drawable.hard_2_19,
                                        R.drawable.hard_2_20,
                                        R.drawable.hard_2_21,
                                        R.drawable.hard_2_22,
                                        R.drawable.hard_2_23,
                                        R.drawable.hard_2_24,
                                        R.drawable.hard_2_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_3,
                                new int[] {
                                        R.drawable.hard_3_1,
                                        R.drawable.hard_3_2,
                                        R.drawable.hard_3_3,
                                        R.drawable.hard_3_4,
                                        R.drawable.hard_3_5,
                                        R.drawable.hard_3_6,
                                        R.drawable.hard_3_7,
                                        R.drawable.hard_3_8,
                                        R.drawable.hard_3_9,
                                        R.drawable.hard_3_10,
                                        R.drawable.hard_3_11,
                                        R.drawable.hard_3_12,
                                        R.drawable.hard_3_13,
                                        R.drawable.hard_3_14,
                                        R.drawable.hard_3_15,
                                        R.drawable.hard_3_16,
                                        R.drawable.hard_3_17,
                                        R.drawable.hard_3_18,
                                        R.drawable.hard_3_19,
                                        R.drawable.hard_3_20,
                                        R.drawable.hard_3_21,
                                        R.drawable.hard_3_22,
                                        R.drawable.hard_3_23,
                                        R.drawable.hard_3_24,
                                        R.drawable.hard_3_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_4,
                                new int[] {
                                        R.drawable.hard_4_1,
                                        R.drawable.hard_4_2,
                                        R.drawable.hard_4_3,
                                        R.drawable.hard_4_4,
                                        R.drawable.hard_4_5,
                                        R.drawable.hard_4_6,
                                        R.drawable.hard_4_7,
                                        R.drawable.hard_4_8,
                                        R.drawable.hard_4_9,
                                        R.drawable.hard_4_10,
                                        R.drawable.hard_4_11,
                                        R.drawable.hard_4_12,
                                        R.drawable.hard_4_13,
                                        R.drawable.hard_4_14,
                                        R.drawable.hard_4_15,
                                        R.drawable.hard_4_16,
                                        R.drawable.hard_4_17,
                                        R.drawable.hard_4_18,
                                        R.drawable.hard_4_19,
                                        R.drawable.hard_4_20,
                                        R.drawable.hard_4_21,
                                        R.drawable.hard_4_22,
                                        R.drawable.hard_4_23,
                                        R.drawable.hard_4_24,
                                        R.drawable.hard_4_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_5,
                                new int[] {
                                        R.drawable.hard_5_1,
                                        R.drawable.hard_5_2,
                                        R.drawable.hard_5_3,
                                        R.drawable.hard_5_4,
                                        R.drawable.hard_5_5,
                                        R.drawable.hard_5_6,
                                        R.drawable.hard_5_7,
                                        R.drawable.hard_5_8,
                                        R.drawable.hard_5_9,
                                        R.drawable.hard_5_10,
                                        R.drawable.hard_5_11,
                                        R.drawable.hard_5_12,
                                        R.drawable.hard_5_13,
                                        R.drawable.hard_5_14,
                                        R.drawable.hard_5_15,
                                        R.drawable.hard_5_16,
                                        R.drawable.hard_5_17,
                                        R.drawable.hard_5_18,
                                        R.drawable.hard_5_19,
                                        R.drawable.hard_5_20,
                                        R.drawable.hard_5_21,
                                        R.drawable.hard_5_22,
                                        R.drawable.hard_5_23,
                                        R.drawable.hard_5_24,
                                        R.drawable.hard_5_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_6,
                                new int[] {
                                        R.drawable.hard_6_1,
                                        R.drawable.hard_6_2,
                                        R.drawable.hard_6_3,
                                        R.drawable.hard_6_4,
                                        R.drawable.hard_6_5,
                                        R.drawable.hard_6_6,
                                        R.drawable.hard_6_7,
                                        R.drawable.hard_6_8,
                                        R.drawable.hard_6_9,
                                        R.drawable.hard_6_10,
                                        R.drawable.hard_6_11,
                                        R.drawable.hard_6_12,
                                        R.drawable.hard_6_13,
                                        R.drawable.hard_6_14,
                                        R.drawable.hard_6_15,
                                        R.drawable.hard_6_16,
                                        R.drawable.hard_6_17,
                                        R.drawable.hard_6_18,
                                        R.drawable.hard_6_19,
                                        R.drawable.hard_6_20,
                                        R.drawable.hard_6_21,
                                        R.drawable.hard_6_22,
                                        R.drawable.hard_6_23,
                                        R.drawable.hard_6_24,
                                        R.drawable.hard_6_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_7,
                                new int[] {
                                        R.drawable.hard_7_1,
                                        R.drawable.hard_7_2,
                                        R.drawable.hard_7_3,
                                        R.drawable.hard_7_4,
                                        R.drawable.hard_7_5,
                                        R.drawable.hard_7_6,
                                        R.drawable.hard_7_7,
                                        R.drawable.hard_7_8,
                                        R.drawable.hard_7_9,
                                        R.drawable.hard_7_10,
                                        R.drawable.hard_7_11,
                                        R.drawable.hard_7_12,
                                        R.drawable.hard_7_13,
                                        R.drawable.hard_7_14,
                                        R.drawable.hard_7_15,
                                        R.drawable.hard_7_16,
                                        R.drawable.hard_7_17,
                                        R.drawable.hard_7_18,
                                        R.drawable.hard_7_19,
                                        R.drawable.hard_7_20,
                                        R.drawable.hard_7_21,
                                        R.drawable.hard_7_22,
                                        R.drawable.hard_7_23,
                                        R.drawable.hard_7_24,
                                        R.drawable.hard_7_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_8,
                                new int[] {
                                        R.drawable.hard_8_1,
                                        R.drawable.hard_8_2,
                                        R.drawable.hard_8_3,
                                        R.drawable.hard_8_4,
                                        R.drawable.hard_8_5,
                                        R.drawable.hard_8_6,
                                        R.drawable.hard_8_7,
                                        R.drawable.hard_8_8,
                                        R.drawable.hard_8_9,
                                        R.drawable.hard_8_10,
                                        R.drawable.hard_8_11,
                                        R.drawable.hard_8_12,
                                        R.drawable.hard_8_13,
                                        R.drawable.hard_8_14,
                                        R.drawable.hard_8_15,
                                        R.drawable.hard_8_16,
                                        R.drawable.hard_8_17,
                                        R.drawable.hard_8_18,
                                        R.drawable.hard_8_19,
                                        R.drawable.hard_8_20,
                                        R.drawable.hard_8_21,
                                        R.drawable.hard_8_22,
                                        R.drawable.hard_8_23,
                                        R.drawable.hard_8_24,
                                        R.drawable.hard_8_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_9,
                                new int[] {
                                        R.drawable.hard_9_1,
                                        R.drawable.hard_9_2,
                                        R.drawable.hard_9_3,
                                        R.drawable.hard_9_4,
                                        R.drawable.hard_9_5,
                                        R.drawable.hard_9_6,
                                        R.drawable.hard_9_7,
                                        R.drawable.hard_9_8,
                                        R.drawable.hard_9_9,
                                        R.drawable.hard_9_10,
                                        R.drawable.hard_9_11,
                                        R.drawable.hard_9_12,
                                        R.drawable.hard_9_13,
                                        R.drawable.hard_9_14,
                                        R.drawable.hard_9_15,
                                        R.drawable.hard_9_16,
                                        R.drawable.hard_9_17,
                                        R.drawable.hard_9_18,
                                        R.drawable.hard_9_19,
                                        R.drawable.hard_9_20,
                                        R.drawable.hard_9_21,
                                        R.drawable.hard_9_22,
                                        R.drawable.hard_9_23,
                                        R.drawable.hard_9_24,
                                        R.drawable.hard_9_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_10,
                                new int[] {
                                        R.drawable.hard_10_1,
                                        R.drawable.hard_10_2,
                                        R.drawable.hard_10_3,
                                        R.drawable.hard_10_4,
                                        R.drawable.hard_10_5,
                                        R.drawable.hard_10_6,
                                        R.drawable.hard_10_7,
                                        R.drawable.hard_10_8,
                                        R.drawable.hard_10_9,
                                        R.drawable.hard_10_10,
                                        R.drawable.hard_10_11,
                                        R.drawable.hard_10_12,
                                        R.drawable.hard_10_13,
                                        R.drawable.hard_10_14,
                                        R.drawable.hard_10_15,
                                        R.drawable.hard_10_16,
                                        R.drawable.hard_10_17,
                                        R.drawable.hard_10_18,
                                        R.drawable.hard_10_19,
                                        R.drawable.hard_10_20,
                                        R.drawable.hard_10_21,
                                        R.drawable.hard_10_22,
                                        R.drawable.hard_10_23,
                                        R.drawable.hard_10_24,
                                        R.drawable.hard_10_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_11,
                                new int[] {
                                        R.drawable.hard_11_1,
                                        R.drawable.hard_11_2,
                                        R.drawable.hard_11_3,
                                        R.drawable.hard_11_4,
                                        R.drawable.hard_11_5,
                                        R.drawable.hard_11_6,
                                        R.drawable.hard_11_7,
                                        R.drawable.hard_11_8,
                                        R.drawable.hard_11_9,
                                        R.drawable.hard_11_10,
                                        R.drawable.hard_11_11,
                                        R.drawable.hard_11_12,
                                        R.drawable.hard_11_13,
                                        R.drawable.hard_11_14,
                                        R.drawable.hard_11_15,
                                        R.drawable.hard_11_16,
                                        R.drawable.hard_11_17,
                                        R.drawable.hard_11_18,
                                        R.drawable.hard_11_19,
                                        R.drawable.hard_11_20,
                                        R.drawable.hard_11_21,
                                        R.drawable.hard_11_22,
                                        R.drawable.hard_11_23,
                                        R.drawable.hard_11_24,
                                        R.drawable.hard_11_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_12,
                                new int[] {
                                        R.drawable.hard_12_1,
                                        R.drawable.hard_12_2,
                                        R.drawable.hard_12_3,
                                        R.drawable.hard_12_4,
                                        R.drawable.hard_12_5,
                                        R.drawable.hard_12_6,
                                        R.drawable.hard_12_7,
                                        R.drawable.hard_12_8,
                                        R.drawable.hard_12_9,
                                        R.drawable.hard_12_10,
                                        R.drawable.hard_12_11,
                                        R.drawable.hard_12_12,
                                        R.drawable.hard_12_13,
                                        R.drawable.hard_12_14,
                                        R.drawable.hard_12_15,
                                        R.drawable.hard_12_16,
                                        R.drawable.hard_12_17,
                                        R.drawable.hard_12_18,
                                        R.drawable.hard_12_19,
                                        R.drawable.hard_12_20,
                                        R.drawable.hard_12_21,
                                        R.drawable.hard_12_22,
                                        R.drawable.hard_12_23,
                                        R.drawable.hard_12_24,
                                        R.drawable.hard_12_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_13,
                                new int[] {
                                        R.drawable.hard_13_1,
                                        R.drawable.hard_13_2,
                                        R.drawable.hard_13_3,
                                        R.drawable.hard_13_4,
                                        R.drawable.hard_13_5,
                                        R.drawable.hard_13_6,
                                        R.drawable.hard_13_7,
                                        R.drawable.hard_13_8,
                                        R.drawable.hard_13_9,
                                        R.drawable.hard_13_10,
                                        R.drawable.hard_13_11,
                                        R.drawable.hard_13_12,
                                        R.drawable.hard_13_13,
                                        R.drawable.hard_13_14,
                                        R.drawable.hard_13_15,
                                        R.drawable.hard_13_16,
                                        R.drawable.hard_13_17,
                                        R.drawable.hard_13_18,
                                        R.drawable.hard_13_19,
                                        R.drawable.hard_13_20,
                                        R.drawable.hard_13_21,
                                        R.drawable.hard_13_22,
                                        R.drawable.hard_13_23,
                                        R.drawable.hard_13_24,
                                        R.drawable.hard_13_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_14,
                                new int[] {
                                        R.drawable.hard_14_1,
                                        R.drawable.hard_14_2,
                                        R.drawable.hard_14_3,
                                        R.drawable.hard_14_4,
                                        R.drawable.hard_14_5,
                                        R.drawable.hard_14_6,
                                        R.drawable.hard_14_7,
                                        R.drawable.hard_14_8,
                                        R.drawable.hard_14_9,
                                        R.drawable.hard_14_10,
                                        R.drawable.hard_14_11,
                                        R.drawable.hard_14_12,
                                        R.drawable.hard_14_13,
                                        R.drawable.hard_14_14,
                                        R.drawable.hard_14_15,
                                        R.drawable.hard_14_16,
                                        R.drawable.hard_14_17,
                                        R.drawable.hard_14_18,
                                        R.drawable.hard_14_19,
                                        R.drawable.hard_14_20,
                                        R.drawable.hard_14_21,
                                        R.drawable.hard_14_22,
                                        R.drawable.hard_14_23,
                                        R.drawable.hard_14_24,
                                        R.drawable.hard_14_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_15,
                                new int[] {
                                        R.drawable.hard_15_1,
                                        R.drawable.hard_15_2,
                                        R.drawable.hard_15_3,
                                        R.drawable.hard_15_4,
                                        R.drawable.hard_15_5,
                                        R.drawable.hard_15_6,
                                        R.drawable.hard_15_7,
                                        R.drawable.hard_15_8,
                                        R.drawable.hard_15_9,
                                        R.drawable.hard_15_10,
                                        R.drawable.hard_15_11,
                                        R.drawable.hard_15_12,
                                        R.drawable.hard_15_13,
                                        R.drawable.hard_15_14,
                                        R.drawable.hard_15_15,
                                        R.drawable.hard_15_16,
                                        R.drawable.hard_15_17,
                                        R.drawable.hard_15_18,
                                        R.drawable.hard_15_19,
                                        R.drawable.hard_15_20,
                                        R.drawable.hard_15_21,
                                        R.drawable.hard_15_22,
                                        R.drawable.hard_15_23,
                                        R.drawable.hard_15_24,
                                        R.drawable.hard_15_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_16,
                                new int[] {
                                        R.drawable.hard_16_1,
                                        R.drawable.hard_16_2,
                                        R.drawable.hard_15_3,
                                        R.drawable.hard_16_4,
                                        R.drawable.hard_16_5,
                                        R.drawable.hard_16_6,
                                        R.drawable.hard_16_7,
                                        R.drawable.hard_16_8,
                                        R.drawable.hard_16_9,
                                        R.drawable.hard_16_10,
                                        R.drawable.hard_16_11,
                                        R.drawable.hard_16_12,
                                        R.drawable.hard_16_13,
                                        R.drawable.hard_16_14,
                                        R.drawable.hard_16_15,
                                        R.drawable.hard_16_16,
                                        R.drawable.hard_16_17,
                                        R.drawable.hard_16_18,
                                        R.drawable.hard_16_19,
                                        R.drawable.hard_16_20,
                                        R.drawable.hard_16_21,
                                        R.drawable.hard_16_22,
                                        R.drawable.hard_16_23,
                                        R.drawable.hard_16_24,
                                        R.drawable.hard_16_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_17,
                                new int[] {
                                        R.drawable.hard_17_1,
                                        R.drawable.hard_17_2,
                                        R.drawable.hard_17_3,
                                        R.drawable.hard_17_4,
                                        R.drawable.hard_17_5,
                                        R.drawable.hard_17_6,
                                        R.drawable.hard_17_7,
                                        R.drawable.hard_17_8,
                                        R.drawable.hard_17_9,
                                        R.drawable.hard_17_10,
                                        R.drawable.hard_17_11,
                                        R.drawable.hard_17_12,
                                        R.drawable.hard_17_13,
                                        R.drawable.hard_17_14,
                                        R.drawable.hard_17_15,
                                        R.drawable.hard_17_16,
                                        R.drawable.hard_17_17,
                                        R.drawable.hard_17_18,
                                        R.drawable.hard_17_19,
                                        R.drawable.hard_17_20,
                                        R.drawable.hard_17_21,
                                        R.drawable.hard_17_22,
                                        R.drawable.hard_17_23,
                                        R.drawable.hard_17_24,
                                        R.drawable.hard_17_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_18,
                                new int[] {
                                        R.drawable.hard_18_1,
                                        R.drawable.hard_18_2,
                                        R.drawable.hard_18_3,
                                        R.drawable.hard_18_4,
                                        R.drawable.hard_18_5,
                                        R.drawable.hard_18_6,
                                        R.drawable.hard_18_7,
                                        R.drawable.hard_18_8,
                                        R.drawable.hard_18_9,
                                        R.drawable.hard_18_10,
                                        R.drawable.hard_18_11,
                                        R.drawable.hard_18_12,
                                        R.drawable.hard_18_13,
                                        R.drawable.hard_18_14,
                                        R.drawable.hard_18_15,
                                        R.drawable.hard_18_16,
                                        R.drawable.hard_18_17,
                                        R.drawable.hard_18_18,
                                        R.drawable.hard_18_19,
                                        R.drawable.hard_18_20,
                                        R.drawable.hard_18_21,
                                        R.drawable.hard_18_22,
                                        R.drawable.hard_18_23,
                                        R.drawable.hard_18_24,
                                        R.drawable.hard_18_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_19,
                                new int[] {
                                        R.drawable.hard_19_1,
                                        R.drawable.hard_19_2,
                                        R.drawable.hard_19_3,
                                        R.drawable.hard_19_4,
                                        R.drawable.hard_19_5,
                                        R.drawable.hard_19_6,
                                        R.drawable.hard_19_7,
                                        R.drawable.hard_19_8,
                                        R.drawable.hard_19_9,
                                        R.drawable.hard_19_10,
                                        R.drawable.hard_19_11,
                                        R.drawable.hard_19_12,
                                        R.drawable.hard_19_13,
                                        R.drawable.hard_19_14,
                                        R.drawable.hard_19_15,
                                        R.drawable.hard_19_16,
                                        R.drawable.hard_19_17,
                                        R.drawable.hard_19_18,
                                        R.drawable.hard_19_19,
                                        R.drawable.hard_19_20,
                                        R.drawable.hard_19_21,
                                        R.drawable.hard_19_22,
                                        R.drawable.hard_19_23,
                                        R.drawable.hard_19_24,
                                        R.drawable.hard_19_25,
                                }) ,
                        new JigsawPuzzle(
                                R.drawable.hard_20,
                                new int[] {
                                        R.drawable.hard_20_1,
                                        R.drawable.hard_20_2,
                                        R.drawable.hard_20_3,
                                        R.drawable.hard_20_4,
                                        R.drawable.hard_20_5,
                                        R.drawable.hard_20_6,
                                        R.drawable.hard_20_7,
                                        R.drawable.hard_20_8,
                                        R.drawable.hard_20_9,
                                        R.drawable.hard_20_10,
                                        R.drawable.hard_20_11,
                                        R.drawable.hard_20_12,
                                        R.drawable.hard_20_13,
                                        R.drawable.hard_20_14,
                                        R.drawable.hard_20_15,
                                        R.drawable.hard_20_16,
                                        R.drawable.hard_20_17,
                                        R.drawable.hard_20_18,
                                        R.drawable.hard_20_19,
                                        R.drawable.hard_20_20,
                                        R.drawable.hard_20_21,
                                        R.drawable.hard_20_22,
                                        R.drawable.hard_20_23,
                                        R.drawable.hard_20_24,
                                        R.drawable.hard_20_25,
                                }) ,
                }


        );


        //******************* SET GUESS THE PICTURE *********************//

//                Global.Places.get(0).setGuessTheWord(
//                        new GuessTheWord[] {
//                                new GuessTheWord("deer", R.drawable.dear),
//                                new GuessTheWord("fox", R.drawable.fox),
//                                new GuessTheWord("dolphin", R.drawable.dolphin),
//                                new GuessTheWord("duck", R.drawable.duck),
//                                new GuessTheWord("eagle", R.drawable.eagle),
//                                new GuessTheWord("parrot", R.drawable.parrot),
//                                new GuessTheWord("pig", R.drawable.pig),
//                                new GuessTheWord("rat", R.drawable.rat),
//                                new GuessTheWord("turtle", R.drawable.turtle),
//                                new GuessTheWord("viper", R.drawable.viper),
//                                new GuessTheWord("warbler", R.drawable.warbler),
//                                new GuessTheWord("bitatawa", R.drawable.bitatawa),
//                                new GuessTheWord("celestial", R.drawable.easy_18),
//                                new GuessTheWord("dwarfking", R.drawable.easy_11),
//                                new GuessTheWord("crocodile", R.drawable.easy_12),
//                                new GuessTheWord("malkoha", R.drawable.easy_7),
//                                new GuessTheWord("pahutan", R.drawable.tree_1),
//                                new GuessTheWord("pili", R.drawable.tree_2),
//                                new GuessTheWord("katmon", R.drawable.tree_3),
//                                new GuessTheWord("hornbill", R.drawable.easy_4),
//                        }
//                );

        Global.Places.get(0).setGuessTheWord(
                new GuessTheWord[] {
                        new GuessTheWord("deer", R.drawable.dear),
                        new GuessTheWord("bat", R.drawable.fox),
                        new GuessTheWord("eagle", R.drawable.eagle),
                        new GuessTheWord("pig", R.drawable.pig),
                        new GuessTheWord("rat", R.drawable.rat),
                        new GuessTheWord("duck", R.drawable.duck),
                        new GuessTheWord("viper", R.drawable.viper),
                        new GuessTheWord("pili", R.drawable.tree_2),
                        new GuessTheWord("yakal", R.drawable.tree_8),
                        new GuessTheWord("guijo", R.drawable.tree_9),
                        new GuessTheWord("narra", R.drawable.tree_10),

                        new GuessTheWord("dolphin", R.drawable.dolphin),
                        new GuessTheWord("parrot", R.drawable.parrot),
                        new GuessTheWord("turtle", R.drawable.turtle),
                        new GuessTheWord("warbler", R.drawable.warbler),
                        new GuessTheWord("bitatawa", R.drawable.san_mariano_animal_14),
                        new GuessTheWord("pahutan", R.drawable.tree_1),
                        new GuessTheWord("katmon", R.drawable.tree_3),
                        new GuessTheWord("hornbill", R.drawable.easy_4),
                        new GuessTheWord("malkoha", R.drawable.easy_7),
                }
        );

        Global.Places.get(0).setInterMediateGuessTheWord(
                new GuessTheWord[] {
                        new GuessTheWord("dolphin", R.drawable.dolphin),
                        new GuessTheWord("parrot", R.drawable.parrot),
                        new GuessTheWord("turtle", R.drawable.turtle),
                        new GuessTheWord("warbler", R.drawable.warbler),
                        new GuessTheWord("bitatawa", R.drawable.san_mariano_animal_14),
                        new GuessTheWord("pahutan", R.drawable.tree_1),
                        new GuessTheWord("katmon", R.drawable.tree_3),
                        new GuessTheWord("hornbill", R.drawable.easy_4),
                        new GuessTheWord("malkoha", R.drawable.easy_7),
                        new GuessTheWord("oriole", R.drawable.san_mariano_animal_16),

                        new GuessTheWord("montane", R.drawable.san_pablo_animal_2),

                        new GuessTheWord("celestial", R.drawable.divilacan_animal_21),
                        new GuessTheWord("dwarfking", R.drawable.easy_11),
                        new GuessTheWord("crocodile", R.drawable.easy_12),
                        new GuessTheWord("flyingfox", R.drawable.fox),
                        new GuessTheWord("hawksbill", R.drawable.turtle),
                        new GuessTheWord("browndeer", R.drawable.dear),
                        new GuessTheWord("kingfisher", R.drawable.cabagan_animal_11),
                        new GuessTheWord("pitviper", R.drawable.viper),
                        new GuessTheWord("wartypig", R.drawable.pig),
                        new GuessTheWord("warbler", R.drawable.dinapigue_animal_8),
                }
        );

        Global.Places.get(0).setHardGuessTheWord(
                new GuessTheWord[] {
                        new GuessTheWord("celestial", R.drawable.divilacan_animal_21),
                        new GuessTheWord("dwarfking", R.drawable.easy_11),
                        new GuessTheWord("crocodile", R.drawable.easy_12),
                        new GuessTheWord("flyingfox", R.drawable.fox),
                        new GuessTheWord("hawksbill", R.drawable.turtle),
                        new GuessTheWord("browndeer", R.drawable.dear),
                        new GuessTheWord("kingfisher", R.drawable.cabagan_animal_11),
                        new GuessTheWord("pitviper", R.drawable.viper),
                        new GuessTheWord("wartypig", R.drawable.pig),
                        new GuessTheWord("warbler", R.drawable.dinapigue_animal_8),

                        new GuessTheWord("seaturtle", R.drawable.dinapigue_animal_14),
                        new GuessTheWord("parrot", R.drawable.parrot),
                        new GuessTheWord("turtle", R.drawable.turtle),
                        new GuessTheWord("warbler", R.drawable.warbler),
                        new GuessTheWord("bitatawa", R.drawable.san_mariano_animal_14),
                        new GuessTheWord("pahutan", R.drawable.tree_1),
                        new GuessTheWord("katmon", R.drawable.tree_3),
                        new GuessTheWord("hornbill", R.drawable.easy_4),
                        new GuessTheWord("malkoha", R.drawable.easy_7),
                        new GuessTheWord("oriole", R.drawable.san_mariano_animal_16),
                }
        );

        //******************* SET QUESTIONS *********************//

        Global.Places.get(0).setQuestions(new Question[]{
                new Question(
                        "What is the smallest hornbill that is found only in Luzon Philippines?",
                        new String[] {
                                "a) Green Racquet-tail ",
                                "b) Luzon Hornbill"
                        },
                        "b) Luzon Hornbill"
                ),
                new Question(
                        "Green Racquet-tail is lowland relative of the?",
                        new String[] {
                                "a) Montane Racquet-tail  ",
                                "b) Warty pig"
                        },
                        "a) Montane Racquet-tail  "
                ),
                new Question(
                        "_________ is one of four known species in the pig genus.",
                        new String[] {
                                "a) Warty pig ",
                                "b) Philippine Brown Deer"
                        },
                        "a) Warty pig "
                ),
                new Question(
                        "_________ it is generally thriving in the terrestrial environment.",
                        new String[] {
                                "a) Philippine Brown Deer",
                                "b) Philippine Pit Viper"
                        },
                        "a) Philippine Brown Deer"
                ),
                new Question(
                        "_________ it  is a species of cuckooin the family Cuculidae.",
                        new String[] {
                                "a) Philippine Hanging Parrot",
                                "b) Scale-feathered Malkoha"
                        },
                        "b) Scale-feathered Malkoha"
                ),
                new Question(
                        "_________ is a bird that is endemic to Luzon and satellite Islands.",
                        new String[] {
                                "a) Northern Rufous Hornbill",
                                "b) Philippine Brown Deer"
                        },
                        "a) Northern Rufous Hornbill"
                ),
                new Question(
                        "It is a kind of bird that is about 14 cm (5.5 in) long, weigh 32–40g, and have a short rounded tail.",
                        new String[] {
                                "a) Montane Racquet-tail",
                                "b) Philippine Hanging Parrot"
                        },
                        "b) Philippine Hanging Parrot"
                ),
                new Question(
                        "It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly",
                        new String[] {
                                "a) Sierra Madre Ground Warbler",
                                "b) Luzon Hornbill"
                        },
                        "a) Sierra Madre Ground Warbler"
                ),
                new Question(
                        "A species it has overlapping scutes on its shell.",
                        new String[] {
                                "a) Hawksbill Sea Turtle",
                                "b) Philippine Brown Deer"
                        },
                        "a) Hawksbill Sea Turtle"
                ),
                new Question(
                        "The _________ is a rather distinctive bird.",
                        new String[] {
                                "a) Scale-feathered Malkoha",
                                "b) Philippine Duck"
                        },
                        "b) Philippine Duck"
                ),
                new Question(
                        "Local name of Isabela Oriole?",
                        new String[] {
                                "a) Tarictic",
                                "b) Kiyaw"
                        },
                        "b) Kiyaw"
                ),
                new Question(
                        "Local name of Philippine Eagle?",
                        new String[] {
                                "a.) Agila",
                                "b.) Tarictic"
                        },
                        "a.) Agila"
                ),
                new Question(
                        "It is one of the largest crocodile in the world",
                        new String[] {
                                "a.) Pilippine Crocodile",
                                "b.) Salt water Crocodile"
                        },
                        "b.) Salt water Crocodile"
                ),
                new Question(
                        "It is a kind of animals in which has a golden-yellow spots on its body and large strong claws to climb trees.",
                        new String[] {
                                "a.) Philippine Pit Viper",
                                "b.) Bitatawa"
                        },
                        "b.) Bitatawa"
                ),
                new Question(
                        "It is have the second largest encephalization levels of any mammal on Earth.",
                        new String[] {
                                "a.) Bottle-nosed Dolphin",
                                "b.) Tarictic"
                        },
                        "a.) Bottle-nosed Dolphin"
                ),
                new Question(
                        "This species is feeds on sea grasses",
                        new String[] {
                                "a.) Hawksbill Sea Turtle",
                                "b.) Green Sea Turtle "
                        },
                        "b.) Green Sea Turtle "
                ),
                new Question(
                        "The Philippine dwarf kingfisher is a species of bird in the family ________?",
                        new String[] {
                                "a.) Green Racquet-tail",
                                "b.) Alcedinidae"
                        },
                        "b.) Alcedinidae"
                ),
                new Question(
                        "It is a venomous snake speciesendemic to the Philippines.",
                        new String[] {
                                "a.) Philippine Pit Viper",
                                "b.) Philippine Hanging Parrot"
                        },
                        "a.) Philippine Pit Viper"
                ),
                new Question(
                        "This animal found is only found northern and central part of Luzon, the Philippines.",
                        new String[] {
                                "a.) Northern Luzon Giant Cloud Rat",
                                "b.) Bottle-nosed Dolphin"
                        },
                        "a.) Northern Luzon Giant Cloud Rat"
                ),
                new Question(
                        "This species is one of the rarest crocodile species in the world",
                        new String[] {
                                "a.) Philippine Crocodile",
                                "b.) Saltwater Crocodile"
                        },
                        "a.) Philippine Crocodile"
                ),
                new Question(
                        "It is the heaviest bat in the world.",
                        new String[] {
                                "a.) Golden-crowned Flying Fox",
                                "b.) Large Flying Fox"
                        },
                        "a.) Golden-crowned Flying Fox"
                ),
                new Question(
                        "It is known as the greater flying fox.",
                        new String[] {
                                "a.) Golden-crowned Flying Fox",
                                "b.) Large Flying Fox"
                        },
                        "b.) Large Flying Fox"
                ),
                new Question(
                        "It is a species of bird in the family Monarchidae",
                        new String[] {
                                "a.) Celestial Monarch",
                                "b.) Philippine Hanging Parrot"
                        },
                        "a.) Celestial Monarch"
                ),
                new Question(
                        "What is the national bird of the Philippines?",
                        new String[] {
                                "a.) Philippine Eagle",
                                "b.) Isabela Oriol"
                        },
                        "a.) Philippine Eagle"
                ),
                new Question(
                        "Green Sea Turtle feeds on _______",
                        new String[] {
                                "a.) Fruits",
                                "b.) Sea Grasses"
                        },
                        "b.) Sea Grasses"
                ),
        });

        Global.Places.get(0).setIntermediateQuestions(new Question[]{
                new Question(
                        "Local name of Luzon Hornbill?",
                        new String[]{
                                "a.) Kalaw",
                                "b.) Kulasis",
                                "c.) Tarictic",
                        },
                        "c.) Tarictic"
                ),
                new Question(
                        "The Green Racquet-tail is the lowland relative of the ________?",
                        new String[]{
                                "a.) Hawksbill Sea Turtle",
                                "b.) Montane Racquet-tail",
                                "c.) Warty Pig",
                        },
                        "b.) Montane Racquet-tail"
                ),
                new Question(
                        "This species has two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw",
                        new String[]{
                                "a.) Warty Pig ",
                                "b.) Bitatawa",
                                "c.) Saltwater Crocodile",
                        },
                        "a.) Warty Pig "
                ),
                new Question(
                        "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least?",
                        new String[]{
                                "a.) 2,900m",
                                "b.) 2,000m",
                                "c.) 2,600m",
                        },
                        "a.) 2,900m"
                ),
                new Question(
                        "Local name of Golden-crowned Flying Fox",
                        new String[]{
                                "a.) Paniki",
                                "b.) Bukarot",
                                "c.) Kalaw",
                        },
                        "a.)Paniki"
                ),
                new Question(
                        "Conservation status of Large Flying Fox",
                        new String[]{
                                "a.) Least concern",
                                "b.) Near Threatened ",
                                "c.) Endangered",
                        },
                        "b.) Near Threatened "
                ),
                new Question(
                        "The celestial monarch is a species of bird in the family",
                        new String[]{
                                "a.) Monarchidae",
                                "b.) Alcedinidae",
                                "c.) Cuculidae",
                        },
                        "a.) Monarchidae"
                ),
                new Question(
                        "The ___________ is a species of bird in the family Alcedinidae.",
                        new String[]{
                                "a.) Philippine dwarf kingfisher ",
                                "b.) Philippine Brown Deer",
                                "c.) Green Racquet-tail",
                        },
                        "a.) Philippine dwarf kingfisher "
                ),
                new Question(
                        "Conservation status of Philippine pit viper",
                        new String[]{
                                "a.) Least concern",
                                "b.) Near Threatened ",
                                "c.) Endangered",
                        },
                        "a.) Least concern"
                ),
                new Question(
                        "The northern Luzon giant cloud rat occurs from sea level to an altitude of about _______ meters",
                        new String[]{
                                "a.) 2,200 meters ",
                                "b.) 2,100 meters",
                                "c.) 2,300 meters",
                        },
                        "a.) 2,200 meters "
                ),
                new Question(
                        "Local name of Philippine Crocodile",
                        new String[]{
                                "a.) Kalaw",
                                "b.) Kulasisi",
                                "c.) Bukarot",
                        },
                        "c.) Bukarot"
                ),
                new Question(
                        "The Saltwater Crocodile can reach a length of",
                        new String[]{
                                "a.) 4-5 meters",
                                "b.) 5-6 meters",
                                "c.) 6-7 meters ",
                        },
                        "c.) 6-7 meters "
                ),
                new Question(
                        "The Bitatawa is a giant _______ eating monitor lizard",
                        new String[]{
                                "a.) Fruit ",
                                "b.) Meat",
                                "c.) Vegetable",
                        },
                        "a.) Fruit "
                ),
                new Question(
                        "Conservation status of Bottle-nosed Dolphin",
                        new String[]{
                                "a.) Least concern",
                                "b.) Near Threatened",
                                "c.) Endangered ",
                        },
                        "c.) Endangered "
                ),
                new Question(
                        "Local name of Green Sea Turtle",
                        new String[]{
                                "a.) Pawikan, Bildog",
                                "b.) Pawikan, Karahan",
                                "c.) Pawikan",
                        },
                        "a.) Pawikan, Bildog"
                ),
                new Question(
                        "Conservation status of Hawksbill Sea Turtle",
                        new String[]{
                                "a.) Near Threatened",
                                "b.) Critically endangered",
                                "c.) Endangered",
                        },
                        "b.) Critically endangered"
                ),
                new Question(
                        "Conservation status of Philippine Duck.",
                        new String[]{
                                "a.) Vulnerable",
                                "b.) Near Threatened",
                                "c.) Endangered",
                        },
                        "a.) Vulnerable"
                ),
                new Question(
                        "This species is one of the rarest birds in the world and can be found only in Luzon Island in the Philippines.",
                        new String[]{
                                "a.) Isabela Oriole",
                                "b.) Philippine Brown Deer",
                                "c.) Green Racquet-tail",
                        },
                        "a.) Isabela Oriole"
                ),
                new Question(
                        "An animal has a very large dark bill, distinct crest on the head and large powerful claws.",
                        new String[]{
                                "a.) Isabela Oriole",
                                "b.) Philippine Eagle",
                                "c.) Green Racquet-tail",
                        },
                        "answerA"
                ),
                new Question(
                        "Conservation status of Scale-feathered Malkoha.",
                        new String[]{
                                "a.) Least concern",
                                "b.) Near Threatened",
                                "c.) Endangered ",
                        },
                        "a.) Least concern"
                ),
                new Question(
                        "Local name of Northern Rufous Hornbill",
                        new String[]{
                                "a.) Kalaw",
                                "b.) Kulasisi",
                                "c.) Bukarot",
                        },
                        "b.) Kulasisi"
                ),
                new Question(
                        "Local name of Philippine Hanging Parrot",
                        new String[]{
                                "a.) Kalaw",
                                "b.) Bukarot",
                                "c.) Colasisi",
                        },
                        "c.) Colasisi"
                ),
                new Question(
                        "The Sierra Madre ground warbler feeds on",
                        new String[]{
                                "a.) Insects",
                                "b.) Meat",
                                "c.) Vegetable",
                        },
                        "a.) Insects"
                ),
                new Question(
                        "What is the color of Isabela Oriole?",
                        new String[]{
                                "a.) Yellow-green",
                                "b.) Black-white",
                                "c.) Red-orange",
                        },
                        "a.) Yellow-green"
                ),
                new Question(
                        "Isabela Oriole lives in lowland forest and eats _____.",
                        new String[]{
                                "a.) Insects and fruits",
                                "b.) insects and vegetables",
                                "c.) insects and meat",
                        },
                        "a.) Insects and fruits"
                ),
        });

        Global.Places.get(0).setHardQuestions(new Question[]{
                new Question(
                        "Scientific name of Luzon Hornbill",
                        new String[]{
                                "a.) Penelopides manillae",
                                "b.) Pithecophaga jefferyi",
                                "c.) Oriolus isabellae",
                                "d.) Eretmochelys imbricate",
                        },
                        "a.) Penelopides manillae"
                ),
                new Question(
                        "Scientific name of Green Racquet-tail",
                        new String[]{
                                "a.) Prioniturus luconesis",
                                "b.) Ceyx melanurus",
                                "c.) Pteropus vampyrus",
                                "d.) Robsonius thompsoni",
                        },
                        "a.) Prioniturus luconesis"
                ),
                new Question(
                        "Scientific name of Warty pig",
                        new String[]{
                                "a.) Prioniturus montanus",
                                "b.) Phloeomys pallidus",
                                "c.) Sus philippensis",
                                "d.) Tursiops truncates",
                        },
                        "c.) Sus philippensis"
                ),
                new Question(
                        "Scientific name of Philippine Brown Deer",
                        new String[]{
                                "a.) Rusa Marianna",
                                "b.) Pithecophaga jefferyi",
                                "c.) Tursiops truncates",
                                "d.) Prioniturus luconesis",
                        },
                        "a.) Rusa Marianna"
                ),
                new Question(
                        "Scientific name of Golden-crowned Flying Fox",
                        new String[]{
                                "a.) Acerodon jubatus",
                                "b.) Buceros hydrocorax",
                                "c.) Prioniturus luconesis",
                                "d.) Sus philippensis",
                        },
                        "a.) Acerodon jubatus"
                ),
                new Question(
                        "Scientific name of Large Flying Fox",
                        new String[]{
                                "a.) Ceyx melanurus",
                                "b.) Pteropus vampyrus",
                                "c.) Lepidogrammus cumingi",
                                "d.) Varanus bitatawa",
                        },
                        "b.) Pteropus vampyrus"
                ),
                new Question(
                        "Scientific name of Celestial Monarch",
                        new String[]{
                                "a.) Hypothymis coelestis",
                                "b.) Lepidogrammus cumingi",
                                "c.) Anas luzonica",
                                "d.) Rusa Marianna",
                        },
                        "a.) Hypothymis coelestis"
                ),
                new Question(
                        "Scientific name of Philippine dwarf kingfisher",
                        new String[]{
                                "a.) Sus philippensis",
                                "b.) Phloeomys pallidus",
                                "c.) Ceyx melanurus",
                                "d.) Buceros hydrocorax",
                        },
                        "c.) Ceyx melanurus"
                ),
                new Question(
                        "Scientific name of Philippine pit viper",
                        new String[]{
                                "a.) Trimeresurus flavomaculatus",
                                "b.) Buceros hydrocora",
                                "c.) Prioniturus luconesis",
                                "d.) Sus philippensis",
                        },
                        "a.) Trimeresurus flavomaculatus"
                ),
                new Question(
                        "Scientific name of Northern Luzon giant cloud rat",
                        new String[]{
                                "a.) Sus philippensis",
                                "b.) Phloeomys pallidus",
                                "c.) Ceyx melanurus",
                                "d.) Phloeomys pallidus",
                        },
                        "c.) Ceyx melanurus"
                ),
                new Question(
                        "Scientific name of Philippine Crocodile",
                        new String[]{
                                "a.) Crocodylus mindorensis",
                                "b.) Crocodylus porosus",
                                "c.) Buceros hydrocorax",
                                "d.) Prioniturus luconesis",
                        },
                        "a.) Crocodylus mindorensis"
                ),
                new Question(
                        "Scientific name of Saltwater Crocodile",
                        new String[]{
                                "a.) Crocodylus mindorensis",
                                "b.) Crocodylus porosus",
                                "c.) Buceros hydrocorax",
                                "d.) Prioniturus luconesis",
                        },
                        "b.) Crocodylus porosus"
                ),
                new Question(
                        "Scientific name of Bitatawa.",
                        new String[]{
                                "a.) Varanus bitatawa",
                                "b.) Buceros hydrocorax",
                                "c.) Prioniturus luconesis",
                                "d.) Sus philippensis",
                        },
                        "a.) Varanus bitatawa"
                ),
                new Question(
                        "Scientific name of Bottle-nosed Dolphin.",
                        new String[]{
                                "a.) Lepidogrammus cumingi",
                                "b.) Anas luzonica",
                                "c.) Rusa Marianna",
                                "d.) Tursiops truncates",
                        },
                        "d.) Tursiops truncates"
                ),
                new Question(
                        "Scientific name of Green Sea Turtle",
                        new String[]{
                                "a.) Lepidogrammus cumingi",
                                "b.) Chelonia mydas",
                                "c.) Anas luzonica",
                                "d.) Rusa Marianna",
                        },
                        "b.) Chelonia mydas"
                ),
                new Question(
                        "Scientific name of Hawksbill Sea Turtle",
                        new String[]{
                                "a.) Buceros hydrocorax",
                                "b.) Prioniturus luconesis",
                                "c.) Eretmochelys imbricate",
                                "d.) Anas luzonica",
                        },
                        "c.) Eretmochelys imbricate"
                ),
                new Question(
                        "Scientific name of Isabela Oriole",
                        new String[]{
                                "a.) Buceros hydrocorax",
                                "b.) Prioniturus luconesis",
                                "c.) Oriolus isabellae",
                                "d.) Anas luzonica",
                        },
                        "c.) Oriolus isabellae"
                ),
                new Question(
                        "Scientific name of Philippine Duck",
                        new String[]{
                                "a.) Varanus bitatawa",
                                "b.) Anas luzonica",
                                "c.) Prioniturus luconesis",
                                "d.) Sus philippensis",
                        },
                        "b.) Anas luzonica"
                ),
                new Question(
                        "Scientific name of Philippine Eagle",
                        new String[]{
                                "a.) Pithecophaga jefferyi",
                                "b.) Prioniturus luconesis",
                                "c.) Eretmochelys imbricate ",
                                "d.) Anas luzonica",
                        },
                        "a.) Pithecophaga jefferyi"
                ),
                new Question(
                        "Scientific name of Scale-feathered Malkoha",
                        new String[]{
                                "a.) Lepidogrammus cumingi ",
                                "b.) Prioniturus luconesis",
                                "c.) Eretmochelys imbricate ",
                                "d.) Anas luzonica",
                        },
                        "a.) Lepidogrammus cumingi "
                ),
                new Question(
                        "Scientific name of Northern Rufous Hornbill",
                        new String[]{
                                "a.) Lepidogrammus cumingi ",
                                "b.) Prioniturus luconesis",
                                "c.) Eretmochelys imbricate ",
                                "d.) Buceros hydrocorax",
                        },
                        "d.) Buceros hydrocorax"
                ),
                new Question(
                        "Scientific name of Philippine Hanging Parrot",
                        new String[]{
                                "a.) Pithecophaga jefferyi",
                                "b.) Loriculus philippensis",
                                "c.) Eretmochelys imbricate",
                                "d.) Anas luzonica",
                        },
                        "b.) Loriculus philippensis"
                ),
                new Question(
                        "Scientific name of Sierra Madre ground warbler",
                        new String[]{
                                "a.) Robsonius thompsoni",
                                "b.) Anas luzonica",
                                "c.) Prioniturus luconesis",
                                "d.) Sus philippensis",
                        },
                        "a.) Robsonius thompsoni"
                ),
                new Question(
                        "This species lives in rivers, streams, small lakes and ponds",
                        new String[]{
                                "a.) Philippine Crocodile",
                                "b.) Bitatawa",
                                "c.) Saltwater Crocodile",
                                "d.) Green Sea Turtle",
                        },
                        "a.) Philippine Crocodile"
                ),
                new Question(
                        "This species lives mainly in mangrove swamps, estuaries and large coastal lakes",
                        new String[]{
                                "a.) Philippine Crocodile",
                                "b.) Saltwater Crocodile",
                                "c.) Hawksbill Sea Turtle",
                                "d.) Green Sea Turtle",
                        },
                        "b.) Saltwater Crocodile"
                ),
        });


        //COPY ALL TO OTHER PLACES
                for(int i=1; i<Global.Places.size(); i++) {

                    Global.Places.get(i).setJigsawPuzzle(Global.Places.get(0).getJigsawPuzzle());
                    Global.Places.get(i).setInterMediateJigsawPuzzle(Global.Places.get(0).getInterMediateJigsawPuzzle());
                    Global.Places.get(i).setHardJigsawPuzzle(Global.Places.get(0).getHardJigsawPuzzle());
                    Global.Places.get(i).setGuessTheWord(Global.Places.get(0).getGuessTheWord());

                    //temporary
                    Global.Places.get(i).setQuestions(  Global.Places.get(0).getQuestions());

                }
        }

}
