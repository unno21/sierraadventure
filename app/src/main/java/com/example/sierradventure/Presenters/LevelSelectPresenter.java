package com.example.sierradventure.Presenters;

import android.content.Context;
import android.media.session.MediaSession;
import android.widget.Toast;

import com.example.sierradventure.R;
import com.example.sierradventure.Utilities.Database;
import com.example.sierradventure.Utilities.Difficulty;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Mode;
import com.example.sierradventure.Utilities.Settings;

public class LevelSelectPresenter {

    public interface View {

        void showDifficultyTitle(String title);

        void changeBackGroundLock(int index);

        void changeBackGoundNumber(int index, int number);

        void hideQuestionNumber(int index);

        void showQuestionActivity();

        void showPicturePuzzleActivity();

        void showGuessTheWordActivity();

        void setTitleImage(int image);

        void showMessage(String message);

    }

    private View mView;

    public LevelSelectPresenter(View view) {
        this.mView = view;
    }

    public void initiateDisplay() {

        int maxAttainedLevel = 1;

        int numberOfQuestion = 1;

        if (Settings.getDifficulty() == Difficulty.EASY)
            mView.showDifficultyTitle("EASY");
        else if (Settings.getDifficulty() == Difficulty.AVERAGE)
            mView.showDifficultyTitle("AVERAGE");
        else if (Settings.getDifficulty() == Difficulty.HARD)
            mView.showDifficultyTitle("HARD");

        if (Global.Mode == Mode.QUESTION)
            mView.setTitleImage(R.drawable.md_quiz_challenge);
        else if (Global.Mode == Mode.GUESS_THE_WORD)
            mView.setTitleImage(R.drawable.md_guess_pic);
        else if (Global.Mode == Mode.PICTURE_PUZZLE)
            mView.setTitleImage(R.drawable.md_picture_puzzle);

        Database db = new Database((Context)mView);

        String difficulty = Settings.getDifficulty() == Difficulty.EASY ? "EASY" :  Settings.getDifficulty() == Difficulty.AVERAGE ? "AVERAGE" : "HARD";

        if (Global.Mode == Mode.QUESTION) {
            maxAttainedLevel = db.getLevel("Quiz", difficulty);
            numberOfQuestion = Global.SelectedPlace.getQuestions().length < 20 ? Global.SelectedPlace.getQuestions().length : 20;
        } else if (Global.Mode == Mode.PICTURE_PUZZLE) {
            maxAttainedLevel = db.getLevel("Jigsaw Puzzle", difficulty);
            numberOfQuestion = Settings.getDifficulty() == Difficulty.EASY ? Global.Places.get(0).getJigsawPuzzle().length :  Settings.getDifficulty() == Difficulty.AVERAGE ?  Global.Places.get(0).getInterMediateJigsawPuzzle().length :  Global.Places.get(0).getHardJigsawPuzzle().length;
        } else if (Global.Mode == Mode.GUESS_THE_WORD) {
            maxAttainedLevel = db.getLevel("Guess the word", difficulty);
            numberOfQuestion = Global.SelectedPlace.getGuessTheWord().length;
        }

        //mView.showMessage(String.valueOf(maxAttainedLevel));
        //Toast.makeText((Context)mView, maxAttainedLevel, Toast.LENGTH_SHORT).show();

//        int maxAttainedLevel = 1; //get this from database
//
//        int numberOfQuestion = Global.SelectedPlace.getQuestions().length;
        maxAttainedLevel = maxAttainedLevel > 20 ? 20 : maxAttainedLevel;

        //set text as level number
        for (int i = 0; i < maxAttainedLevel; i ++) {
            mView.changeBackGoundNumber(i, i + 1);
        }

        int maxLevel = Settings.getDifficulty() == Difficulty.EASY ? Global.SelectedPlace.getGuessTheWord().length :  Settings.getDifficulty() == Difficulty.AVERAGE ? Global.SelectedPlace.getGuessTheWord().length : Global.SelectedPlace.getGuessTheWord().length;
        //change bacground resource as lock
        for (int i = maxAttainedLevel; i < numberOfQuestion; i++) {
            if (i < maxLevel) {
                mView.changeBackGroundLock(i);
            }
        }

        //hide unnecessary button
        for (int i = numberOfQuestion; i < 20; i++) {
            mView.hideQuestionNumber(i);
        }

    }

    public void clickQuesttion(int index) {

        Global.SelectedLevel = index + 1;

        Database db = new Database((Context)mView);

        String difficulty = Settings.getDifficulty() == Difficulty.EASY ? "EASY" :  Settings.getDifficulty() == Difficulty.AVERAGE ? "AVERAGE" : "HARD";

        int maxAttainedLevel = 1;

        if (Global.Mode == Mode.QUESTION)
            maxAttainedLevel = db.getLevel("Quiz", difficulty);
        else if (Global.Mode == Mode.GUESS_THE_WORD)
            maxAttainedLevel = db.getLevel("Guess the word", difficulty);
        else if (Global.Mode == Mode.PICTURE_PUZZLE)
            maxAttainedLevel = db.getLevel("Jigsaw Puzzle", difficulty);


       // int maxAttainedLevel = 1; //get this from database
        maxAttainedLevel = maxAttainedLevel > 20 ? 20 : maxAttainedLevel;

        if (index < maxAttainedLevel) {


            if (Global.Mode == Mode.QUESTION) {

                if (Settings.getDifficulty() == Difficulty.EASY)
                    Global.SelectedQuestion = Global.SelectedPlace.getQuestions()[index];
                else if (Settings.getDifficulty() == Difficulty.AVERAGE)
                    Global.SelectedQuestion = Global.SelectedPlace.getIntermediateQuestions()[index];
                else
                    Global.SelectedQuestion = Global.SelectedPlace.getHardQuestions()[index];

                mView.showQuestionActivity();

            }
            else if (Global.Mode == Mode.GUESS_THE_WORD) {

                mView.showGuessTheWordActivity();

            }
            else if (Global.Mode == Mode.PICTURE_PUZZLE) {

                mView.showPicturePuzzleActivity();

            }




        } else {

            mView.showMessage("Current level is locked.");

        }

    }

}
