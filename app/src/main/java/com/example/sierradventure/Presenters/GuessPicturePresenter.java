package com.example.sierradventure.Presenters;

import android.content.Context;

import com.example.sierradventure.Models.GuessTheWord;
import com.example.sierradventure.Utilities.CollectionHelper;
import com.example.sierradventure.Utilities.Database;
import com.example.sierradventure.Utilities.Difficulty;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Settings;
import com.google.android.gms.common.internal.GmsLogger;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

public class GuessPicturePresenter {

    public interface View {
        void hideAnswerButton(int left, int right);
        void showAllAnswerButton();
        void addAnswer(String text, int position);
        void removeAnswer(int position);
        void enableLetter(int position);
        void disableLetter(int position);
        void loadImage(int image);
        void loadLetters(ArrayList<String> letters);
        void clearAnswer();
        void showCorrectAlert(String message);
        void showMessage(String message);
    }

    private View mView;

    private int finishedMinigame = 1;

    private ArrayList<String> playerAnswerLetters = new ArrayList<>();

    private ArrayList<String> randomizeLetters;

    private ArrayList<String> answerIndexes = new ArrayList<>();

    private int left, right;

    private GuessTheWord guessTheWord;

    public GuessPicturePresenter(View view) {
        this.mView = view;
    }

    public void initializeComponents() {

        Database db = new Database((Context)mView);

        String difficulty = Settings.getDifficulty() == Difficulty.EASY ? "EASY" :  Settings.getDifficulty() == Difficulty.AVERAGE ? "AVERAGE" : "HARD";

//        int level = db.getLevel("Guess the word", difficulty);

        int level = Global.SelectedLevel;

        if (Settings.getDifficulty() == Difficulty.EASY)
            guessTheWord = Global.SelectedPlace.getGuessTheWord()[level - 1];
        else if (Settings.getDifficulty() == Difficulty.AVERAGE)
            guessTheWord = Global.SelectedPlace.getInterMediateGuessTheWord()[level - 1];
        else if (Settings.getDifficulty() == Difficulty.HARD)
            guessTheWord = Global.SelectedPlace.getHardGuessTheWord()[level - 1];

        mView.loadImage(guessTheWord.getImage());

        randomizeLetters = CollectionHelper.RandomizeLetters(guessTheWord.getAnswer(), 8);

        mView.loadLetters(randomizeLetters);

        //mView.showAllAnswerButton();

        /* Hide unneeded letter container */
        int numberOfContainer = 10;
        int difference = numberOfContainer - guessTheWord.getAnswer().length();
        left = difference / 2;
        right = difference - left;

        mView.hideAnswerButton(left, right);

        /* Add temporary space to Player's Answer Array */
        playerAnswerLetters.clear();
        for(int i = 0; i < guessTheWord.getAnswer().length(); i++) {
            playerAnswerLetters.add(" ");
        }

    }

    public void answer(String letter, int index) {

        /* check if still has space for answer */
        if (playerAnswerLetters.indexOf(" ") > -1) {

            /* replace the nearest space with a letter */
            for (int i = 0; i < playerAnswerLetters.size(); i++) {
                if (playerAnswerLetters.get(i).equals(" ")) {
                    playerAnswerLetters.set(i, letter);
                    int position = i + left;
                    mView.addAnswer(letter, position);
                    break;
                }
            }

            mView.disableLetter(index);

            int indexOfNeg1 = answerIndexes.indexOf("-1");
            if (indexOfNeg1 > -1) {
                answerIndexes.set(indexOfNeg1, String.valueOf(index));
            } else {
                answerIndexes.add(String.valueOf(index));
            }
//            checkAnswer();
        }

    }

    public void returnAnswer(String letter, int index) {

            try {

                int answerIndex = index - left;
                int indexOfBtnClue = Integer.parseInt(answerIndexes.get(answerIndex));

                mView.enableLetter(indexOfBtnClue);

                playerAnswerLetters.set(answerIndex, " ");

                mView.removeAnswer(index);

                answerIndexes.set(answerIndex, "-1");

            } catch (Exception ex){}

    }

    public void checkAnswer() {

        String playerAnswer = "";
        for (String s : playerAnswerLetters) {
            playerAnswer += s;
        }
        if (playerAnswer.equals(guessTheWord.getAnswer().toUpperCase())) {

            Database db = new Database((Context)mView);

            String difficulty = Settings.getDifficulty() == Difficulty.EASY ? "EASY" :  Settings.getDifficulty() == Difficulty.AVERAGE ? "AVERAGE" : "HARD";

            int maxLevel = Settings.getDifficulty() == Difficulty.EASY ? Global.SelectedPlace.getGuessTheWord().length :  Settings.getDifficulty() == Difficulty.AVERAGE ? Global.SelectedPlace.getGuessTheWord().length : Global.SelectedPlace.getGuessTheWord().length;

            maxLevel = (maxLevel > 20) ? 20 : maxLevel;

            int level = db.getLevel("Guess the word", difficulty);

            if (Global.SelectedLevel == level && level < maxLevel) {
                db.update("Guess the word", difficulty, ++level);
            }

            mView.showCorrectAlert(guessTheWord.getAnswer());

        } else {

            mView.showMessage("Incorrect answer.");

        }
    }

    public void next() {
        if (Global.SelectedLevel == 20) {

            if (Settings.getDifficulty() == Difficulty.EASY)
                Settings.Difficulty(Difficulty.AVERAGE);
            else if (Settings.getDifficulty() == Difficulty.AVERAGE)
                Settings.Difficulty(Difficulty.HARD);

        } else {
            Global.SelectedLevel++;
        }
    }

}
