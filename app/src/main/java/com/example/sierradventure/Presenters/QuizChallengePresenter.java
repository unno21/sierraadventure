package com.example.sierradventure.Presenters;

import android.content.Context;

import com.example.sierradventure.Models.Question;
import com.example.sierradventure.Utilities.Database;
import com.example.sierradventure.Utilities.Difficulty;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Settings;

import java.util.ArrayList;
import java.util.Random;

public class QuizChallengePresenter {

    public interface View {

        void showQuestion(Question question);

        void showCorrectDialog();

        void showWrongDialog();

    }

    private View mView;

    private Question question;

    public QuizChallengePresenter(View view) {
        this.mView = view;
    }

    public void initializeDisplay() {

//        Question question = Global.SelectedQuestion;

        Question[] questions = new Question[0];

        if (Settings.getDifficulty() == Difficulty.EASY)
            questions = Global.SelectedPlace.getQuestions();
        else if (Settings.getDifficulty() == Difficulty.AVERAGE)
            questions = Global.SelectedPlace.getIntermediateQuestions();
        else if (Settings.getDifficulty() == Difficulty.HARD)
            questions = Global.SelectedPlace.getHardQuestions();

        if (question == null)
            question = radomizeQuestion(questions);

        mView.showQuestion(question);

    }

    public void answer(String answer) {

//        Question question = Global.SelectedQuestion;

        if (question.getAnswer().toUpperCase().equals(answer.toUpperCase())) {

            mView.showCorrectDialog();

            Database db = new Database((Context)mView);

            String difficulty = Settings.getDifficulty() == Difficulty.EASY ? "EASY" :  Settings.getDifficulty() == Difficulty.AVERAGE ? "AVERAGE" : "HARD";

            int maxLevel = Settings.getDifficulty() == Difficulty.EASY ? Global.SelectedPlace.getQuestions().length :  Settings.getDifficulty() == Difficulty.AVERAGE ? Global.SelectedPlace.getIntermediateQuestions().length : Global.SelectedPlace.getHardQuestions().length;

            maxLevel = (maxLevel > 20) ? 20 : maxLevel;

            int level = db.getLevel("Quiz", difficulty);

            if (Global.SelectedLevel == level && level < maxLevel) {
                /*
                    add 1 to max level attained
                    and save to database
                */
                db.update("Quiz", difficulty, ++level);
            }


        } else {

            mView.showWrongDialog();

        }

    }

    private Question radomizeQuestion(Question[] questions) {

        int size = questions.length;

        final int randomIndex = new Random().nextInt(size);

        return questions[randomIndex];

    }

}
