package com.example.sierradventure;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.speech.tts.TextToSpeech;
import android.support.constraint.solver.GoalRow;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sierradventure.Models.Animal;
import com.example.sierradventure.Utilities.ClickSoundService;
import com.example.sierradventure.Utilities.Global;
import com.example.sierradventure.Utilities.Screen;
import com.example.sierradventure.Utilities.Settings;

import java.util.ArrayList;
import java.util.Locale;

public class ZoomActivity extends AppCompatActivity {

    int lastImageSize = 0;
    ArrayList<ImageView> imageViews = new ArrayList<>();
    ArrayList<Animal> animals = new ArrayList<Animal>();
    int counter = 0;
    TextToSpeech tts;
    boolean hasActive = false;
    Dialog searchDialog, informationDialog;

    ImageView imgHome, imgSetting, imgNext, imgBack, imgMap;
    ImageView btnSearch;
    Screen screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);
        counter = 0;

        imageViews.add((ImageView)findViewById(R.id.img1));
        imageViews.add((ImageView)findViewById(R.id.img2));
        imageViews.add((ImageView)findViewById(R.id.img3));
        imageViews.add((ImageView)findViewById(R.id.img4));
        imageViews.add((ImageView)findViewById(R.id.img5));
        imageViews.add((ImageView)findViewById(R.id.img6));
        imageViews.add((ImageView)findViewById(R.id.img7));
        imageViews.add((ImageView)findViewById(R.id.img8));
        imageViews.add((ImageView)findViewById(R.id.img9));
        imageViews.add((ImageView)findViewById(R.id.img10));
        imageViews.add((ImageView)findViewById(R.id.img11));
        imageViews.add((ImageView)findViewById(R.id.img12));
        imageViews.add((ImageView)findViewById(R.id.img13));
        imageViews.add((ImageView)findViewById(R.id.img14));
        imageViews.add((ImageView)findViewById(R.id.img15));
        imageViews.add((ImageView)findViewById(R.id.img16));
        imageViews.add((ImageView)findViewById(R.id.img17));
        imageViews.add((ImageView)findViewById(R.id.img18));
        imageViews.add((ImageView)findViewById(R.id.img19));
        imageViews.add((ImageView)findViewById(R.id.img20));
        imageViews.add((ImageView)findViewById(R.id.img21));
        imageViews.add((ImageView)findViewById(R.id.img22));
        imageViews.add((ImageView)findViewById(R.id.img23));

        screen = new Screen(this);

        informationDialog = new Dialog(this);

        btnSearch = findViewById(R.id.imgSearch);
        btnSearch.setOnClickListener( v -> {
            playButtonClickSound();
            Global.SearchString = "";
            Intent i = new Intent(ZoomActivity.this, PlaceInfoActivity.class);
            startActivity(i);

//            searchDialog = new Dialog(this);
//            searchDialog.setContentView(R.layout.dialog_search);
//            searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            searchDialog.show();
//
//            Button btnSearch = searchDialog.findViewById(R.id.btnDialogSearchAnimal);
//            EditText txtSearchAnimal = searchDialog.findViewById(R.id.txtDialogSearchAnimal);
//
//            btnSearch.setOnClickListener( b -> {
//                Global.SearchString = txtSearchAnimal.getText().toString();
//                searchDialog.dismiss();
//
//                Intent i = new Intent(ZoomActivity.this, PlaceInfoActivity.class);
//                startActivity(i);
//
//            });
        });

        animals.add( new Animal(R.drawable.cabagan_animal_12,
                "Philippine pit viper is a venomous pit viper species endemic to the Philippines. Two subspecies are currently recognized, including the nominate subspecies.",
                "Philippine Pit Viper",
                " ",
                "Trimeresurus Flavomaculatus",
                "Least Concern",
                "ZEBULON HOOVER"
        ));

        animals.add( new Animal(R.drawable.divilacan_animal_17,
                "Bottlenose dolphins have the second largest encephalization levels of any mammal on Earth (humans have the largest), sharing close ratios with those of humans and other great apes, which more than likely contributes to their high intelligence and emotional intelligence.",
                "Bottle-Nosed Dolphin",
                " ",
                "Tursiops Truncatus",
                "Endangered",
                "ROBERT PERRY"
        ));

        animals.add( new Animal(R.drawable.divilacan_animal_20,
                "The celestial monarch is a species of bird in the family Monarchidae, and one of the most attractive of all the monarch flycatchers, with its spectacular blue crest and large yellow eye-ring (neither of which are illustrated in the facing painting). It is endemic to the Philippines.",
                "Celestial Monarch",
                " ",
                "Hypothymis Coelestis",
                "Vulnerable",
                "JONI T. ACAY"
        ));

        animals.add(new Animal(R.drawable.maconacon_animal_17,
                "The Hawksbill Sea Turtle can be found only in tropical seas. It has overlapping scutes on its shell. It has a narrow head and a hooked beak shaped like the bill of a hawk. They live in coral reefs and eat sponges.",
                "Hawksbill Sea Turtle",
                "Pawikan, Karahan",
                "Eretmochelys Imbricata",
                "Critically Endangered",
                "NICK CALOYIANIS"
        ));

        animals.add(new Animal(R.drawable.dinapigue_animal_13,
                "The Large flying fox also known as the greater flying fox, Malaysian flying fox, large fruit bat, kalang or kalong, is a species of megabat in the Pteropodidae family. Like the other members of the genus Pteropus, or the Old World fruit bats, it feeds exclusively on fruits, nectar and flowers. It is noted for being one of the largest bats. It, as with all other Old World fruit bats, lacks the ability to echolocate.",
                "Large Flying Fox",
                " ",
                "Pteropus Vampyrus",
                "Near Threatened",
                "FLETCHER & BAYLIS"
        ));

        animals.add( new Animal(R.drawable.divilacan_animal_4,
                "The Philippine brown deer generally thrives in a terrestrial environment from sea level up to at least 2,900m. They prefer to forage in grasslands under primary and secondary forests. However, due to forest denudation and excessive hunting, they are driven uphill to hide in the remaining patches of forests. They are generally nocturnal which means that they look for food: grasses, leaves, fallen fruits and berries at night. During daytime, they rest in the dense forest thickets. Mating season is usually between September to January. During this time, females organize in small groups composed of at most eight (8) individuals, while males are solitary and are aggressive. After approximately six (6) months, females give birth to a single fawn with light-colored spots that eventually disappear after several weeks.",
                "Philippine Brown Deer",
                " ",
                "Rusa Marianna",
                "Vulnerable",
                "DANIEL HEUCLIN"
        ));

        animals.add(new Animal(R.drawable.cabagan_animal_6,
                "With its rusty-cinnamon head and bluish-grey bill, the Philippine duck is a rather distinctive bird. The cinnamon coloured head is boldly decorated with a black crown and a black stripe through the eye, while the rest of the plumage is brownish-grey. When in flight, a well-defined patch of glossy green on the wing can be clearly seen, which is bordered with black and has a narrow white edge, and the underside of the wing is also white. Immature Philippine ducks have slightly duller plumage than that of adults, while ducklings are olive-brown with a bright yellow face and neck. It calls with a typical duck-like quack.",
                "Philippine Duck",
                " ",
                "Anas Luzonica",
                "Vulnerable",
                "E.G PEIKER"
        ));

        animals.add(new Animal(R.drawable.cabagan_animal_11,
                "The Philippine dwarf kingfisher is a species of bird in the family Alcedinidae that is endemic to the Philippines. Its natural habitat is subtropical or tropical moist lowland forests. It is threatened by habitat loss.",
                "Philippine Dwarf Kingfisher",
                " ",
                "Ceyx Melanurus",
                "Vulnerable",
                "MIGUEL DAVID DE LEON"
        ));

        animals.add(new Animal(R.drawable.san_mariano_animal_14,
                "The Bitatawa is a giant fruit-eating monitor lizard. It lives in the lowland forest of northern Luzon. It has golden-yellow spots on its body and large strong claws to climb trees. It can grow up to 6 feet and can weigh up to 10 kg.",
                "Bitatawa",
                "Bitatawa, Batikaw",
                "Varanus Bitatawa",
                "Data Deficient",
                "BRIAN SANTOS"
        ));

        animals.add(new Animal(R.drawable.san_pablo_animal_3,
                "The Green Racquet-tail is the lowland relative of the Montane Racquet-tail. It is endemic to Luzon and satellite islands and is now very rare as a result of habitat loss and catching for the illegal pet trade.",
                "Green Racquet-tail",
                " ",
                "Prioniturus Luconesis",
                "Endangered",
                "DEL HOYO"
        ));

        animals.add(new Animal(R.drawable.dinapigue_animal_14,
                "The Green Sea Turtle can be found in tropical seas. It feeds on sea grasses, which gives it a greenish-colored fat under its shell. It has a small, rounded head and smooth carapace.",
                "Green Sea Turtle",
                "Pawikan, Bildog",
                "Chelonia Mydas",
                "Endangered",
                "LUCIANO CANDISANI"
        ));

        animals.add(new Animal(R.drawable.divilacan_animal_1,
                "The Luzon hornbill is the smallest hornbill in the Philippines. It is only found in Luzon and satellite islands. It is not as sensitive to forest disturbance as the Northern Rufous Hornbill and can also be seen in forest patches.",
                "Luzon Hornbill",
                "Tarictic",
                "Penelopides Manillae",
                "Not Threatened",
                "JONI T. ACAY"
        ));

        animals.add(new Animal(R.drawable.divilacan_animal_11,
                "The northern Luzon giant cloud rat is only found in northern and central part of Luzon, the Philippines. It is found in at least 12 provinces. The northern Luzon giant cloud rat prefers forest and scrub, but also occurs in degraded habitats such as plantations. It occurs from sea level to an altitude of about 2,200 meters (7,200 ft). In some areas it overlaps with the rarer giant bushy-tailed cloud rat, but that species mainly occurs at higher altitudes than the northern Luzon giant cloud rat. ",
                "Northern Luzon Giant Cloud Rat",
                " ",
                "Phloeomys Pallidus",
                "Least Concern",
                "DANIEL HEUCLIN"
        ));

        animals.add(new Animal(R.drawable.san_mariano_animal_15,
                "The Philippine Eagle is one of the largest eagles 3 feet tall and a wingspan of 7 feet. It feeds mainly on small mammals that live in trees. It is the national bird of the Philippines and survives only in pristine forests. It has a very large dark bill, distinct crest on the head and large powerful claws.",
                "Philippine Eagle",
                "Agila, Haring ibon",
                "Pithecophaga Jefferyi",
                "Critically Endangered",
                "MERJLIN VAN WEERD"
        ));

        animals.add(new Animal(R.drawable.san_pablo_animal_9,
                "Philippine hanging parrots are about 14 cm (5.5 in) long, weigh 32–40 g, and have a short rounded tail. They are mainly green with areas of red, orange, yellow, and blue varying between subspecies.The forehead is red and the irises are dark brown. Adults have red beaks and orange legs except for Loriculus (philippensis) bonapartei which have black beaks and grey legs.They are sexually dimorphic with only the males having red on their chin or upper chest, except for the Loriculus (philippensis) camiguinensis in which neither the male or female has a red bib or chest. Juveniles have less red on their heads and paler beaks, but otherwise resemble the female.",
                "Philippine Hanging Parrot",
                "Colasisi",
                "Loriculus Philippensis",
                "Least Concern",
                "ALEX VARGAS"
        ));

        animals.add(new Animal(R.drawable.divilacan_animal_15,
                "The Saltwater Crocodile or Estuarine Crocodile is one of the largest crocodiles in the world and can reach a length of 6 to 7 meters. It lives mainly in mangrove swamps, estuaries and large coastal lakes.",
                "Saltwater Crocodile",
                "Buwaya",
                "Crocodylus Porosus",
                "Not Globally Threatened",
                "MERJLIN VAN WEERD"
        ));

        animals.add(new Animal(R.drawable.ilagan_animal_6,
                "The scale-feathered malkoha (Phaenicophaeus cumingi) is a species of cuckoo in the family Cuculidae. It is endemic to the northern Philippines.",
                "Scale-feathered Malkoha",
                " ",
                "Lepidogrammus Cumingi",
                "Least Concern",
                "JONI T. ACAY"
        ));

        animals.add(new Animal(R.drawable.ilagan_animal_9,
                "The Sierra Madre ground warbler feeds on insects and lives in tropical under stories. It is a ground-walking songbird rotund, with strong legs and weak wings and it appears that it can barely fly. It tends to inhabit dense forest under stories, where they feed on insects. Their song is extremely high in pitched and it is difficult to locate the source of the sound in the forest they always sound like they are far away, even when they are almost at your feet. The bird looks similar to the other two species of ground warblers on the island of Luzon, the Bicol ground warbler and the Cordillera ground warbler, and probably for this reason that it was not recognized as an separate species.",
                "Sierra Madre Ground Warbler",
                " ",
                "Robsonius Thompsoni",
                "Least Concern",
                "JONI T. ACAY"
        ));

        animals.add(new Animal(R.drawable.maconacon_animal_3,
                "The Philippine warty pig  is one of four known species in the pig genus ( Sus ) endemic to the Philippines. The other three endemic species are the Visayan warty pig ( S. cebifrons ), Mindoro warty pig ( S. oliveri ) and the Palawan bearded pig ( S. ahoenobarbus ), also being rare members of the family Suidae. Philippine warty pigs have two pairs of warts, with a tuft of hair extending outwards from the warts closest to the jaw.",
                "Warty Pig",
                " ",
                "Sus Philippensis",
                "Vulnerable",
                "BRENT HUFFMAN"
        ));

        animals.add(new Animal(R.drawable.dinapigue_animal_12,
                "The Golden-crowned Flying Fox is the heaviest bat in the world. It can weigh up to 1,400 grams and has a wingspan of 3 feet. It is found only in the Philippines. It has a light yellow to golden fur on top of its head extending the back. It depends on undisturbed lowland forest where it feeds on fruits and flowers.",
                "Golden-Crowned Flying Fox",
                "Paniki",
                "Acerodon Jubatus",
                "Endangered",
                "FLETCHER & BAYLIS"
        ));

        animals.add(new Animal(R.drawable.san_mariano_animal_16,
                "The Isabela Oriole is one of the rarest birds in the world and can be found only in Luzon island in the Philippines. It has a yellow-green body, broad gray beak, yellow lores, a yellow eye-ring and gray feet. It lives in lowland forest and eats insects and fruits.",
                "Isabela Oriole",
                "Kiyaw",
                "Oriolus Isabellae",
                "Critically Endangered",
                "MERJLIN VAN WEERD"
        ));

        animals.add(new Animal(R.drawable.san_pablo_animal_2,
                "The Montane Racquet-tail belongs to the family of parrots. It is endemic to Luzon. Also known as Luzon Racquet-tail, it can only found high up in the mountains, above 1,000 meters from sea level. Their central tail feathers have extensions that look like rackets. The male bird has a red spot on its head.",
                "Montane Racquet-tail",
                " ",
                "Prioniturus Montanus",
                "Near Threatened",
                "MERJLIN VAN WEERD"
        ));

        animals.add(new Animal(R.drawable.tumauini_animal_12,
                "The Philippine Crocodile is one of the rarest crocodile species in the world. It can only be found in the Philippines. It is a relatively small freshwater crocodile. It lives in rivers, streams, small lakes and ponds. It feeds on snails, shrimps, fish, frogs, snakes and small mammals.",
                "Philippine Crocodile",
                "Bukarot",
                "Crocodylus Mindorensis",
                "Critically Endangered",
                "MERJLIN VAN WEERD"
        ));

//        images[0] = R.drawable.philippinepitviper;
//        images[1] = R.drawable.bottlenoseddolphin;
//        images[2] = R.drawable.celestialmonarch;
//        images[3] = R.drawable.hawksbillseaturtle;
//        images[4] = R.drawable.largeflyingfox;
//        images[5] = R.drawable.philippinebrowndeer;
//        images[6] = R.drawable.philippineduck;
//        images[7] = R.drawable.philippinedwarfkingfisher;
//        images[8] = R.drawable.bitatawa2;
//        images[9] = R.drawable.greenracquettail;
//        images[10] = R.drawable.greenseaturtle;
//        images[11] = R.drawable.luzonhornbill;
//        images[12] = R.drawable.northernluzongiantcloudrat;
//        images[13] = R.drawable.philippineeagle;
//        images[14] = R.drawable.philippinehangingparrot;
//        images[15] = R.drawable.saltwatercrocodile;
//        images[16] = R.drawable.scalefeatheredmalkoha;
//        images[17] = R.drawable.sierramadregroundwabbler;
//        images[18] = R.drawable.wartypig;

        for (int i = 0; i < imageViews.size(); i++) {
            int x = i;
            imageViews.get(i).setOnClickListener(view -> {

                if (imageViews.get(x).getLayoutParams().width == LinearLayout.LayoutParams.WRAP_CONTENT){
                    imageViews.get(x).getLayoutParams().width = lastImageSize;
                    imageViews.get(x).requestLayout();

                    hasActive = false;

                } else {

//                    if (!hasActive) {
                    playButtonClickSound();
                        final Animation zoomInAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
                        lastImageSize =  imageViews.get(x).getLayoutParams().width;
                        imageViews.get(x).startAnimation(zoomInAnimation);
                        imageViews.get(x).getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;
                        imageViews.get(x).requestLayout();

                        String toSpeak = "Animal name, " + animals.get(x).getEnglishName()
                                + ". Conservation status, " + animals.get(x).getConservationStatus()
                                + ". " + animals.get(x).getDescription();

                        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);

                        hasActive = true;

                        informationDialog.setContentView(R.layout.dialog_animal_information);
                        informationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        informationDialog.show();

                        TextView lblAnimalName = informationDialog.findViewById(R.id.lblAnimalName);
                        TextView lblAnimalDescription = informationDialog.findViewById(R.id.lblAnimalDescription);
                        ImageView btnInformatinDialogDismiss = informationDialog.findViewById(R.id.btnInformationDialogDismiss);

                        Animal animal = animals.get(x);
                        lblAnimalName.setText(animal.getEnglishName());
                        lblAnimalDescription.setText(animal.getDescription());
                        lblAnimalDescription.setMovementMethod(new ScrollingMovementMethod());
                        btnInformatinDialogDismiss.setOnClickListener( v -> {
                            informationDialog.dismiss();
                        });

                        informationDialog.setOnDismissListener( d -> {
                            tts.stop();
                            imageViews.get(x).getLayoutParams().width = lastImageSize;
                            imageViews.get(x).requestLayout();
                        });

                        Window window = informationDialog.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();

                        /*
                            GET THE POSITION OF IMAGEVIEW
                         */
                        int imgViewPositionFromTop = imageViews.get(x).getTop();
                        int screenHeight = screen.getHeigth();

                        if (imgViewPositionFromTop < (screenHeight / 2)) {
                            wlp.gravity = Gravity.BOTTOM;
                        } else {
                            wlp.gravity = Gravity.TOP;
                        }

                        informationDialog.show();


                        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                        window.setAttributes(wlp);
//                    }


                }

            });
        }

        //hide images not included in the selected place
        hideImages(animals);
//        Animal[] animalsToShow = Global.SelectedPlace.getAnimals();
//        for (int i = 0; i < animals.size(); i++) {
//            boolean shouldRemove = true;
//            for (Animal animal : animalsToShow) {
//                if (animals.get(i).getEnglishName().equals(animal.getEnglishName())) {
//                    shouldRemove = false;
//                    break;
//                }
//            }
//
//            if (shouldRemove) {
//                imageViews.get(i).setVisibility(View.GONE);
//            }
//        }


        /*
            Resize imageviews base on screen resolution
         */
//        Screen screen = new Screen(this);
//        float screenPercentageHeightDifference = screen.getHeigthPercentage();
//        float screenPercentageWidthDifference = screen.getWidthInPercentage();
//
//        for (ImageView imgV : imageViews) {
//            int oldHeight = imgV.getLayoutParams().height;
//            int oldWidth = imgV.getLayoutParams().width;
//
//            imgV.getLayoutParams().height = Math.round(oldHeight * screenPercentageHeightDifference);
//            imgV.getLayoutParams().width = Math.round(oldWidth *  screenPercentageWidthDifference);
//            imgV.requestLayout();
//
//        }

        tts = Global.Tts;
        String strCount = String.valueOf(Global.SelectedPlace.getAnimals().length);
        String placeName = Global.SelectedPlace.getName();
        String toSpeak = "There are " + strCount + " animal species with the town of " + placeName;
        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);

//        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//                if (status == TextToSpeech.SUCCESS) {
//                    int ttsLang = tts.setLanguage(Locale.UK);
//
//                    String strCount = String.valueOf(Global.SelectedPlace.getAnimals().length);
//                    String placeName = Global.SelectedPlace.getName();
//                    String toSpeak = "There are " + strCount + " animal species with the town of " + placeName;
//                    tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
//
//                    if (ttsLang == TextToSpeech.LANG_MISSING_DATA
//                            || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
//                        Log.e("TTS", "The Language is not supported!");
//                    } else {
//                        Log.i("TTS", "Language Supported.");
//                    }
//                    Log.i("TTS", "Initialization success.");
//                } else {
//                    Toast.makeText(getApplicationContext(), "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        imgHome = findViewById(R.id.imgHome);
        imgSetting = findViewById(R.id.imgSetting);
//        imgNext = findViewById(R.id.imgNext);
//        imgBack = findViewById(R.id.imgBack);
        imgMap = findViewById(R.id.imgMap);

        imgHome.setOnClickListener( v -> {
            playButtonClickSound();
            Intent i = new Intent(ZoomActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        });

        imgSetting.setOnClickListener( v -> {
            playButtonClickSound();
            Intent i = new Intent(ZoomActivity.this, SettingsActivity.class);
            startActivity(i);
        });

//        imgNext.setOnClickListener( v -> {
//
//            int index = Global.Places.indexOf(Global.SelectedPlace);
//            int totalPlaceCount = Global.Places.size();
//            if (index < (totalPlaceCount - 1)) {
//                Global.SelectedPlace = Global.Places.get(++index);
//                Intent i = new Intent(ZoomActivity.this, ZoomActivity.class);
//                startActivity(i);
//                finish();
//            }
//
//        });
//
//        imgBack.setOnClickListener( v -> {
//            int index = Global.Places.indexOf(Global.SelectedPlace);
//            if (index > 0) {
//                Global.SelectedPlace = Global.Places.get(--index);
//                Intent i = new Intent(ZoomActivity.this, ZoomActivity.class);
//                startActivity(i);
//                finish();
//            }
//        });

        imgMap.setOnClickListener( v -> {
            playButtonClickSound();
            finish();
        });

    }

    private void hideImages(ArrayList<Animal> animalsIncluded) {
        Animal[] animalsToShow = Global.SelectedPlace.getAnimals();
        for (int i = 0; i < animalsIncluded.size(); i++) {
            boolean shouldRemove = true;
            for (Animal animal : animalsToShow) {
                if (animalsIncluded.get(i).getEnglishName().equals(animal.getEnglishName())) {
                    shouldRemove = false;
                    break;
                }
            }

            if (shouldRemove) {
                imageViews.get(i).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }

    protected void onDestroy() {
        //stop service and stop music
        stopService(new Intent(ZoomActivity.this, SoundService.class));
        super.onDestroy();
        if (tts != null) {
            tts.stop();
//            tts.shutdown();
        }
    }

    private void playButtonClickSound() {
        if (Settings.isIsSoundOn()) {
            stopService(new Intent(ZoomActivity.this, ClickSoundService.class));
            startService(new Intent(ZoomActivity.this, ClickSoundService.class));
        }
    }
}
