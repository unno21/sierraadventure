package com.example.sierradventure;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity {

    Dialog helpDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        ImageButton btnBack = findViewById(R.id.btnHelpBack);
        btnBack.setOnClickListener( v -> {
            finish();
        });

        ImageButton btnHelpQuiz = findViewById(R.id.btnHelpQuiz);
        btnHelpQuiz.setOnClickListener( v -> {
            helpDialog = new Dialog(this);
            helpDialog.setContentView(R.layout.dialog_help);
            helpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            helpDialog.show();

            TextView lblHelp = helpDialog.findViewById(R.id.lblHelp);
            lblHelp.setText("1. Choose the letter of your answer; base your answer from what you learned about the animal species inside the NSMNP. ");
            lblHelp.setMovementMethod(new ScrollingMovementMethod());

            Button btnHelp = helpDialog.findViewById(R.id.btnHelpOk);
            btnHelp.setOnClickListener( b -> {
                helpDialog.dismiss();
            });

        });

        ImageButton btnHelpPicturePuzzle = findViewById(R.id.btnHelpPicturePuzzle);
        btnHelpPicturePuzzle.setOnClickListener( v -> {
            helpDialog = new Dialog(this);
            helpDialog.setContentView(R.layout.dialog_help);
            helpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            helpDialog.show();

            TextView lblHelp = helpDialog.findViewById(R.id.lblHelp);
            lblHelp.setText("1. You will be presented with a scrambled version of the picture.\n2. Reassemble the picture puzzle by swapping pieces to matching the animal picture that is shown above.");
            lblHelp.setMovementMethod(new ScrollingMovementMethod());

            Button btnHelp = helpDialog.findViewById(R.id.btnHelpOk);
            btnHelp.setOnClickListener( b -> {
                helpDialog.dismiss();
            });

        });

        ImageButton btnHelpGuessPicture = findViewById(R.id.btnHelpGuessPicture);
        btnHelpGuessPicture.setOnClickListener( v -> {
            helpDialog = new Dialog(this);
            helpDialog.setContentView(R.layout.dialog_help);
            helpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            helpDialog.show();

            TextView lblHelp = helpDialog.findViewById(R.id.lblHelp);
            lblHelp.setText("1. You need to recall the animals and guess what animal name is shown on the picture.\n2. Below the pictures, you will see blank spaces indicating how many letters are in the answer.\n3. Top on these letters to spell out the word you believe to be the answer and click send button.");
            lblHelp.setMovementMethod(new ScrollingMovementMethod());

            Button btnHelp = helpDialog.findViewById(R.id.btnHelpOk);
            btnHelp.setOnClickListener( b -> {
                helpDialog.dismiss();
            });

        });
    }
}
