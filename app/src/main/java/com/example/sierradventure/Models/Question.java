package com.example.sierradventure.Models;

public class Question {

    private String text;

    private String[] choices;

    private String answer;

    public Question(String text, String[] choices, String answer) {
        this.text = text;
        this.choices = choices;
        this.answer = answer;
    }

    public String getText() {
        return text;
    }

    public String[] getChoices() {
        return choices;
    }

    public String getAnswer() {
        return answer;
    }
}
