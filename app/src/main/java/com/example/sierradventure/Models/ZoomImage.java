package com.example.sierradventure.Models;

public class ZoomImage {

    private String name;

    private String conservationStatus;

    public ZoomImage(String name, String conservationStatus) {
        this.name = name;
        this.conservationStatus = conservationStatus;
    }

    public String getName() {
        return name;
    }

    public String getConservationStatus() {
        return conservationStatus;
    }
}
