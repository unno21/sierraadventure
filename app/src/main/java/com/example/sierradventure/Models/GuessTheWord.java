package com.example.sierradventure.Models;

public class GuessTheWord {

    private String answer;

    private int image;

    public GuessTheWord(String answer, int image) {
        this.answer = answer;
        this.image = image;
    }

    public String getAnswer() {
        return answer;
    }

    public int getImage() {
        return image;
    }
}
