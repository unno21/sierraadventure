package com.example.sierradventure.Models;

import android.support.annotation.NonNull;

import java.util.Arrays;

public class Animal implements Comparable<Animal> {

    private int image;

    private String description;

    private String englishName;

    private String localName;

    private String scientificName;

    private String conservationStatus;

    private String photoCredit;

    public Animal(int image, String description, String englishName, String localName, String scientificName, String conservationStatus, String photoCredit) {
        this.image = image;
        this.description = description;
        this.englishName = englishName;
        this.localName = localName;
        this.scientificName = scientificName;
        this.conservationStatus = conservationStatus;
        this.photoCredit = photoCredit;
    }

    public int getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getLocalName() {
        return localName;
    }

    public String getScientificName() {
        return scientificName;
    }

    public String getConservationStatus() {
        return conservationStatus;
    }

    public String getPhotoCredit() {
        return photoCredit;
    }

    @Override
    public int compareTo(@NonNull Animal o) {
        return this.getEnglishName().compareTo(o.getEnglishName());
    }
}
