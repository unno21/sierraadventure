package com.example.sierradventure.Models;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Place {

    private String name;

    private Animal[] animals;

    private Animal[] trees;

    private Question[] questions;

    private Question[] intermediateQuestions;

    private Question[] hardQuestions;

    private GuessTheWord[] guessTheWord;

    private GuessTheWord[] interMediateGuessTheWord;

    private GuessTheWord[] hardGuessTheWord;

    private JigsawPuzzle[] jigsawPuzzle;

    private JigsawPuzzle[] interMediateJigsawPuzzle;

    private JigsawPuzzle[] hardJigsawPuzzle;

    private double latitude;

    private double longitude;

    private float legendColor;

    public Place(String name, Animal[] animals, Animal[] trees) {
        this.name = name;
        this.animals = animals;
        this.trees = trees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Animal[] getAnimals() {

        Arrays.sort(this.animals);

        return this.animals;
    }

    public Animal[] getTrees() {
        return trees;
    }

    public Question[] getQuestions() { return this.questions; }

    public Question[] getIntermediateQuestions() { return this.intermediateQuestions; }

    public Question[] getHardQuestions() { return this.hardQuestions; }

    public GuessTheWord[] getGuessTheWord() {
        return guessTheWord;
    }

    public GuessTheWord[] getInterMediateGuessTheWord() {
        return interMediateGuessTheWord;
    }

    public GuessTheWord[] getHardGuessTheWord() {
        return hardGuessTheWord;
    }

    public JigsawPuzzle[] getJigsawPuzzle() {
        return jigsawPuzzle;
    }

    public JigsawPuzzle[] getInterMediateJigsawPuzzle() {
        return interMediateJigsawPuzzle;
    }

    public JigsawPuzzle[] getHardJigsawPuzzle() {
        return hardJigsawPuzzle;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getLegendColor() {
        return legendColor;
    }

    public void setAnimals(Animal[] animals) {
        this.animals = animals;
    }

    public void setTrees(Animal[] trees) {
        this.trees = trees;
    }

    public void setQuestions(Question[] questions) {
        this.questions = questions;
    }

    public void setIntermediateQuestions(Question[] questions) { this.intermediateQuestions = questions; }

    public void setHardQuestions(Question[] questions) { this.hardQuestions = questions; }

    public void setInterMediateGuessTheWord(GuessTheWord[] interMediateGuessTheWord) {
        this.interMediateGuessTheWord = interMediateGuessTheWord;
    }

    public void setHardGuessTheWord(GuessTheWord[] hardGuessTheWord) {
        this.hardGuessTheWord = hardGuessTheWord;
    }

    public void setGuessTheWord(GuessTheWord[] guessTheWord) {
        this.guessTheWord = guessTheWord;
    }

    public void setJigsawPuzzle(JigsawPuzzle[] jigsawPuzzle) {
        this.jigsawPuzzle = jigsawPuzzle;
    }

    public void setInterMediateJigsawPuzzle(JigsawPuzzle[] interMediateJigsawPuzzle) {
        this.interMediateJigsawPuzzle = interMediateJigsawPuzzle;
    }

    public void setHardJigsawPuzzle(JigsawPuzzle[] hardJigsawPuzzle) {
        this.hardJigsawPuzzle = hardJigsawPuzzle;
    }

    public void setLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setLegendColor(float color) {
        this.legendColor = color;
    }
}
