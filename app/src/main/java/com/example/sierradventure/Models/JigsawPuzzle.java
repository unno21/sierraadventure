package com.example.sierradventure.Models;

public class JigsawPuzzle {

    private int actualImage;

    private int[] partImages;

    public JigsawPuzzle(int actualImage, int[] partImages) {
        this.actualImage = actualImage;
        this.partImages = partImages;
    }

    public int getActualImage() {
        return actualImage;
    }

    public int[] getPartImages() {
        return partImages;
    }
}
